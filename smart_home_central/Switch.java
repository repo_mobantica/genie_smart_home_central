package com.gsmarthome.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Switch {
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
	
	private String switchName;
	
	private String switchType;
	
	private String dimmerStatus;
	
	private String dimmerValue;
	
	private String switchStatus;
	
	@ManyToOne
	@JsonIgnore
	@JoinColumn(name="IOTPRODUCTID", nullable=false)
	private IOTProduct iotProduct;

	public Switch() {
	}

	
	
	public Switch(String switchName) {
		this.switchName = switchName;
	}


	

	public Switch(String switchName, String switchType, String dimmerStatus, String dimmerValue, String switchStatus) {
		super();
		this.switchName = switchName;
		this.switchType = switchType;
		this.dimmerStatus = dimmerStatus;
		this.dimmerValue = dimmerValue;
		this.switchStatus = switchStatus;
	}



	public Switch(String switchName, String switchType) {
		this.switchName = switchName;
		this.switchType = switchType;
	}



	public Switch(String switchName, IOTProduct iotProduct) {
		this.switchName = switchName;
		this.iotProduct = iotProduct;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSwitchName() {
		return switchName;
	}

	public void setSwitchName(String switchName) {
		this.switchName = switchName;
	}

	public IOTProduct getIotProduct() {
		return iotProduct;
	}

	public void setIotProduct(IOTProduct iotProduct) {
		this.iotProduct = iotProduct;
	}

	public String getSwitchType() {
		return switchType;
	}

	public void setSwitchType(String switchType) {
		this.switchType = switchType;
	}


	

	public String getDimmerStatus() {
		return dimmerStatus;
	}



	public void setDimmerStatus(String dimmerStatus) {
		this.dimmerStatus = dimmerStatus;
	}



	public String getDimmerValue() {
		return dimmerValue;
	}



	public void setDimmerValue(String dimmerValue) {
		this.dimmerValue = dimmerValue;
	}



	public String getSwitchStatus() {
		return switchStatus;
	}



	public void setSwitchStatus(String switchStatus) {
		this.switchStatus = switchStatus;
	}



	@Override
	public String toString() {
		return "Switch [id=" + id + ", switchName=" + switchName
				+ ", switchType=" + switchType + "]";
	}


	
	

}
