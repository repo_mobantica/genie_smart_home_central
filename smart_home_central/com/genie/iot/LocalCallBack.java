package com.genie.iot;

import come.genie.iot.utility.JDBCConnection;
import come.genie.iot.utility.MqttConnection;
import come.genie.iot.utility.Utility;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import javax.swing.SwingUtilities;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.json.JSONException;
import org.json.JSONObject;











public class LocalCallBack
  implements MqttCallback
{
  private static LocalCallBack currentObject;
  private static Integer switchCount;
  private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:00");
  private static String oldMessage = "";
  
  public LocalCallBack() {}
  
  public void setCallBackLocal() { Properties prop = new Properties();
    InputStream input = null;
    try {
      input = new FileInputStream("config.properties");
      prop.load(input);
      MemoryPersistence memoryPersistence = new MemoryPersistence();
      String clientId = "genieClientId_" + System.currentTimeMillis() % 1002L;
      MqttClient client = new MqttClient(prop.getProperty("localBroker"), clientId, memoryPersistence);
      MqttConnectOptions connectOptions = new MqttConnectOptions();
      connectOptions.setCleanSession(true);
      client.connect();
      client.setCallback(this);
      client.subscribe(prop.getProperty("physicalSwTopic"));
      System.out.println("Subscribed to topic \t" + prop.getProperty("physicalSwTopic") + "\t" + 
        new Timestamp(System.currentTimeMillis()));



    }
    catch (IOException localIOException1)
    {



      if (input != null) {
        try {
          input.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
    catch (MqttException e)
    {
      System.out.println("Big problem local web service is down or not connected or crashed\t" + 
        new Timestamp(System.currentTimeMillis()));
      
      if (input != null) {
        try {
          input.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
    finally
    {
      if (input != null) {
        try {
          input.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
  }
  
  public void connectionLost(Throwable arg0)
  {
    try {
      System.out.println("\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Local Connection lost <<<<<<<<<<<<<<<<<<<<<<<<<\t" + 
        new Timestamp(System.currentTimeMillis()));
      Thread.sleep(10000L);
      getCurrentObj().setCallBackLocal();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
  
  public static LocalCallBack getCurrentObj() {
    if (currentObject == null) {
      currentObject = new LocalCallBack();
      return currentObject;
    }
    return currentObject;
  }
  


  public void deliveryComplete(IMqttDeliveryToken arg0) {}
  

  public void messageArrived(String arg0, final MqttMessage message)
  {
    if (!oldMessage.equals(message.toString())) {
      oldMessage = message.toString();
      SwingUtilities.invokeLater(new Runnable()
      {
        public void run()
        {
          try {
            String activityMessage = "Physically switch ";
            String dimmerValue = "0";
            String switchStatus = "0";
            String dimmerStatus = "0";
            byte[] b = message.toString().getBytes();
            StringBuffer topicName = new StringBuffer();
            
            for (int i = 5; i < b.length; i++)
              topicName.append((char)b[i]);
            if (((char)b[2] == 'p') && ((char)b[3] == 'q'))
            {
              if (b[1] == 42) {
                if ((b[0] == 5) || (b[0] == 6))
                {
                  System.out.println("Dimmer Switch no " + b[0] + " is off (" + topicName + ")" + "\t" + 
                    new Timestamp(System.currentTimeMillis()));
                  activityMessage = activityMessage + "off ";
                  dimmerValue = switchStatus = "0";
                  dimmerStatus = "1";
                }
                else {
                  System.out.println("Switch no " + b[0] + " is off (" + topicName + ")" + "\t" + 
                    new Timestamp(System.currentTimeMillis()));
                  dimmerValue = switchStatus = "0";
                  dimmerStatus = "0";
                  activityMessage = activityMessage + "off ";
                }
              } else if (b[1] == 1)
              {
                System.out.println("Switch no " + b[0] + " is on (" + topicName + ")" + "\t" + 
                  new Timestamp(System.currentTimeMillis()));
                switchStatus = "1";
                dimmerValue = "0";
                activityMessage = activityMessage + "on ";
              }
              else {
                System.out.println("Dimmer Switch no " + b[0] + " is on and value is " + b[1] + " (" + 
                  topicName + ")" + "\t" + new Timestamp(System.currentTimeMillis()));
                switchStatus = "1";
                dimmerStatus = "1";
                dimmerValue = b[1];
                activityMessage = activityMessage + "on ";
              }
            }
            else {
              StringBuffer connectedOrNot = new StringBuffer();
              for (int i = 0; i < b.length; i++)
                connectedOrNot.append((char)b[i]);
              if (!connectedOrNot.toString().contains("con")) {
                String switchStatusString = Integer.toBinaryString(b[0]);
                JSONObject jMessage = new JSONObject();
                LocalCallBack.switchCount = Integer.valueOf(6);
                for (Integer i = Integer.valueOf(switchStatusString.length() - 2); i.intValue() >= 0; i = Integer.valueOf(i.intValue() - 1)) {
                  try {
                    char charAt = switchStatusString.charAt(i.intValue());
                    jMessage.put(LocalCallBack.switchCount.toString(), Character.toString(charAt));
                    LocalCallBack.switchCount = Integer.valueOf(LocalCallBack.switchCount.intValue() - 1);
                  } catch (Exception exception) {
                    System.out.println("Exceptoin: " + exception + "\t" + 
                      new Timestamp(System.currentTimeMillis()));
                  }
                }
                
                jMessage.put("5dim", b[1]);
                jMessage.put("6dim", b[2]);
                System.out.println("^\t" + jMessage);
              } else {
                System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!! panel connected name: " + 
                  connectedOrNot.toString().replace("con", "") + 
                  " !!!!!!!!!!!!!!!!!!!!!!!!!!!!!" + "\t" + 
                  new Timestamp(System.currentTimeMillis()));
              }
            }
            
            int switch_no = b[0];
            

            Statement statement = JDBCConnection.getConnection().createStatement();
            String sql = "SELECT S.id,S.dimmer_status,S.dimmer_value,S.hide_status,S.lock_status,S.switch_identifier,S.switch_name,S.switch_number,S.switch_status,S.switch_type,R.id as 'roomid', R.homeid as 'homeid',R.room_name FROM switch S LEFT JOIN iotproduct I ON S.iotproductid=I.id  LEFT JOIN panel p ON p.panel_id=I.iot_product_number  LEFT JOIN room R ON R.id=I.roomid   where p.panel_name ='" + 
            

              topicName.toString() + "'" + "AND S.switch_number = " + 
              switch_no;
            
            ResultSet resultSet = statement.executeQuery(sql);
            
            while (resultSet.next()) {
              activityMessage = activityMessage + resultSet.getString("switch_name");
              String roomName = resultSet.getString("room_name");
              String switch_number = resultSet.getString("switch_number");
              int switchid = Integer.parseInt(resultSet.getString("id"));
              
              if (Integer.parseInt(resultSet.getString("lock_status")) == 0) {
                sql = 
                
                  "UPDATE switch SET switch_status = '" + switchStatus + "'," + " dimmer_status = " + "'" + dimmerStatus + "' ," + "dimmer_value=" + "'" + dimmerValue + "'" + " WHERE id = " + switchid;
                
                JDBCConnection.getConnection().createStatement().executeUpdate(sql);
                

                String query = " insert into activity (created_date, message, message_type, image, roomname) values (?, ?, ?, ?, ?)";
                
                PreparedStatement preparedStmt = JDBCConnection.getConnection()
                  .prepareStatement(query, 1);
                preparedStmt.setString(1, LocalCallBack.dateFormat.format(new Date()).toString());
                preparedStmt.setString(2, activityMessage);
                preparedStmt.setString(3, "Physically switch on");
                preparedStmt.setString(4, "");
                preparedStmt.setString(5, roomName);
                int id = 0;
                ResultSet rs = null;
                
                int rowAffected = preparedStmt.executeUpdate();
                if (rowAffected == 1)
                {
                  rs = preparedStmt.getGeneratedKeys();
                  if (rs.next()) {
                    id = rs.getInt(1);
                  }
                }
                
                JSONObject jMessage = new JSONObject();
                jMessage.put("status", "Switch_ON_OFF");
                jMessage.put("userId", "");
                jMessage.put("username", "");
                jMessage.put("message", activityMessage);
                jMessage.put("switchid", switchid);
                jMessage.put("switchnumber", switch_number);
                jMessage.put("switchstatus", switchStatus);
                jMessage.put("dimmerstatus", switchStatus);
                jMessage.put("dimmervalue", dimmerValue);
                jMessage.put("userimage", "");
                jMessage.put("RoomName", roomName.toString());
                jMessage.put("Time", LocalCallBack.dateFormat.format(new Date()));
                jMessage.put("activityid", id);
                Date dateCurrent = new Date();
                jMessage.put("Time", dateCurrent.getDate() + " " + theMonth(dateCurrent.getMonth()) + 
                  " " + 
                  geTime(new StringBuilder(String.valueOf(dateCurrent.getHours())).append(":").append(dateCurrent.getMinutes()).toString()).toString());
                





                MqttMessage refreshBackMessage = new MqttMessage(jMessage.toString().getBytes());
                refreshBackMessage.setQos(2);
                MqttConnection.getMqttConnectionLocal().publish("refreshBack", refreshBackMessage);
                String awsTopic = "refreshIntBackGenieHomeId_" + Utility.getHomeIdOriginal();
                MqttConnection.getMqttConnectionIntenet().publish(awsTopic, refreshBackMessage);
                System.out.println("aws topic back\t" + awsTopic + "\t" + 
                  new Timestamp(System.currentTimeMillis()));
              }
            }
          } catch (SQLException e) {
            e.printStackTrace();
          } catch (MqttPersistenceException e) {
            e.printStackTrace();
          } catch (MqttException e) {
            e.printStackTrace();
          } catch (JSONException e) {
            e.printStackTrace();
          }
        }
        
        public String theMonth(int month) {
          String[] monthNames = { "Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sept", "Oct", 
            "Nov", "Dec" };
          return monthNames[month];
        }
        
        public String geTime(String time) {
          String time1 = "";
          try {
            String _24HourTime = time;
            SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm");
            SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");
            Date _24HourDt = _24HourSDF.parse(_24HourTime);
            time1 = _12HourSDF.format(_24HourDt);
          } catch (Exception e) {
            e.printStackTrace();
          }
          return time1;
        }
      });
    } else {
      System.out.println(
        "Local call back SAME message ignoring ...\t" + new Timestamp(System.currentTimeMillis()));
    }
  }
}
