package com.genie.iot;

import come.genie.iot.utility.ScheduledTaskInternet;
import come.genie.iot.utility.ScheduledTaskLocal;
import come.genie.iot.utility.Utility;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Timestamp;
import java.util.Properties;
import java.util.Timer;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONException;
import org.json.JSONObject;













public class InternetOnOff
  implements MqttCallback
{
  static Logger logger = Logger.getLogger(InternetOnOff.class.getName());
  
  static String topic;
  private static Thread t1;
  MqttClient client = null;
  private static String oldMessageInt = "";
  private static String awsBroker = null;
  private static String localWebServiceUrl = null;
  private static boolean connectedToInternet = false;
  private static String tockenAuth;
  
  public InternetOnOff() {}
  
  public static void main(String[] args) { topic = Utility.getHomeId();
    System.out.println("topic local\t" + topic);
    SwingUtilities.invokeLater(new Runnable()
    {
      public void run() {
        new LocalCallBack().setCallBackLocal();
      }
    });
    new InternetOnOff().startInternet();
    Timer time = new Timer();
    ScheduledTaskLocal taskLocal = new ScheduledTaskLocal();
    time.schedule(taskLocal, 3600000L, 3600000L);
    ScheduledTaskInternet taskInt = new ScheduledTaskInternet();
    time.schedule(taskInt, 3600000L, 3600000L);
  }
  


  private static InternetOnOff currentObject;
  
  private static void createPropertyFile()
  {
    Properties props = new Properties();
    
    props.setProperty("localBroker", "tcp://192.168.0.119:1883");
    props.setProperty("awsBroker", "tcp://geniewish.genieiot.com:1883");
    props.setProperty("physicalSwTopic", "refresh");
    props.setProperty("callBackMobile", "refreshBack");
    props.setProperty("localWebServiceUrl", "http://192.168.0.119:8080/GSmart_final_9jan/");
    
    props.setProperty("localDbUrl", "jdbc:mysql://localhost/geniefinal_1901");
    props.setProperty("localDbUsername", "root");
    props.setProperty("localDbPassword", "root");
    props.setProperty("awsDbUrl", "jdbc:mysql://geniedb.cyab3rb5yzef.us-west-2.rds.amazonaws.com/gsmarthome");
    props.setProperty("awsDbUsername", "GenieDB");
    props.setProperty("awsDbPassword", "GenieDB2016");
    File f = new File("config.properties");
    try {
      OutputStream out = new FileOutputStream(f);
      props.store(out, "This is property file created by Swapnil Bhosale");
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
  
  private static String dologin() throws JSONException, IOException {
    String tocken = null;
    
    String url = "http://192.168.0.119:8080/GSmart_final_9jan/login";
    JSONObject postJsonData = new JSONObject();
    postJsonData.put("username", "demo.jar@gsmarthome.com");
    postJsonData.put("password", "genieDemo2dfoGh");
    



    JSONObject jsonObject = new JSONObject(Utility.postCommanRestApi(url, postJsonData, tockenAuth));
    if (jsonObject.getString("status").equals("SUCCESS"))
      tocken = jsonObject.getJSONObject("result").getString("token");
    return tocken;
  }
  
  public void startInternet() {
    setMqttCallBackInt();
    logger.info("\nSubscribed to topic\t" + topic + "\t" + new Timestamp(System.currentTimeMillis()));
    t1 = new Thread(new Runnable() {
      public void run() {
        synchronized (InternetOnOff.t1) {
          try {
            for (;;) {
              URL url = new URL("http://www.google.com");
              URLConnection connection = url.openConnection();
              connection.connect();
              try {
                Thread.sleep(1000L);
              } catch (InterruptedException e1) {
                e1.printStackTrace();
              }
              if (!InternetOnOff.connectedToInternet) {
                setMqttCallBackInt();
              } else {
                InternetOnOff.connectedToInternet = true;
              }
              Thread.sleep(1500L);
            }
          } catch (Exception e) {
            try { Thread.sleep(1000L);
            } catch (InterruptedException e1) {
              e1.printStackTrace();
            }
            InternetOnOff.connectedToInternet = false;
            setMqttCallBackInt();
          }
          
        }
      }
    });
    t1.start();
  }
  
  private String getAwsBrokerAddress() {
    if (awsBroker == null) {
      Properties prop = new Properties();
      InputStream input = null;
      try {
        input = new FileInputStream("config.properties");
        prop.load(input);
        awsBroker = prop.getProperty("awsBroker");
      } catch (IOException ex) {
        System.out.println("Config file not found\t" + ex);
        
        if (input != null) {
          try {
            input.close();
          } catch (IOException e) {
            e.printStackTrace();
          }
        }
      }
      finally
      {
        if (input != null) {
          try {
            input.close();
          } catch (IOException e) {
            e.printStackTrace();
          }
        }
      }
      return awsBroker;
    }
    return awsBroker;
  }
  
  public void setMqttCallBackInt()
  {
    try
    {
      String clientId = "genieClientId_" + System.currentTimeMillis() % 1000L;
      client = new MqttClient(getAwsBrokerAddress(), clientId);
      MqttConnectOptions connOpts = new MqttConnectOptions();
      connOpts.setCleanSession(true);
      client.connect(connOpts);
      client.setCallback(this);
      client.subscribe(topic);
      System.out.println("\nConnected to aws\t" + new Timestamp(System.currentTimeMillis()));
      connectedToInternet = true;
      System.out.println("\nSubscribed to :\t" + topic + "\t" + new Timestamp(System.currentTimeMillis()));
    } catch (MqttException e) {
      connectedToInternet = false;
      System.out.print(".");
    }
  }
  
  public void connectionLost(Throwable cause) {
    try {
      System.out.println(
        "\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Internet Connection lost <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\t" + 
        new Timestamp(System.currentTimeMillis()));
      setMqttCallBackInt();
      Thread.sleep(10000L);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
  


  public void messageArrived(String topic, final MqttMessage message)
    throws JSONException
  {
    System.out.println("old message\t" + oldMessageInt);
    System.out.println("New message\t" + message);
    if (!oldMessageInt.equals(message.toString())) {
      oldMessageInt = message.toString();
      SwingUtilities.invokeLater(new Runnable()
      {
        public void run()
        {
          try {
            InternetOnOff.tockenAuth = InternetOnOff.access$3();
            System.out.println("tocken: " + InternetOnOff.tockenAuth);
          }
          catch (JSONException localJSONException) {}catch (IOException e) {
            System.out.println(
              "\nUnable to perform login big problem\t" + new Timestamp(System.currentTimeMillis()));
          }
          try {
            String[] data = message.toString().split("\\|");
            String url = getWebServiceUrl();
            JSONObject postJsonData = new JSONObject(data[2]);
            String str1; switch ((str1 = data[0]).hashCode()) {case -838846263:  if (str1.equals("update")) {} break; case 3556498:  if (str1.equals("test")) {} break; case 3649703:  if (str1.equals("wish")) break; break; case 96965648:  if (str1.equals("extra")) {} break; case 109918509:  if (str1.equals("synch")) {} break; case 111574433:  if (!str1.equals("usage")) { break label1843;
                String str2;
                switch ((str2 = data[1]).hashCode()) {case -250636483:  if (str2.equals("switch_list_by_user")) {} break; case -63829508:  if (str2.equals("Internet_Switch_On_Off")) break; break; case -27458417:  if (str2.equals("turnOffAll")) {} break; case 99084752:  if (str2.equals("sync_share_control_data")) {} break; case 132032683:  if (str2.equals("scheduleSwitch")) {} break; case 350560212:  if (str2.equals("hideStatus")) {} break; case 533324926:  if (str2.equals("shareControl")) {} break; case 603706766:  if (str2.equals("updateUserType")) {} break; case 621513552:  if (str2.equals("deleteScheduler")) {} break; case 891925757:  if (str2.equals("lockStatus")) {} break; case 980374916:  if (str2.equals("roomUpdate")) {} break; case 1102896180:  if (str2.equals("changeStatusByRoom")) {} break; case 1275633105:  if (str2.equals("editScheduler")) {} break; case 1296016669:  if (str2.equals("switchUpdate")) {} break; case 1297700491:  if (!str2.equals("Internet_getRoomListByVendorId"))
                  {
                    break label1639;
                    System.out.println(
                      "Internet\tSwitch on off\t" + 
                      Utility.postCommanRestApi(new StringBuilder(String.valueOf(url)).append("switch/changestatus").toString(), 
                      postJsonData, InternetOnOff.tockenAuth) + 
                      "\t" + new Timestamp(System.currentTimeMillis()));
                    return;
                    
                    System.out
                      .println("Internet\tSwitch list by user\t" + 
                      Utility.postCommanRestApi(new StringBuilder(String.valueOf(url)).append("switch/getswitchbyuser").toString(), 
                      postJsonData, InternetOnOff.tockenAuth) + 
                      "\t" + new Timestamp(System.currentTimeMillis()));
                    return;
                    
                    System.out
                      .println("Internet\t Sync share control data\t" + 
                      Utility.postCommanRestApi(new StringBuilder(String.valueOf(url)).append("user/syncShareControlData").toString(), 
                      postJsonData, InternetOnOff.tockenAuth) + 
                      "\t" + new Timestamp(System.currentTimeMillis()));
                    return;
                    
                    System.out.println("Internet\troom update\t" + 
                      Utility.postCommanRestApi(new StringBuilder(String.valueOf(url)).append("room/add").toString(), postJsonData, InternetOnOff.tockenAuth) + "\t" + 
                      new Timestamp(System.currentTimeMillis()));
                    return;
                    
                    System.out.println("Internet\tSwitch update\t" + 
                      Utility.postCommanRestApi(new StringBuilder(String.valueOf(url)).append("switch/edit").toString(), postJsonData, InternetOnOff.tockenAuth) + 
                      "\t" + new Timestamp(System.currentTimeMillis()));
                    return;
                    
                    System.out
                      .println("Internet\tLock status\t" + 
                      Utility.postCommanRestApi(new StringBuilder(String.valueOf(url)).append("switch/changelockstatus").toString(), 
                      postJsonData, InternetOnOff.tockenAuth) + 
                      "\t" + new Timestamp(System.currentTimeMillis()));
                    return;
                    
                    System.out
                      .println("Internet\tHide status\t" + 
                      Utility.postCommanRestApi(new StringBuilder(String.valueOf(url)).append("switch/changehidestatus").toString(), 
                      postJsonData, InternetOnOff.tockenAuth) + 
                      "\t" + new Timestamp(System.currentTimeMillis()));
                    return;
                    
                    System.out
                      .println(
                      "Internet\tschedule Switch\t" + 
                      Utility.postCommanRestApi(new StringBuilder(String.valueOf(url)).append("switch/scheduleswitch").toString(), 
                      postJsonData, InternetOnOff.tockenAuth) + 
                      "\t" + new Timestamp(System.currentTimeMillis()));
                    return;
                    
                    System.out
                      .println("Internet\tedit schedule\t" + 
                      Utility.postCommanRestApi(new StringBuilder(String.valueOf(url)).append("switch/editscheduleswitch").toString(), 
                      postJsonData, InternetOnOff.tockenAuth) + 
                      "\t" + new Timestamp(System.currentTimeMillis()));
                    return;
                    
                    System.out
                      .println("Internet\tdelete Scheduler\t" + 
                      Utility.postCommanRestApi(new StringBuilder(String.valueOf(url)).append("switch/deletescheduleswitch").toString(), 
                      postJsonData, InternetOnOff.tockenAuth) + 
                      "\t" + new Timestamp(System.currentTimeMillis()));
                    return;
                    
                    System.out
                      .println("Internet\tchange Status By Room\t" + 
                      Utility.postCommanRestApi(new StringBuilder(String.valueOf(url)).append("switch/changeStatusByRoom").toString(), 
                      postJsonData, InternetOnOff.tockenAuth) + 
                      "\t" + new Timestamp(System.currentTimeMillis()));
                    return;
                    
                    System.out
                      .println(
                      "Internet\tupdate User Type\t" + 
                      Utility.postCommanRestApi(new StringBuilder(String.valueOf(url)).append("user/updateUserType").toString(), 
                      postJsonData, InternetOnOff.tockenAuth) + 
                      "\t" + new Timestamp(System.currentTimeMillis()));
                    return;
                    
                    System.out.println("Internet\tshare control\t" + 
                      Utility.postCommanRestApi(new StringBuilder(String.valueOf(url)).append("user/shareControl").toString(), postJsonData, InternetOnOff.tockenAuth) + 
                      "\t" + new Timestamp(System.currentTimeMillis()));
                    return;
                    
                    System.out.println("In internet on off\t" + postJsonData);
                    System.out
                      .println(
                      "\nInternet\ttrun off all\t" + 
                      Utility.postCommanRestApi(new StringBuilder(String.valueOf(url)).append("switch/turnOnOffAll").toString(), 
                      postJsonData, InternetOnOff.tockenAuth) + 
                      "\t" + new Timestamp(System.currentTimeMillis()));
                    return;
                  } else {
                    System.out.println("In internet on off get All home statsu\t" + postJsonData);
                    System.out
                      .println(
                      "\nInternet\ttrun off all\t" + 
                      Utility.postCommanRestApi(new StringBuilder(String.valueOf(url)).append("room/getlistbyuser").toString(), 
                      postJsonData, InternetOnOff.tockenAuth) + 
                      "\t" + new Timestamp(System.currentTimeMillis())); }
                  break; }
                label1639:
                System.out.println(
                  "Internet\tDefault code\t" + new Timestamp(System.currentTimeMillis()));
                

                return;
                
                System.out.println(
                  "doing frimware update...\t" + new Timestamp(System.currentTimeMillis()));
                return;
                
                System.out.println("doing synching...\t" + new Timestamp(System.currentTimeMillis()));
                return;
              } else {
                System.out.println("doing usage...\t" + new Timestamp(System.currentTimeMillis()));
                return;
                
                System.out
                  .println("doing extra stuff...\t" + new Timestamp(System.currentTimeMillis()));
                return;
                
                System.out
                  .println("doing extra stuff...\t" + new Timestamp(System.currentTimeMillis())); }
              break; }
            label1843:
            System.out.println(
              "default case\t" + message + "\t" + new Timestamp(System.currentTimeMillis()));
          }
          catch (Exception exception)
          {
            exception.printStackTrace();
          }
        }
        
        private String getWebServiceUrl() {
          if (InternetOnOff.localWebServiceUrl == null) {
            Properties prop = new Properties();
            InputStream input = null;
            try {
              input = new FileInputStream("config.properties");
              prop.load(input);
              InternetOnOff.localWebServiceUrl = prop.getProperty("localWebServiceUrl");
              return InternetOnOff.localWebServiceUrl;
            } catch (IOException ex) {
              ex.printStackTrace();
            } finally {
              if (input != null) {
                try {
                  input.close();
                } catch (IOException e) {
                  e.printStackTrace();
                }
              }
            }
          }
          return InternetOnOff.localWebServiceUrl;
        }
      });
    }
    else {
      System.out.println("\nInternet SAME message ignoring ...\t" + new Timestamp(System.currentTimeMillis()));
    }
  }
  
  public void deliveryComplete(IMqttDeliveryToken token) {
    System.out.println("deliveryComplete");
  }
  
  public static InternetOnOff getCurrentObj() {
    if (currentObject == null) {
      currentObject = new InternetOnOff();
      return currentObject;
    }
    return currentObject;
  }
}
