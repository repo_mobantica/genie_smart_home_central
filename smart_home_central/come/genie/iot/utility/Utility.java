package come.genie.iot.utility;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Utility
{
  private static String homeId;
  private static final String USER_AGENT = "Mozilla/5.0";
  
  public Utility() {}
  
  public static String getHomeId()
  {
    try
    {
      java.sql.Statement statement = JDBCConnection.getConnection().createStatement();
      String sql = "SELECT id FROM home";
      ResultSet resultSet = statement.executeQuery(sql);
      while (resultSet.next()) {
        homeId = "refreshIntGenieHomeId_" + resultSet.getString("id");
      }
      
      resultSet.close();
    } catch (SQLException se) {
      se.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return homeId;
  }
  
  public static String getHomeIdOriginal()
  {
    String id = null;
    try {
      java.sql.Statement statement = JDBCConnection.getConnection().createStatement();
      String sql = "SELECT id FROM home";
      ResultSet resultSet = statement.executeQuery(sql);
      while (resultSet.next()) {
        id = resultSet.getString("id");
      }
      resultSet.close();
    } catch (SQLException se) {
      se.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return id;
  }
  
  public static String postCommanRestApi(String url, org.json.JSONObject jsonData, String tockenAuth)
    throws java.io.IOException
  {
    StringBuffer response = null;
    java.net.URL obj = new java.net.URL(url);
    HttpURLConnection con = (HttpURLConnection)obj.openConnection();
    

    con.setRequestMethod("POST");
    con.setRequestProperty("User-Agent", "Mozilla/5.0");
    con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
    con.setRequestProperty("Content-Type", "application/json");
    con.setRequestProperty("X-AUTH-TOKEN", tockenAuth);
    

    con.setDoOutput(true);
    DataOutputStream wr = new DataOutputStream(con.getOutputStream());
    wr.writeBytes(jsonData.toString());
    wr.flush();
    wr.close();
    



    BufferedReader in = new BufferedReader(new java.io.InputStreamReader(con.getInputStream()));
    
    response = new StringBuffer();
    String output; while ((output = in.readLine()) != null) { String output;
      response.append(output);
    }
    in.close();
    return response.toString();
  }
  
  public static void main(String[] args)
  {
    try
    {
      Process p = Runtime.getRuntime().exec("ls -aF");
      BufferedReader br = new BufferedReader(new java.io.InputStreamReader(p.getInputStream()));
      String s; while ((s = br.readLine()) != null) { String s;
        System.out.println("line: " + s); }
      p.waitFor();
      System.out.println("exit: " + p.exitValue());
      p.destroy();
    }
    catch (Exception localException) {}
  }
}
