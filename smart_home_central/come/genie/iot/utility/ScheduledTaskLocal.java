package come.genie.iot.utility;

import com.genie.iot.LocalCallBack;

public class ScheduledTaskLocal extends java.util.TimerTask {
  public ScheduledTaskLocal() {}
  
  public void run() {
    System.out.println("\n******************************* Re subscribing local callback ***************************** ");
    LocalCallBack.getCurrentObj().setCallBackLocal();
  }
}
