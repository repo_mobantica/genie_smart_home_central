package come.genie.iot.utility;

import com.genie.iot.InternetOnOff;

public class ScheduledTaskInternet extends java.util.TimerTask
{
  public ScheduledTaskInternet() {}
  
  public void run() {
    System.out.println("\n******************************* Re subscribing internet callback ***************************** ");
    InternetOnOff.getCurrentObj().setMqttCallBackInt();
  }
}
