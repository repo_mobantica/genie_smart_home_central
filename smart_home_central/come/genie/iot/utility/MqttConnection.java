package come.genie.iot.utility;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Properties;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

public class MqttConnection
{
  public MqttConnection() {}
  
  private static MqttClient mqttConnectionLocal = null;
  private static MqttClient mqttConnectionInt = null;
  
  public static synchronized MqttClient getMqttConnectionLocal() throws org.eclipse.paho.client.mqttv3.MqttException {
    if (mqttConnectionLocal != null) {
      System.out.println("reusing local connection ...");
      return mqttConnectionLocal;
    }
    Properties prop = new Properties();
    InputStream input = null;
    try {
      input = new java.io.FileInputStream("config.properties");
      prop.load(input);
      MemoryPersistence persistence = new MemoryPersistence();
      String clientId = System.currentTimeMillis() / 10000L;
      mqttConnectionLocal = new MqttClient(prop.getProperty("localBroker"), clientId, persistence);
      MqttConnectOptions connOpts = new MqttConnectOptions();
      connOpts.setCleanSession(true);
      mqttConnectionLocal.connect(connOpts);
      System.out.println("Connected...");
    } catch (IOException ex) {
      ex.printStackTrace();
      
      if (input != null) {
        try {
          input.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
    finally
    {
      if (input != null) {
        try {
          input.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
    return mqttConnectionLocal;
  }
  
  public static synchronized MqttClient getMqttConnectionIntenet() throws org.eclipse.paho.client.mqttv3.MqttException
  {
    if (mqttConnectionInt != null) {
      System.out.println("reusing internet connection ...");
      return mqttConnectionInt;
    }
    Properties prop = new Properties();
    InputStream input = null;
    try {
      input = new java.io.FileInputStream("config.properties");
      prop.load(input);
      MemoryPersistence persistence = new MemoryPersistence();
      String clientId = System.currentTimeMillis() / 8278L;
      mqttConnectionInt = new MqttClient(prop.getProperty("awsBroker"), clientId, 
        persistence);
      MqttConnectOptions connOpts = new MqttConnectOptions();
      connOpts.setCleanSession(true);
      mqttConnectionInt.connect(connOpts);
      System.out.println("Connected...");
    } catch (IOException ex) {
      ex.printStackTrace();
      
      if (input != null) {
        try {
          input.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
    finally
    {
      if (input != null) {
        try {
          input.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
    return mqttConnectionInt;
  }
}
