package come.genie.iot.utility;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class JDBCConnection
{
  static Connection connection = null;
  
  public JDBCConnection() {}
  
  public static Connection getConnection() { if (connection == null) {
      try {
        Properties prop = new Properties();
        InputStream input = null;
        try {
          Class.forName("com.mysql.jdbc.Driver");
          input = new java.io.FileInputStream("config.properties");
          prop.load(input);
          connection = DriverManager.getConnection(prop.getProperty("localDbUrl"), 
            prop.getProperty("localDbUsername"), prop.getProperty("localDbPassword"));
        } catch (IOException ex) {
          ex.printStackTrace();
          


          if (input == null)
            break label166;
          try { input.close();
          } catch (IOException e) {
            e.printStackTrace();
          }
        }
        catch (ClassNotFoundException e)
        {
          e.printStackTrace();
          
          if (input == null)
            break label166;
          try { input.close();
          } catch (IOException e) {
            e.printStackTrace();
          }
        }
        finally
        {
          if (input != null) {
            try {
              input.close();
            } catch (IOException e) {
              e.printStackTrace();
            }
          }
        }
        try
        {
          input.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
        





        return connection;
      }
      catch (SQLException exception)
      {
        System.out.println(exception);
      }
    }
  }
}
