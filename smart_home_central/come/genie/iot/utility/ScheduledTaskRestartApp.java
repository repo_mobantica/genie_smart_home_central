package come.genie.iot.utility;

import java.io.IOException;

public class ScheduledTaskRestartApp extends java.util.TimerTask
{
  public ScheduledTaskRestartApp() {}
  
  public void run()
  {
    System.out.println("Restarting java jar\t" + new java.sql.Timestamp(System.currentTimeMillis()));
    javax.swing.SwingUtilities.invokeLater(new Runnable()
    {
      public void run() {
        ProcessBuilder processBuilder = new ProcessBuilder(new String[] { "sudo ./res.sh" });
        try {
          processBuilder.start();
          System.out.println("jar restarted successfully\t" + new java.sql.Timestamp(System.currentTimeMillis()));
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    });
    System.exit(0);
  }
}
