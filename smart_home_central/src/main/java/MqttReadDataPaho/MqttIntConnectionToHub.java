package MqttReadDataPaho;

import javax.swing.SwingUtilities;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;

import com.gsmarthome.constants.Constants;
import com.gsmarthome.repository.PanelRepository;
import com.gsmarthome.repository.SwitchRepository;
import com.gsmarthome.repository.UserDetailsRepository;
import com.gsmarthome.service.SwitchService;


public class MqttIntConnectionToHub implements MqttCallback {
	String subscribePanel;
	private String clientId = "119";
	private static MqttClient mqttConnection = null;
	private static String broker = "tcp://103.12.211.52:1883";
	//private static String broker = "tcp://192.168.0.18:1883";
	private static MqttIntConnectionToHub currObj = null;

	@Autowired
	SwitchService switchService;

	@Autowired
	SwitchRepository switchRepository;
	@Autowired
	UserDetailsRepository userDetailsRepository;
	@Autowired
	PanelRepository panelRepository;

	public MqttIntConnectionToHub() {
	}

	public MqttIntConnectionToHub(String csubscribePanel) {
		subscribePanel = csubscribePanel;
	}

	public MqttClient getMqttConnection() {

		if (mqttConnection != null && mqttConnection.isConnected()) {
			try {
				mqttConnection.disconnect();
			}catch (Exception e) {
				// TODO: handle exception
			}
			getObj().getMqttConnection();
			System.out.println("reusing connection...to Genie Central");
			return mqttConnection;
		} else 

		{
			try {

				MemoryPersistence persistence = new MemoryPersistence();
				mqttConnection = new MqttClient(broker, clientId, persistence);
				MqttConnectOptions connOpts = new MqttConnectOptions();
				connOpts.setKeepAliveInterval(10);
				connOpts.setAutomaticReconnect(true);
				connOpts.setCleanSession(true);

				mqttConnection.connect(connOpts);

				// String topicFilters[]= {"refreshIntGenieHome", "updateStatus1"};

				mqttConnection.subscribe("refreshIntGenieHome");
				// mqttConnection.subscribe("updateStatus1");

				mqttConnection.setCallback(this);

				System.out.println("Connected");
				return mqttConnection;
			} catch (MqttException exception) {
				try {
					System.out.println("MqttException exception = "+exception);
					System.out.println("unable to connect Genie Central");
					System.out.println("trying...");
					Thread.sleep(3000);
					MqttIntConnectionToHub.getObj().getMqttConnection();
					exception.printStackTrace();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				return mqttConnection;
			}
		}
	}


	public static MqttIntConnectionToHub getObj() {
		if (currObj == null) {
			currObj = new MqttIntConnectionToHub();
			return currObj;
		} else
			return currObj;
	}

	@Override
	public void connectionLost(Throwable arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void messageArrived(String arg0, MqttMessage mqttmessage) throws Exception {
		// TODO Auto-generated method stub

		System.out.println("Topic name ="+arg0);
		System.out.println("mqttmessage= "+mqttmessage);


		String home_id="1";
		String msg1 = "";
		try
		{
			msg1 = "" + mqttmessage;

		} catch (Exception e) {
			msg1 = "";
		}

		String[] msg2 = msg1.split("/");

		if (msg2[0].equalsIgnoreCase("wish"))
		{

			if(msg2.length!=1)
			{
				if ((msg2[1].equalsIgnoreCase("Internet_Switch_On_Off")) || (msg2[1].equalsIgnoreCase("Local_Switch_On_Off")))
				{
					home_id=msg2[8];
					String savestatus="";
					int length=msg2[5].length();
					savestatus="$["+msg2[3]+""+msg2[4]+"";
					if(length==1)
					{
						savestatus=savestatus+"0"+msg2[5]+""+"]"+msg2[7]+"/"+home_id+"";
					}
					else
					{
						savestatus=savestatus+""+msg2[5]+""+"]"+msg2[7]+"/"+home_id+"";	
					}
					/*
					try {
						JdbcConnection jdbcconnection=new JdbcConnection();
						jdbcconnection.saveSwitchStatus(savestatus);   
					}
					catch (Exception e) {
						// TODO: handle exception
					}
					 */
					String topicName="refreshIntGenieHomeId_"+""+home_id;
					MqttMessage message = new MqttMessage((mqttmessage.toString()).getBytes());
					message.setQos(Constants.QoS_EXACTLY_ONCE);

					SwingUtilities.invokeLater(new Runnable() {
						@Override
						public void run() {
							try {
								System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> aws topic 2:\t"+topicName);
								MqttIntConnectionToHub.getObj().getMqttConnection().publish(topicName, message);

							} catch (MqttException e) {
								System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> problem:\t"+topicName);
								e.printStackTrace();
							}
						}
					});

				}

			}
		}
		else
		{
			try {
				JdbcConnection jdbcconnection=new JdbcConnection();
				jdbcconnection.saveSwitchStatus(msg1);   
			}
			catch (Exception e) {}
			String[] msg23 = msg1.split("/");

			String updateStatus="updateStatus2_"+""+msg23[2];
			String msg;
			if(msg23.length==3)
			{
				msg=msg23[0].toString()+"/"+msg23[1];
			}
			else
			{
				msg=msg23[0].toString();
			}
			MqttMessage refreshMessage = new MqttMessage(msg.toString().getBytes());

			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					try {
						System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> aws topic 2:\t"+updateStatus);
						MqttIntConnectionToHub.getObj().getMqttConnection().publish(updateStatus, refreshMessage);
					} catch (MqttException e) {
						System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> problem:\t"+updateStatus);
						e.printStackTrace();
					}
				}
			});
		}

	}
}
