package com.gsmarthome.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.gsmarthome.dto.AddUpdateRoomDTO;
import com.gsmarthome.dto.RoomListByUserDTO;
import com.gsmarthome.dto.UpdateHideRoomDTO;
import com.gsmarthome.dto.getReponseDTO;


public interface RoomService {

	getReponseDTO addUpdateRoom(AddUpdateRoomDTO dto);
	
	List<RoomListByUserDTO> getRoomListByVendorId(Long vendorId,String messageFrom);
		
	Boolean deleteRoom(Long roomId);
	
	Boolean changeRoomHideStatus(UpdateHideRoomDTO updateHideRoomDTO) throws Exception;

}
