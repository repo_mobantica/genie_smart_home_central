package com.gsmarthome.service;

import java.util.List;

import com.gsmarthome.dto.ActivityDTO;
import com.gsmarthome.dto.NotificationHistoryDTO;
import com.gsmarthome.dto.NotificationHistoryResponseDTO;

public interface NotificationService {
	 
	List<NotificationHistoryResponseDTO> getNotificationHistory(NotificationHistoryDTO notificationHistoryDTO );

	void getActivityList(ActivityDTO activityDTO);
}
