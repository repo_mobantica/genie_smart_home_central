package com.gsmarthome.service;

import com.gsmarthome.dto.HomeArmANDBlockResponseDTO;
import com.gsmarthome.dto.HomeDTO;
import com.gsmarthome.dto.HomeLockCodeResponseDTO;
import com.gsmarthome.dto.SwitchIdDTO;

public interface HomeService {
	
	HomeArmANDBlockResponseDTO armHome(HomeDTO homeDTO) throws Exception;

	HomeArmANDBlockResponseDTO blockHome(HomeDTO homeDTO) throws Exception;
	
}
