package com.gsmarthome.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.gsmarthome.dto.RemoteIRRequestDTO;
import com.gsmarthome.dto.RemoteIRResponseDTO;
import com.gsmarthome.dto.ResponseDTO;
import com.gsmarthome.entity.RemoteDetails;

@Service
public interface IGenieRemoteService {
	
	public ResponseDTO<RemoteIRResponseDTO> remoteIRStorage(RemoteIRRequestDTO remoteIRReqDTO);
	
	public ResponseDTO<List<RemoteDetails>> getAllIrRemote(RemoteIRRequestDTO remoteIRReqDTO);
	
}
