package com.gsmarthome.service;

import java.util.List;

import com.gsmarthome.dto.AddUpdateSwitchDTO;
import com.gsmarthome.dto.ChangeCurtainStatusDTO;
import com.gsmarthome.dto.ChangeHideStatusDTO;
import com.gsmarthome.dto.ChangeHideStatusResponseDTO;
import com.gsmarthome.dto.ChangeLockStatusDTO;
import com.gsmarthome.dto.ChangeLockStatusResponseDTO;
import com.gsmarthome.dto.ChangeStatusByRoomDTO;
import com.gsmarthome.dto.ChangeStatusByRoomResponseDTO;
import com.gsmarthome.dto.ChangeSwitchStatusDTO;
import com.gsmarthome.dto.ChangeSwitchStatusResponseDTO;
import com.gsmarthome.dto.SwitchListByRoomDTO;
import com.gsmarthome.entity.Room;

public interface SwitchService {

	Room addEditSwitch(AddUpdateSwitchDTO switchDTO);
	
	Boolean deleteSwitch(Long switchId);
	
	Boolean changeGenieColor(ChangeSwitchStatusDTO changeSwitchStatusDTO);
	
	ChangeSwitchStatusResponseDTO changeStatus(ChangeSwitchStatusDTO changeSwitchStatusDTO);
	
	ChangeSwitchStatusResponseDTO changeCurtainStatus(ChangeCurtainStatusDTO changeCurtainStatusDTO);
	
	List<SwitchListByRoomDTO> getSwitchListByRoom(Long roomId);
	
	List<SwitchListByRoomDTO> getSwitchListByUserId(Long userId, String messageFrom);

	ChangeLockStatusResponseDTO changeLockStatus(ChangeLockStatusDTO changeLockStatusDTO);

	ChangeHideStatusResponseDTO changeHidestatus(ChangeHideStatusDTO changeHideStatusDTO);

	List<ChangeStatusByRoomResponseDTO>  changeStatusByRoom(ChangeStatusByRoomDTO changeStatusByRoomDTO );

}
