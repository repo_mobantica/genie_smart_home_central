package com.gsmarthome.service;

import com.gsmarthome.dto.EnquiryDTO;
import com.gsmarthome.dto.getReponseDTO;

public interface EnquiryService {

	getReponseDTO addEnquiry(EnquiryDTO dto);

}
