package com.gsmarthome.service;

import com.gsmarthome.dto.ScheduleProfileDTO;
import com.gsmarthome.dto.ScheduleProfileResponseDTO;
import com.gsmarthome.dto.ScheduleSwitchDTO;
import com.gsmarthome.dto.ScheduleSwitchResponseDTO;

public interface ScheduleService {

	ScheduleSwitchResponseDTO scheduleSwitch(ScheduleSwitchDTO dto) throws Exception;
	
	ScheduleSwitchResponseDTO editScheduleSwitch(ScheduleSwitchDTO scheduleSwitchDTO)throws Exception;

	Boolean deleteScheduleSwitch(ScheduleSwitchDTO scheduleSwitchDTO)throws Exception;
	
	Boolean getScheduleSwitchList(ScheduleSwitchDTO scheduleSwitchDTO)throws Exception;
	
	ScheduleProfileResponseDTO scheduleProfile(ScheduleProfileDTO dto) throws Exception;
	
	ScheduleProfileResponseDTO deleteScheduleProfile(ScheduleProfileDTO scheduleProfileDTO) throws Exception;
	
	ScheduleProfileResponseDTO editScheduleProfile(ScheduleProfileDTO scheduleProfileDTO) throws Exception;
}
