package com.gsmarthome.service;

import java.util.List;

import com.gsmarthome.dto.ChangeStatusByHomeDTO;
import com.gsmarthome.dto.ChangeStatusByRoomResponseDTO;
import com.gsmarthome.dto.ProfileRequestDTO;
import com.gsmarthome.dto.ProfileResponseDTO;

public interface ProfileService {

	List<ChangeStatusByRoomResponseDTO> profileOnOff(ChangeStatusByHomeDTO changeStatusByHomeDTO)throws Exception;
	
	ProfileResponseDTO addProfileMode(ProfileRequestDTO profileRequestDTO)throws Exception;
	
	ProfileResponseDTO editProfileMode(ProfileRequestDTO profileRequestDTO)throws Exception;
	
	ProfileResponseDTO deleteProfileMode(ProfileRequestDTO profileRequestDTO)throws Exception;
	
	List<ProfileResponseDTO> getProfileList(ProfileRequestDTO profileRequestDTO)throws Exception;
	
}
