package com.gsmarthome;

import org.apache.log4j.Logger;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class IndexController implements ErrorController {

	static Logger logger = Logger.getLogger(IndexController.class.getName());
	
    private static final String PATH = "/error";

    @RequestMapping(value = PATH)
    public String error() {

    	System.out.println("Path error");
		logger.info("*******************Error page********************");
        return "greeting";
    }
    
	@Override
	public String getErrorPath() {
		return PATH;
	}
	
	
	@RequestMapping(value = "/" , method = RequestMethod.GET)
    public String home(){
		
		logger.info("*******************Home page********************");
        return "greeting";
    }

}
