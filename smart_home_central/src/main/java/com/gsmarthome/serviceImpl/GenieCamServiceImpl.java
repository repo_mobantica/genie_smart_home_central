package com.gsmarthome.serviceImpl;

import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;
import com.gsmarthome.constants.Constants;
import com.gsmarthome.dto.GenieCamRequestDTO;
import com.gsmarthome.dto.GenieCamResponseDTO;
import com.gsmarthome.dto.ResponseDTO;
import com.gsmarthome.entity.GenieCam;
import com.gsmarthome.entity.UserDetails;
import com.gsmarthome.repository.UserDetailsRepository;
import com.gsmarthome.security.repository.GenieCamRepository;
import com.gsmarthome.service.GenieCamService;

import MqttReadDataPaho.MqttIntConnectionToHub;


@Component
public class GenieCamServiceImpl implements GenieCamService {

	static Logger logger = Logger.getLogger(GenieCamServiceImpl.class.getName());

	@Autowired
	UserDetailsRepository userDetailsRepository;
	
	@Autowired
	GenieCamRepository genieCamRepository;
	
	@Value("${API_STATUS_SUCCESS}")
	private String API_STATUS_SUCCESS;
	
	@Value("${API_STATUS_FAILURE}")
	private String API_STATUS_FAILURE;

	@Value("${USER_DETAILS_NOT_PRESENT}")
	private String USER_DETAILS_NOT_PRESENT;

	@Override
	public ResponseDTO<List<GenieCam>> getAllGenieCamDetails(GenieCamRequestDTO genieCamRequestDTO) {
		JSONObject jMessage = new JSONObject();
		
		UserDetails userDetails = userDetailsRepository.findOne(genieCamRequestDTO.getUserId());
		
		if(userDetails != null){
			try {
				jMessage.put(Constants.MESSAGE_FROM, genieCamRequestDTO.getMessageFrom().toString());
				jMessage.put(Constants.USER_ID, userDetails.getId().toString());
				
			} catch (JSONException e1) {
				logger.error("Get All Genie Cam Details, Genie Camera JSON Error. Genie Camera Id: "+genieCamRequestDTO.getGenieCamId());
				e1.printStackTrace();
			}
			
			String publicTopic = "refreshIntGenieHomeId_" + userDetails.getHome().getId().toString();
			MqttMessage message = new MqttMessage(("wish|" + "get_all_genie_cam|" + jMessage.toString()).getBytes());
			message.setQos(Constants.QoS_EXACTLY_ONCE);
			/*try {
				MqttIntConnectionToHub.getObj().getMqttConnection().publish(publicTopic, message);
			} catch (MqttException e) {
				logger.error("Get All Genie Cam Details, Genie Camera MQTT Error. Genie Camera Id: "+genieCamRequestDTO.getGenieCamId());
				e.printStackTrace();
			}*/
		}
		
		return null;
	}

	@Override
	public ResponseDTO<GenieCamResponseDTO> addGenieCamDetails(GenieCamRequestDTO genieCamRequestDTO) {
		GenieCamResponseDTO genieCamResponseDTO = new GenieCamResponseDTO();
		JSONObject jMessage = new JSONObject();
		
		UserDetails userDetails = userDetailsRepository.findOne(genieCamRequestDTO.getUserId());
		
		if(userDetails != null){
			try {
				jMessage.put(Constants.GENIE_CAM_ID, genieCamResponseDTO.getGenieCamId());
				jMessage.put(Constants.GENIE_CAM_NAME, genieCamResponseDTO.getCamName());
				jMessage.put(Constants.GENIE_LOCAL_IP, genieCamResponseDTO.getLocalIP().toString());
				jMessage.put(Constants.GENIE_LOCAL_PORT, genieCamResponseDTO.getLocalPort().toString());
				jMessage.put(Constants.GENIE_INTERNET_IP, genieCamResponseDTO.getInternetIP().toString());
				jMessage.put(Constants.GENIE_INTERNET_PORT, genieCamResponseDTO.getInternetPort().toString());
				jMessage.put(Constants.MESSAGE_FROM, genieCamRequestDTO.getMessageFrom().toString());
				jMessage.put(Constants.USER_ID, genieCamRequestDTO.getUserId().toString());
				
				
			} catch (JSONException e1) {
				logger.error("Add Genie Cam Details, Genie Camera JSON Error. Genie Camera Id: "+genieCamRequestDTO.getGenieCamId());
				e1.printStackTrace();
			}
			
			String publicTopic = "refreshIntGenieHomeId_" + userDetails.getHome().getId().toString();
			MqttMessage message = new MqttMessage(("wish|" + "add_genie_cam|" + jMessage.toString()).getBytes());
			message.setQos(Constants.QoS_EXACTLY_ONCE);
			/*try {
				MqttIntConnectionToHub.getObj().getMqttConnection().publish(publicTopic, message);
			} catch (MqttException e) {
				logger.error("Get All Genie Cam Details, Genie Camera MQTT Error. Genie Camera Id: "+genieCamRequestDTO.getGenieCamId());
				e.printStackTrace();
			}*/
		}
	
		return null;
	}

	@Override
	public ResponseDTO<GenieCamResponseDTO> deleteGenieCamDetails(GenieCamRequestDTO genieCamRequestDTO) {
		
		JSONObject jMessage = new JSONObject();
		UserDetails userDetails = userDetailsRepository.findOne(genieCamRequestDTO.getUserId());
		
		if(userDetails != null){
			try {
				jMessage.put(Constants.GENIE_CAM_ID, genieCamRequestDTO.getGenieCamId());
				jMessage.put(Constants.MESSAGE_FROM, genieCamRequestDTO.getMessageFrom().toString());
				jMessage.put(Constants.USER_ID, genieCamRequestDTO.getUserId().toString());
			} catch (JSONException e1) {
				logger.error("Add Genie Cam Details, Genie Camera JSON Error. Genie Camera Id: "+genieCamRequestDTO.getGenieCamId());
				e1.printStackTrace();
			}
			
			String publicTopic = "refreshIntGenieHomeId_" + userDetails.getHome().getId().toString();
			MqttMessage message = new MqttMessage(("wish|" + "del_genie_cam|" + jMessage.toString()).getBytes());
			message.setQos(Constants.QoS_EXACTLY_ONCE);
			/*try {
				MqttIntConnectionToHub.getObj().getMqttConnection().publish(publicTopic, message);
			} catch (MqttException e) {
				logger.error("Get All Genie Cam Details, Genie Camera MQTT Error. Genie Camera Id: "+genieCamRequestDTO.getGenieCamId());
				e.printStackTrace();
			}*/
		}else{
			return new ResponseDTO<GenieCamResponseDTO>(API_STATUS_FAILURE, null, USER_DETAILS_NOT_PRESENT);
		}
		
		return null;
	}
	
	
}
