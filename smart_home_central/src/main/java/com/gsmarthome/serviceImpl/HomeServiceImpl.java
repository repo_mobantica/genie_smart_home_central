package com.gsmarthome.serviceImpl;

import org.apache.log4j.Logger;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.amazonaws.util.json.JSONObject;
import com.gsmarthome.constants.Constants;
import com.gsmarthome.dto.HomeArmANDBlockResponseDTO;
import com.gsmarthome.dto.HomeDTO;
import com.gsmarthome.entity.Home;
import com.gsmarthome.entity.UserDetails;
import com.gsmarthome.repository.HomeRepository;
import com.gsmarthome.repository.SwitchRepository;
import com.gsmarthome.repository.UserDetailsRepository;
import com.gsmarthome.service.HomeService;
import com.gsmarthome.service.NotificationService;

import MqttReadDataPaho.MqttIntConnectionToHub;

@Component
public class HomeServiceImpl implements HomeService {

	static Logger logger = Logger.getLogger(HomeServiceImpl.class.getName());

	@Autowired
	HomeRepository homeRepository;
	
	@Autowired
	UserDetailsRepository userDetailsRepository;
	
	@Autowired
	SwitchRepository switchRepository;
	
	@Autowired
	NotificationService notificationService;
	
	@Override
	public HomeArmANDBlockResponseDTO armHome(HomeDTO homeDTO) throws Exception{
		
		JSONObject jMessage = new JSONObject();
		
		UserDetails userDetails = userDetailsRepository.findOne(homeDTO.getUserId());
		
		if(userDetails == null)
			throw new Exception("User Details not found");	
		

		jMessage.put(Constants.USER_ID, homeDTO.getUserId().toString());
		jMessage.put(Constants.IS_ARMED, homeDTO.getIsArmed().toString());
		jMessage.put(Constants.MESSAGE_FROM, homeDTO.getMessageFrom());
		String publicTopic = "refreshIntGenieHomeId_" + userDetails.getHome().getId().toString();
		// refreshBackMessage to mobile or to other thin end clients
		MqttMessage message = new MqttMessage(("wish|" + "arm_home|" + jMessage.toString()).getBytes());
		message.setQos(Constants.QoS_EXACTLY_ONCE);
		//MqttIntConnectionToHub.getObj().getMqttConnection().publish(publicTopic, message);
		
		return null;
	}

	@Override
	public HomeArmANDBlockResponseDTO blockHome(HomeDTO homeDTO) throws Exception {
		
		HomeArmANDBlockResponseDTO homeArmANDBlockedResponseDTO = new HomeArmANDBlockResponseDTO();
		
		Home objHome = null;
		
		UserDetails userDetails = userDetailsRepository.findOne(homeDTO.getUserId());
		
		if(userDetails == null)
			throw new Exception("User Details not found");
		
		if(homeDTO != null){
			objHome = homeRepository.findOne(userDetails.getHome().getId());
			logger.info("Fetched the Home Details");
		}
		
		if(objHome != null){ //1 equals successfully saved
			
			//notificationService.sendNotificationMessage(userDetails, "your home is Blocked, due to payment issue. Please contact Customer care to retain the services");
			
			homeArmANDBlockedResponseDTO.setHomeId(objHome.getId().toString());
			homeArmANDBlockedResponseDTO.setHomeName(objHome.getHomeName());
			homeArmANDBlockedResponseDTO.setIsBlocked(objHome.getIsBlocked().toString());
		}
		
		return homeArmANDBlockedResponseDTO;
	}

}
