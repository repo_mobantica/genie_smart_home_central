package com.gsmarthome.serviceImpl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;
import com.gsmarthome.constants.Constants;
import com.gsmarthome.dto.AddUpdateSwitchDTO;
import com.gsmarthome.dto.ChangeCurtainStatusDTO;
import com.gsmarthome.dto.ChangeHideStatusDTO;
import com.gsmarthome.dto.ChangeHideStatusResponseDTO;
import com.gsmarthome.dto.ChangeLockStatusDTO;
import com.gsmarthome.dto.ChangeLockStatusResponseDTO;
import com.gsmarthome.dto.ChangeStatusByRoomDTO;
import com.gsmarthome.dto.ChangeStatusByRoomResponseDTO;
import com.gsmarthome.dto.ChangeSwitchStatusDTO;
import com.gsmarthome.dto.ChangeSwitchStatusResponseDTO;
import com.gsmarthome.dto.SwitchListByRoomDTO;
import com.gsmarthome.entity.Activity;
import com.gsmarthome.entity.Command;
import com.gsmarthome.entity.IOTProduct;
import com.gsmarthome.entity.Panel;
import com.gsmarthome.entity.Room;
import com.gsmarthome.entity.Switch;
import com.gsmarthome.entity.SwitchType;
import com.gsmarthome.entity.UserDetails;
import com.gsmarthome.repository.ActivityRepository;
import com.gsmarthome.repository.CommandRepository;
import com.gsmarthome.repository.IOTProductRepository;
import com.gsmarthome.repository.NotificationRepository;
import com.gsmarthome.repository.PanelRepository;
import com.gsmarthome.repository.RoomRepository;
import com.gsmarthome.repository.ScheduleSwitchRepository;
import com.gsmarthome.repository.SwitchRepository;
import com.gsmarthome.repository.SwitchTypeRepository;
import com.gsmarthome.repository.UserDetailsRepository;
import com.gsmarthome.service.SwitchService;
import com.gsmarthome.util.Utility;

import MqttReadDataPaho.MqttIntConnectionToHub;

@Component
public class SwitchServiceImpl implements SwitchService {
	/* Get actual class name to be printed on */
	final static Logger logger = Logger.getLogger(SwitchServiceImpl.class.getName());

	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:00");
	private static final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:00");

	@Autowired
	UserDetailsRepository userDetailsRepository;

	@Autowired
	RoomRepository roomRepository;

	@Autowired
	SwitchRepository switchRepository;

	@Autowired
	SwitchTypeRepository switchTypeRepository;

	@Autowired
	IOTProductRepository iOTProductRepository;

	@Autowired
	ScheduleSwitchRepository scheduleSwitchRepository;

	@Autowired
	NotificationRepository notificationRepository;

	@Autowired
	PanelRepository panelRepository;

	@Autowired
	CommandRepository commandRepository;

	@Autowired
	ActivityRepository activityRepository;

	@Value("${SWITCH_NAME_ALREADY_PRESENT}")
	private String SWITCH_NAME_ALREADY_PRESENT;

	@Value("${IMAGE_LOCATION}")
	private String IMAGE_LOCATION;
	@Override
	public Room addEditSwitch(AddUpdateSwitchDTO switchDTO) {
		
		
		Activity activity = new Activity();
		Activity addactivity = new Activity();
		
		Date date = new Date();		
		
		try {
			if (switchDTO.getSwitchId() != null) {
				Switch fetchedSwitch = switchRepository.findOne(switchDTO.getSwitchId());

				logger.info("Add Edit Switch method, fetched Switch Details: "+"Switch ID: "+fetchedSwitch.getId());
				
				String oldswitchname = fetchedSwitch.getSwitchName();
				long userId = switchDTO.getUserId();
				
				UserDetails userDetails = userDetailsRepository.findOne(userId);
				
				logger.info("User details fetched "+"User ID: "+userId);
				
				if (switchDTO.getSwitchName() != null)
					fetchedSwitch.setSwitchName(switchDTO.getSwitchName());
				if (switchDTO.getSwitchType() != null)
					fetchedSwitch.setSwitchType(switchDTO.getSwitchType());
				/*if (switchDTO.getDimmerStatus() != null)
					fetchedSwitch.setDimmerStatus(switchDTO.getDimmerStatus());*/
				if (switchDTO.getDimmerValue() != null)
					fetchedSwitch.setDimmerValue(switchDTO.getDimmerValue());
				if (switchDTO.getSwitchStatus() != null)
					fetchedSwitch.setSwitchStatus(switchDTO.getSwitchStatus());
				if (switchDTO.getSwitchidentifier() != null)
					fetchedSwitch.setSwitchIdentifier(switchDTO.getSwitchidentifier());
				/*
				if (switchDTO.getSwitchImageId() != null)
					fetchedSwitch.setSwitchImageId(switchDTO.getSwitchImageId());
*/
				switchRepository.save(fetchedSwitch);
				
				logger.debug("addEditSwitch() - Saved Switch Details. "+"Switch ID: "+fetchedSwitch.getId()
				+"Switch Name: "+fetchedSwitch.getSwitchName());
				
				if (userDetails.getImage() == null) {
					activity.setImage("");
				} else {
					activity.setImage(userDetails.getId()+ ".png");
					//activity.setImage(IMAGE_LOCATION + "/" + userDetails.getId() + ".png");
				}
				activity.setMessage(userDetails.getFirstName().toString() + " updated switch name from " + oldswitchname
						+ " to " + fetchedSwitch.getSwitchName());
				activity.setMessageType("");
				activity.setRoomname(fetchedSwitch.getIotProduct().getRoom().getRoomName().toString());
				dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
				activity.setCreated_date(dateFormat.format(new Date()).toString());
				activity.setMessageFrom(switchDTO.getMessageFrom().toString());
				activity.setHomeid(userDetails.getHome().getId());
				addactivity = activityRepository.save(activity);
			
				return fetchedSwitch.getIotProduct().getRoom();
				
			} else {
				if (switchDTO.getIotProductId() != null) {
					
					IOTProduct iotProduct = iOTProductRepository.findOne(switchDTO.getIotProductId());

					Switch newSwitch = new Switch(switchDTO.getSwitchidentifier(), switchDTO.getSwitchType(),
							switchDTO.getDimmerStatus(), switchDTO.getDimmerValue(), switchDTO.getSwitchStatus(),
							switchDTO.getSwitchName());

					newSwitch.setIotProduct(iotProduct);

					iotProduct.getSwitchList().add(newSwitch);

					switchRepository.save(newSwitch);

					logger.debug("Add Edit Switch method, Saved Switch Details. "+"Switch ID: "+newSwitch.getId()
					+"Switch Name: "+newSwitch.getSwitchName());
					
					return iotProduct.getRoom();
				} else {
					
				}
			}
		} catch (Exception ex) {
			logger.debug("Add Edit Switch method, Saved Switch Details. "+"Switch ID: "+ switchDTO.getSwitchId()
			+"Switch Name: "+ switchDTO.getSwitchName());
			
			ex.printStackTrace();
			
		}
		return null;
		
		
	}

	@Override
	public Boolean deleteSwitch(Long switchId) {
		switchRepository.delete(switchId);
		return true;
	}

	@Override
	public List<SwitchListByRoomDTO> getSwitchListByRoom(Long roomId) {
		
		Room room = roomRepository.findOne(roomId);
		logger.info("getSwitchListByRoom method, room repository Accessed");

		Panel panel = new Panel();

		List<Switch> switchList = new ArrayList<Switch>();
		List<SwitchListByRoomDTO> returnList = new ArrayList<SwitchListByRoomDTO>();

		for (IOTProduct iotProduct : room.getProductList())
			switchList.addAll(iotProduct.getSwitchList());

		for (Switch switch1 : switchList) {
			
			SwitchType switchType = switchTypeRepository.findOne(Long.parseLong(switch1.getSwitchType()));


			int panelId = switch1.getIotProduct().getIotProductNumber();
			panel = panelRepository.findByPanelId(panelId);
			
			
			returnList.add(new SwitchListByRoomDTO(switch1.getId().toString(), switch1.getSwitchName(),switch1.getSwitchNumber(), switch1.getSwitchType(),
					switch1.getDimmerStatus(), switch1.getDimmerValue(), switch1.getSwitchStatus(),
					switchType.getOnImage(), switchType.getOffImage(), switch1.getLockStatus(), switch1.getHideStatus(),
					switch1.getSwitchIdentifier(), panel.getPanelName(), ""+switch1.getIotProduct().getId()));
		}
		
		return returnList;
	}
	
	
	
	@Override
	public List<SwitchListByRoomDTO> getSwitchListByUserId(Long userId,String messageFrom) {
	
		Date dateCurrent = new Date();
		JSONObject jMessage = new JSONObject();
		
		UserDetails userDetails = userDetailsRepository.findOne(userId);
		logger.info("getSwitchListByUserId method, user details fetched");
		
		Long homeId = userDetails.getHome().getId();
		
		try{
			jMessage.put(Constants.USER_ID, userId.toString()); 
			jMessage.put(Constants.TIME, ""+dateCurrent.getTime());
			jMessage.put(Constants.MESSAGE_FROM, messageFrom);
			String publicTopic = "refreshIntGenieHomeId_" + homeId.toString();
			
			logger.info("getSwitchListByUserId method, JSON Message created");
			
			
			String str="wish/" + "switch_list_by_user/"+userDetails.getId().toString()+"/"+dateCurrent.getTime()+"/"+messageFrom;
			
			MqttMessage message = new MqttMessage((str.toString()).getBytes());
			message.setQos(Constants.QoS_EXACTLY_ONCE);
					
			
			/*
			// refreshBackMessage to mobile or to other thin end clients
			MqttMessage message = new MqttMessage(
					("wish|" + "switch_list_by_user|" + jMessage.toString()).getBytes());
			message.setQos(Constants.QoS_EXACTLY_ONCE);
			
			*/
	
			//MqttIntConnectionToHub.getObj().getMqttConnection().publish(publicTopic, message);
			
		}catch(Exception e){
			logger.error("Exception Message: Get Switch List By UserId  Method "+e.getMessage()+" Cause: "+e.getCause()+"\nStack Trace: "+e.getStackTrace());
			return null;
		}
		return null;
	}
	

	@Override
	public ChangeSwitchStatusResponseDTO changeStatus(ChangeSwitchStatusDTO changeSwitchStatusDTO) {
		try {

			UserDetails userDetails = userDetailsRepository.findOne(changeSwitchStatusDTO.getUserid());
			logger.info("change Switch Status method, User Details fetched");
			
			Long homeId = userDetails.getHome().getId();
			
			System.out.println("homeId = "+homeId);
			
			JSONObject jMessage = new JSONObject();
			jMessage.put("userid", userDetails.getId().toString());
			jMessage.put(Constants.SWITCH_ID, changeSwitchStatusDTO.getSwitchId().toString());
			jMessage.put(Constants.SWITCH_STATUS, changeSwitchStatusDTO.getSwitchStatus().toString());
			jMessage.put(Constants.DIMMER_VALUE, changeSwitchStatusDTO.getDimmerValue().toString());
			jMessage.put(Constants.MESSAGE_FROM, changeSwitchStatusDTO.getMessageFrom());
			
			String publicTopic = "refreshIntGenieHomeId_" + homeId.toString();
			//String publicTopic="refresh";				
			
			logger.info("change Switch Status method, JSON Message Created");
			/*
			MqttMessage message = new MqttMessage(
					("wish|" + "Internet_Switch_On_Off|" + jMessage.toString()).getBytes());
			message.setQos(Constants.QoS_EXACTLY_ONCE);
		*/
		/*	
			String str=userDetails.getId().toString()+"/"+ changeSwitchStatusDTO.getSwitchId().toString()+"/"+changeSwitchStatusDTO.getSwitchStatus().toString()
+"/"+changeSwitchStatusDTO.getDimmerValue().toString()+"/"+changeSwitchStatusDTO.getMessageFrom();
			*/

			String str="wish/" + "Internet_Switch_On_Off/"+userDetails.getId().toString()+"/"+ changeSwitchStatusDTO.getSwitchId().toString()+"/"+changeSwitchStatusDTO.getSwitchStatus().toString()
+"/"+changeSwitchStatusDTO.getDimmerValue().toString()+"/"+changeSwitchStatusDTO.getMessageFrom();
			
			System.out.println("str = "+str);
			MqttMessage message = new MqttMessage((str.toString()).getBytes());
			message.setQos(Constants.QoS_EXACTLY_ONCE);
					
	
			/*
			try {
			MqttIntConnectionToHub.getObj().getMqttConnection().publish(publicTopic, message);
			
			System.out.println("Iout  =");
			}catch (Exception e) {
				e.printStackTrace();
				// TODO: handle exception
			}
			*/
			

			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					try {
						System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> aws topic 2:\t"+publicTopic);
						MqttIntConnectionToHub.getObj().getMqttConnection().publish(publicTopic, message);
						
					} catch (MqttException e) {
						System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> problem:\t"+publicTopic);
						e.printStackTrace();
					}
				}
			});
			
			logger.debug("MQTT Message Internet and Hub Published Successfully. Topic: "+publicTopic);
			
			
			return null;

		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public ChangeLockStatusResponseDTO changeLockStatus(ChangeLockStatusDTO changeLockStatusDTO) {

		
		

		try {
			Panel panel = new Panel();
			StringBuffer switchOnOffCommand;
			Switch switchValidOrInvalid = switchRepository.findOne(changeLockStatusDTO.getSwitchId());
			
			logger.info("changeLockStatus(), Switch Details Fetched Switch Id: "+changeLockStatusDTO.getSwitchId());
			
			
			switchValidOrInvalid.setLockStatus(changeLockStatusDTO.getLockStatus());
			
			UserDetails userDetails = userDetailsRepository.findOne(changeLockStatusDTO.getUserId());
			logger.info("changeLockStatus(), User Details Fetched User Id: "+changeLockStatusDTO.getUserId());
			
			String lockStatus;
			if (changeLockStatusDTO.getLockStatus().equals("1")) {
				lockStatus = Constants.LOCK;
			} else {
				lockStatus = Constants.UNLOCK;
			}

			int Panelid = switchValidOrInvalid.getIotProduct().getIotProductNumber();
			panel = panelRepository.findBypanelIp(Panelid);
			logger.info("changeLockStatus(), Penal Details Fetched Panel Id: "+changeLockStatusDTO.getUserId());
			
			if (panel != null) {

					switchOnOffCommand = new StringBuffer("$[");
					switchOnOffCommand.append(switchValidOrInvalid.getSwitchNumber());

					if (switchValidOrInvalid.getSwitchNumber() < 5) {
						switchOnOffCommand.append("0");
						switchOnOffCommand.append(switchValidOrInvalid.getSwitchStatus());
						switchOnOffCommand.append(changeLockStatusDTO.getLockStatus());

					} else {
						if (switchValidOrInvalid.getDimmerStatus().toString().equals("1")) {

							int dimmerValue = Integer.parseInt(switchValidOrInvalid.getDimmerValue());
							if (dimmerValue < 10) {
								switchOnOffCommand.append("0");
								switchOnOffCommand.append(switchValidOrInvalid.getDimmerValue());

							} else {
								switchOnOffCommand.append(switchValidOrInvalid.getDimmerValue());
							}

						} else {
							switchOnOffCommand.append("00");
						}
						switchOnOffCommand.append(switchValidOrInvalid.getLockStatus());
					}
					switchOnOffCommand.append("]");
					Command commandSave = new Command(String.valueOf(switchOnOffCommand),
							dateFormat.format(new Date()).toString());
					
					saveSwitchCommand(commandSave, panel);
					
					switchRepository.save(switchValidOrInvalid);

					logger.info("changeLockStatus(), Switch Details Saved Successfully Switch Id:"+changeLockStatusDTO.getSwitchId());
					
					Activity activity = new Activity();
					if (userDetails.getImage() == null) {
						activity.setImage("");
					} else {
						activity.setImage(userDetails.getId()+ ".png");
						//activity.setImage(IMAGE_LOCATION + "/" + userDetails.getId() + ".png");

					}
					activity.setMessage(userDetails.getFirstName().toString() + " " + lockStatus + " "
							+ switchValidOrInvalid.getSwitchName());
					activity.setMessageType(Constants.SWITCH_LOCK_UNLOCK_MESSAGE_TYPE);
					activity.setRoomname(switchValidOrInvalid.getIotProduct().getRoom().getRoomName().toString());
					dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
					activity.setCreated_date(dateFormat.format(new Date()).toString());
					activity.setMessageFrom(changeLockStatusDTO.getMessageFrom().toString());
					activity.setHomeid(userDetails.getHome().getId());
					Activity addactivity = new Activity();
					addactivity = activityRepository.save(activity);
					logger.info("changeLockStatus(), Activity Details Saved Successfully"+changeLockStatusDTO.getUserId());
					

					return new ChangeLockStatusResponseDTO(
							switchValidOrInvalid.getIotProduct().getRoom().getHome().getHomeName(),
							switchValidOrInvalid.getIotProduct().getIotProductNumber().toString(),
							switchValidOrInvalid.getSwitchNumber().toString(), changeLockStatusDTO.getLockStatus());


			} else {
				
				logger.error("Exception in Change Lock Status method. Panel Not Found. Switch Number: "+changeLockStatusDTO.getSwitchId()
				+"User ID: "+changeLockStatusDTO.getUserId());

				logger.error("changeLockStatus(), Panel not found");
			}
		} catch (Exception e) {
			logger.error("Exception in Change Lock Status method. Switch Number: "+changeLockStatusDTO.getSwitchId()
			+"User ID: "+changeLockStatusDTO.getUserId());
			
			e.printStackTrace();
			
			return null;
		}
		
		return null;
		

	}

	@Override
	public ChangeHideStatusResponseDTO changeHidestatus(ChangeHideStatusDTO changeHideStatusDTO) {
		
		Activity addactivity = new Activity();
		Panel panel = new Panel();
		
		try {

			Switch switch1 = switchRepository.findOne(changeHideStatusDTO.getSwitchId());
			logger.info("changeHidestatus(), Switch Details Fetched Switch Id: "+changeHideStatusDTO.getSwitchId());
			
			UserDetails userDetails = userDetailsRepository.findOne(changeHideStatusDTO.getUserId());
			logger.info("changeHidestatus(), User Details Fetched Switch Id: "+changeHideStatusDTO.getSwitchId());
			
			String hideStatus;
			if (changeHideStatusDTO.getHideStatus().equals("1")) {
				hideStatus = Constants.HIDE;
			} else {
				hideStatus = Constants.UNHIDE;
			}

			if (switch1 == null)
				return null;
			
			int Panelid = switch1.getIotProduct().getIotProductNumber();
			panel = panelRepository.findBypanelId(Panelid);

			if (changeHideStatusDTO.getHideStatus() != null){
				switch1.setHideStatus(changeHideStatusDTO.getHideStatus());
				if(changeHideStatusDTO.getHideStatus().equals("1")){
					switch1.setSwitchStatus("0");
				}
			}

			StringBuffer switchOnOffCommand = new StringBuffer("$[");
			switchOnOffCommand.append(switch1.getSwitchNumber());

			// command formation
			// switch number dimmer value lock
			if (switch1.getSwitchNumber() < 5) {
				switchOnOffCommand.append("0");
				switchOnOffCommand.append(switch1.getSwitchStatus());
				switchOnOffCommand.append(switch1.getLockStatus());

			} else {
				if (switch1.getSwitchStatus().equals("0")) {

					switchOnOffCommand.append("00");
				} else {

					int dimmerValue = Integer.parseInt(switch1.getDimmerValue());
					if (dimmerValue < 10) {
						switchOnOffCommand.append("0");
						switchOnOffCommand.append(switch1.getDimmerValue());

					} else {
						switchOnOffCommand.append(switch1.getDimmerValue());
					}

				}
				switchOnOffCommand.append(switch1.getLockStatus());
			}

			switchOnOffCommand.append("]");

			switchRepository.save(switch1);
			logger.info("Switch Details Saved. Switch ID: "+switch1.getId());
			
			Activity activity = new Activity();
			if (userDetails.getImage() == null) {
				activity.setImage("");
			} else {
				activity.setImage(userDetails.getId()+ ".png");
				//activity.setImage(IMAGE_LOCATION + "/" + userDetails.getId() + ".png");
			}
			
			activity.setMessage(
					userDetails.getFirstName().toString() + " " + hideStatus + " " + switch1.getSwitchName());
			activity.setMessageType("Switch_Hide_Unhide");
			activity.setRoomname(switch1.getIotProduct().getRoom().getRoomName().toString());
			dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
			activity.setCreated_date(dateFormat.format(new Date()).toString());
			activity.setMessageFrom(changeHideStatusDTO.getMessageFrom().toString());

			activity.setHomeid(userDetails.getHome().getId());
			addactivity = activityRepository.save(activity);

			
			return new ChangeHideStatusResponseDTO(switch1.getIotProduct().getRoom().getHome().getHomeName(),
					switch1.getIotProduct().getIotProductNumber().toString(), switch1.getSwitchNumber().toString(),
					changeHideStatusDTO.getHideStatus());
		} catch (Exception e) {
			logger.error("Exception in Change Hide Status method. Switch Number: "+changeHideStatusDTO.getSwitchId()
			+"User ID: "+changeHideStatusDTO.getUserId());
			e.printStackTrace();
			logger.error("Change Hide Status, Error Message: "+e.getStackTrace());
			return null;
		}
		

	}

	@Override
	public List<ChangeStatusByRoomResponseDTO> changeStatusByRoom(ChangeStatusByRoomDTO changeStatusByRoomDTO) {
		try {

			try {
				UserDetails userDetails = userDetailsRepository.findOne(changeStatusByRoomDTO.getUserId());

				logger.info("changeStatusByRoom method, User Details Fetched");
				
				Long homeId = userDetails.getHome().getId();
				
				JSONObject jMessage = new JSONObject();

				jMessage.put(Constants.USER_ID, userDetails.getId().toString());
				jMessage.put(Constants.ROOM_ID, changeStatusByRoomDTO.getRoomId().toString());
				jMessage.put(Constants.ON_OFF_STATUS, changeStatusByRoomDTO.getOnoffstatus());
				jMessage.put(Constants.MESSAGE_FROM, changeStatusByRoomDTO.getMessageFrom());
				
				logger.info("changeStatusByRoom method, JSON Message Created");
				
				String publicTopic = "refreshIntGenieHomeId_" + homeId.toString();
				/*
				// refreshBackMessage to mobile or to other thin end clients
				MqttMessage message = new MqttMessage(
						("wish|" + "changeStatusByRoom|" + jMessage.toString()).getBytes());
				message.setQos(Constants.QoS_EXACTLY_ONCE);
				*/

				String str="wish/" + "changeStatusByRoom/" + userDetails.getId().toString()+"/"+ changeStatusByRoomDTO.getRoomId().toString()+"/"+changeStatusByRoomDTO.getOnoffstatus().toString()
	+"/"+changeStatusByRoomDTO.getMessageFrom().toString();
				
				MqttMessage message = new MqttMessage((str.toString()).getBytes());
				message.setQos(Constants.QoS_EXACTLY_ONCE);
						
		
				//MqttIntConnectionToHub.getObj().getMqttConnection().publish(publicTopic, message);
				
				return null;

			} catch (Exception e) {
				logger.error(e);
				return null;
			}

		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}
	
	public boolean saveSwitchCommand(Command command, Panel panel) {
		commandRepository.save(command);
		return false;
	}

	@Override
	public ChangeSwitchStatusResponseDTO changeCurtainStatus(ChangeCurtainStatusDTO changeCurtainStatusDTO) {

		JSONObject jMessage = new JSONObject();
		UserDetails userDetails = userDetailsRepository.findOne(changeCurtainStatusDTO.getUserId());
		logger.info("changeCurtainStatus method, User Details Fetched");
		
		
		if(userDetails != null){
		
			Long homeId = userDetails.getHome().getId();
			
			try {
				jMessage.put(Constants.SWITCH_ID, changeCurtainStatusDTO.getSwitchId().toString());
				jMessage.put(Constants.USER_ID, userDetails.getId().toString());
				jMessage.put(Constants.SWITCH_STATUS, changeCurtainStatusDTO.getSwitchStatus());
				jMessage.put(Constants.CURTAIN_STATUS, changeCurtainStatusDTO.getCurtainStatus());
				jMessage.put(Constants.MESSAGE_FROM, changeCurtainStatusDTO.getMessageFrom());
				
				logger.info("changeCurtainStatus method, JSON Message Created");
				
				String publicTopic = "refreshIntGenieHomeId_" + homeId.toString();
				// refreshBackMessage to mobile or to other thin end clients
				/*
				MqttMessage message = new MqttMessage(
						("wish|" + "change_curtain_status|" + jMessage.toString()).getBytes());
				message.setQos(Constants.QoS_EXACTLY_ONCE);
*/

				String str="wish/" + "change_curtain_status/" + userDetails.getId().toString()+"/"+ userDetails.getId().toString()+"/"+changeCurtainStatusDTO.getSwitchStatus()
	+"/"+ changeCurtainStatusDTO.getCurtainStatus()+"/"+changeCurtainStatusDTO.getMessageFrom();
				
				MqttMessage message = new MqttMessage((str.toString()).getBytes());
				message.setQos(Constants.QoS_EXACTLY_ONCE);
						
		/*
				try {
					MqttIntConnectionToHub.getObj().getMqttConnection().publish(publicTopic, message);
				} catch (MqttPersistenceException e) {
					logger.error("changeCurtainStatus method, MQTT Persistent Exception");
					e.printStackTrace();
				} catch (MqttException e) {
					logger.error("changeCurtainStatus method, MQTT Exception");
					e.printStackTrace();
				}*/
				
				
			} catch (JSONException e) {
				logger.info("Curtain Status Chnage JSON Exception");
				e.printStackTrace();
			}
		
		}
		
		return null;
	}

	@Override
	public Boolean changeGenieColor(ChangeSwitchStatusDTO changeSwitchStatusDTO) {
		JSONObject jMessage = new JSONObject();
		
		UserDetails userDetails = userDetailsRepository.findOne(changeSwitchStatusDTO.getUserid());
		
		logger.info("Change Genie Color method, User Details Fetched");
		
		if(userDetails != null){
		
			Long homeId = userDetails.getHome().getId();
			try {
				jMessage.put(Constants.SWITCH_ID, changeSwitchStatusDTO.getSwitchId().toString());
				jMessage.put(Constants.USER_iD, userDetails.getId().toString());
				jMessage.put(Constants.SWITCH_STATUS, changeSwitchStatusDTO.getSwitchStatus());
				jMessage.put(Constants.DIMMER_VALUE, changeSwitchStatusDTO.getDimmerValue());
				jMessage.put(Constants.MESSAGE_FROM, changeSwitchStatusDTO.getMessageFrom());
				jMessage.put(Constants.GENIE_COLOR, changeSwitchStatusDTO.getGenieColor());
				
				logger.info("Change Genie Color method, JSON Message Created");
				
				String publicTopic = "refreshIntGenieHomeId_" + homeId.toString();
				// refreshBackMessage to mobile or to other thin end clients
				/*
				MqttMessage message = new MqttMessage(
						("wish|" + "change_genie_color|" + jMessage.toString()).getBytes());
				message.setQos(Constants.QoS_EXACTLY_ONCE);
*/

				String str="wish/" + "change_genie_color/" + changeSwitchStatusDTO.getSwitchId().toString()+"/"+ userDetails.getId().toString()+"/"+ changeSwitchStatusDTO.getSwitchStatus()
	+"/"+changeSwitchStatusDTO.getDimmerValue()+"/"+ changeSwitchStatusDTO.getMessageFrom()+"/"+  changeSwitchStatusDTO.getGenieColor();
				
				MqttMessage message = new MqttMessage((str.toString()).getBytes());
				message.setQos(Constants.QoS_EXACTLY_ONCE);
						
		
				/*try {
					MqttIntConnectionToHub.getObj().getMqttConnection().publish(publicTopic, message);
				} catch (MqttPersistenceException e) {
					logger.error("Change Genie Color method, MQTT Persistent Exception");
					e.printStackTrace();
				} catch (MqttException e) {
					logger.error("Change Genie Color method, MQTT Exception");
					e.printStackTrace();
				}
				*/
				
			} catch (JSONException e) {
				logger.info("Change Genie Color method, JSON Exception");
				e.printStackTrace();
			}
		
		}
		
		return null;
	}



}
