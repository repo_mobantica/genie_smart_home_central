package com.gsmarthome.serviceImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;
import com.gsmarthome.constants.Constants;
import com.gsmarthome.dto.ActivityDTO;
import com.gsmarthome.dto.NotificationHistoryDTO;
import com.gsmarthome.dto.NotificationHistoryResponseDTO;
import com.gsmarthome.entity.Notification;
import com.gsmarthome.repository.NotificationRepository;
import com.gsmarthome.service.NotificationService;
import com.gsmarthome.util.Utility;

import MqttReadDataPaho.MqttIntConnectionToHub;


@Component
@Service
@Qualifier("Ac2dm")
public class NotificationServiceImpl implements NotificationService {

	@Autowired
	NotificationRepository notificationRepository;
	
	static Logger logger = Logger.getLogger(NotificationServiceImpl.class.getName());

	@Override
	public List<NotificationHistoryResponseDTO> getNotificationHistory(NotificationHistoryDTO notificationHistoryDTO)
	{	
		List<NotificationHistoryResponseDTO> list = new ArrayList<NotificationHistoryResponseDTO>();
		Notification notification1[] = notificationRepository.findfityUnreadNotification(notificationHistoryDTO.getUserId());
	
		for(int i = 0;i < notification1.length; i++)
		{			
			if(i==50)
				break;
			list.add(new NotificationHistoryResponseDTO(notification1[i].getMessage()));	
			notification1[i].setIsRead(1);			
			notificationRepository.save(notification1[i]);
		}
		
		return list;
	}

	@Override
	public void getActivityList(ActivityDTO activityDTO) {
		
		Date date = new Date();
		JSONObject jMessage = new JSONObject();

		if(activityDTO != null){
			try {
				jMessage.put(Constants.ACTIVITY_HOME_ID, activityDTO.getHomeId());
				
				jMessage.put(Constants.TIME, date.getDate() + " " + Utility.theMonth(date.getMonth()) + " "
						+ Utility.getTime(date.getHours() + ":" + date.getMinutes()).toString());
				logger.info("Get activity list, JSON Message Created");
				
			} catch (JSONException e) {
				logger.info("Get activity list, MQTT Exception, Message: "+e.getMessage());
				e.printStackTrace();
			}
	
			String publicTopic = "refreshIntGenieHomeId_" + activityDTO.getHomeId().toString();
			// refreshBackMessage to mobile or to other thin end clients
			MqttMessage message = new MqttMessage(("wish|" + "get_activity|" + jMessage.toString()).getBytes());
			message.setQos(Constants.QoS_EXACTLY_ONCE);
			/*
			try {
				MqttIntConnectionToHub.getObj().getMqttConnection().publish(publicTopic, message);
				logger.info("Get activity list, MQTT Message Published");
			} catch (MqttException e) {
				logger.error("Get activity list, MQTT Exception, Message: "+e.getMessage());
				e.printStackTrace();
			}*/
		}
	}
	
	/*	public void pushNotification(Notification notification) {
		  List<Notification> notList = new ArrayList<Notification>();
		  notList.add(notification);
		  pushNotifications(notList);
	}
	
	 public void pushNotifications(List<Notification> notifications) {
		  try {
		   HttpClient client = new HttpClient();
		   PostMethod method = new PostMethod("https://www.google.com/accounts/ClientLogin");
		   method.addParameter("Email", sendingRoleAccount);
		   method.addParameter("Passwd", sendingRolePassword);
		   method.addParameter("accountType", "HOSTED_OR_GOOGLE");
		   method.addParameter("source", "unit-test");
		   method.addParameter("service", "ac2dm");
		   client.executeMethod(method);
		   byte[] responseBody = method.getResponseBody();
		   String response = new String(responseBody);
		   String auth = response.split("\n")[2];
		   String token = auth.split("=")[1];
		   for (Notification notification : notifications) {
		    doSendNotification(notification, token, client);
		   }
		  } catch (Throwable t) {
		   throw new RuntimeException(t);
		  }
		 }
	 private void doSendNotification(Notification notification, String token, HttpClient client) throws HttpException, IOException 
	 {	 
		  PostMethod method = new PostMethod("https://android.apis.google.com/c2dm/send");
		  method.addParameter("registration_id", notification.getDeviceToken());
		  method.addParameter("collapse_key", "collapse");
		  method.addParameter("data.payload", notification.getBadge().toString());  
		  Header header = new Header("Authorization", "GoogleLogin auth="+token);
		  method.addRequestHeader(header);
		  client.executeMethod(method);  
		  byte[] responseBody = method.getResponseBody();  
		  String response = new String(responseBody);
		 }
	 
	public void setSendingRoleAccount(String sendingRoleAccount) 
	{
		  this.sendingRoleAccount = sendingRoleAccount;
	}
		 
	public void setSendingRolePassword(String sendingRolePassword) 
	{
		  this.sendingRolePassword = sendingRolePassword;
	}*/

}
