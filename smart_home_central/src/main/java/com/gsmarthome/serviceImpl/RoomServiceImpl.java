package com.gsmarthome.serviceImpl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;
import com.gsmarthome.constants.Constants;
import com.gsmarthome.controller.RoomController;
import com.gsmarthome.dto.AddUpdateRoomDTO;
import com.gsmarthome.dto.RoomListByUserDTO;
import com.gsmarthome.dto.UpdateHideRoomDTO;
import com.gsmarthome.dto.getReponseDTO;
import com.gsmarthome.entity.Activity;
import com.gsmarthome.entity.Home;
import com.gsmarthome.entity.Room;
import com.gsmarthome.entity.RoomType;
import com.gsmarthome.entity.UserDetails;
import com.gsmarthome.repository.ActivityRepository;
import com.gsmarthome.repository.HomeRepository;
import com.gsmarthome.repository.RoomRepository;
import com.gsmarthome.repository.RoomTypeRepository;
import com.gsmarthome.repository.SwitchRepository;
import com.gsmarthome.repository.UserDetailsRepository;
import com.gsmarthome.repository.ActivityRepository;
import com.gsmarthome.service.RoomService;
import com.gsmarthome.util.Utility;

import MqttReadDataPaho.MqttIntConnectionToHub;

@Component
public class RoomServiceImpl implements RoomService {

	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:00");

	static Logger logger = Logger.getLogger(RoomServiceImpl.class.getName());

	@Autowired
	HomeRepository homeRepository;

	@Autowired
	ActivityRepository  activityRepository;

	@Autowired
	UserDetailsRepository userDetailsRepository;

	@Autowired
	SwitchRepository switchRepository;

	@Autowired
	RoomRepository roomRepository;

	@Autowired
	RoomTypeRepository roomTypeRepository;

	@Value("${ROOM_NAME_ALREADY_PRESENT}")
	private String ROOM_NAME_ALREADY_PRESENT;


	@Value("${API_STATUS_SUCCESS}")
	private String API_STATUS_SUCCESS;

	@Value("${API_STATUS_FAILURE}")
	private String API_STATUS_FAILURE;

	@Override
	public getReponseDTO addUpdateRoom(AddUpdateRoomDTO addUpdateRoomDTO) {
		getReponseDTO objgetReponseDTO=new getReponseDTO();
		JSONObject jMessage = new JSONObject();

		Activity activity= new Activity();
		Activity addactivity= new Activity();

		try {

			if(addUpdateRoomDTO.getRoomId() != null)
			{

				UserDetails userDetails = userDetailsRepository.findOne(addUpdateRoomDTO.getUserId());
				Room room = roomRepository.findOne(addUpdateRoomDTO.getRoomId());			
				String oldRoomName = room.getRoomName();
				if(addUpdateRoomDTO.getRoomType()!=null)
					room.setRoomType(addUpdateRoomDTO.getRoomType());
				if(addUpdateRoomDTO.getRoomName()!=null)
					room.setRoomName(addUpdateRoomDTO.getRoomName());
				if(addUpdateRoomDTO.getRoomImageId() != null)
					room.setRoomImageId(addUpdateRoomDTO.getRoomImageId());
				roomRepository.save(room);		

				if(userDetails.getImage() == null)
				{
					activity.setImage("");
				}
				else
				{
					activity.setImage(userDetails.getId()+ ".png");
				}
				activity.setMessage(userDetails.getFirstName().toString()+" update room name from " +oldRoomName+ " to " + room.getRoomName());
				activity.setMessageType("Room_Name_Update");
				activity.setRoomname(room.getRoomName().toString());
				dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
				activity.setCreated_date(dateFormat.format(new Date()).toString());
				activity.setMessageFrom(addUpdateRoomDTO.getMessageFrom());
				activity.setHomeid(userDetails.getHome().getId());
				addactivity = activityRepository.save(activity);


			}

			objgetReponseDTO.setResponse("Room change request from internet");		
			objgetReponseDTO.setStatus(API_STATUS_SUCCESS);
		}catch (Exception e) {

			objgetReponseDTO.setResponse("Room change request FAILURE");		
			objgetReponseDTO.setStatus(API_STATUS_FAILURE);
			// TODO: handle exception
		}
		return objgetReponseDTO;

	}

	@Override
	public Boolean deleteRoom(Long roomId) {

		Room room = roomRepository.findOne(roomId);

		Home home = room.getHome();

		home.getRoomList().remove(room);

		Home savedHome = homeRepository.save(home);

		return true;

	}

	@Override
	public List<RoomListByUserDTO> getRoomListByVendorId(Long userId , String messageFrom) {


		UserDetails userDetails = userDetailsRepository.findOne(userId);
		logger.info("getRoomListByVendorId method, User details Fetched");

		List<RoomListByUserDTO> list = new ArrayList<RoomListByUserDTO>();

		for (Room room : userDetails.getHome().getRoomList()) {

			RoomType roomType = roomTypeRepository.findOne(Long.parseLong(room.getRoomType()));

			list.add(new RoomListByUserDTO(room.getId(), room.getRoomName(), room.getRoomType(),
					roomType.getRoomImage()));
		}	


		try {		
			//UserDetails userDetails = userDetailsRepository.findOne(vendorId);
			Long homeId = userDetails.getHome().getId();

			//Long iOTProductNo = switchRepository.getSwichbyRoomId(ROOMID, onoffStatus)

			JSONObject jMessage = new JSONObject();

			jMessage.put(Constants.USER_ID, userId);		
			jMessage.put(Constants.MESSAGE_FROM, "Internet");
			logger.info("getRoomListByVendorId method, JSON Message Created");


			String publicTopic = "refreshIntGenieHomeId_" + homeId.toString();

			// refreshBackMessage to mobile or to other thin end clients
			/*
		MqttMessage message = new MqttMessage(
				("wish|" + "Internet_getRoomListByVendorId|" + jMessage.toString()).getBytes());
		message.setQos(Constants.QoS_EXACTLY_ONCE);
			 */
			String str="";

			MqttMessage message = new MqttMessage(
					(str.toString()).getBytes());
			message.setQos(Constants.QoS_EXACTLY_ONCE);

			//MqttIntConnectionToHub.getObj().getMqttConnection().publish(publicTopic, message);

			return list;

		} catch (Exception e) {		
			return null;
		}


	}

	@Override
	public Boolean changeRoomHideStatus(UpdateHideRoomDTO updateHideRoomDTO) throws Exception {

		JSONObject jMessage = new JSONObject();

		try {

			UserDetails userDetails = userDetailsRepository.findOne(updateHideRoomDTO.getUserId());
			logger.info("changeRoomHideStatus method, user Details Fetched");

			Long homeId = userDetails.getHome().getId();

			jMessage.put(Constants.USER_ID, updateHideRoomDTO.getUserId().toString());
			jMessage.put(Constants.ROOM_ID, updateHideRoomDTO.getRoomId().toString());
			jMessage.put(Constants.HIDE_STATUS, updateHideRoomDTO.getHideStatus().toString());
			jMessage.put(Constants.MESSAGE_FROM, updateHideRoomDTO.getMessageFrom());

			logger.info("changeRoomHideStatus method, JSON Message Created");

			//MQTT Call
			String publicTopic = "refreshIntGenieHomeId_" + homeId.toString();
			MqttMessage message = new MqttMessage(
					("wish|" + "hide_room_status|" + jMessage.toString()).getBytes());
			message.setQos(Constants.QoS_EXACTLY_ONCE);
			//MqttIntConnectionToHub.getObj().getMqttConnection().publish(publicTopic, message);

			return true;

		} catch (Exception  e) {
			logger.error("Exception Message: "+e.getMessage()+"Exception Cause: "+e.getCause());
			e.printStackTrace();
			throw new JSONException(e);
		}

	}

}
