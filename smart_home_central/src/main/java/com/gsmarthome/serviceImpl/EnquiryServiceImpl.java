package com.gsmarthome.serviceImpl;

import java.util.Date;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.amazonaws.util.json.JSONObject;
import com.gsmarthome.controller.CommonController;
import com.gsmarthome.dto.AddUpdateRoomDTO;
import com.gsmarthome.dto.EnquiryDTO;
import com.gsmarthome.dto.getReponseDTO;
import com.gsmarthome.entity.Activity;
import com.gsmarthome.entity.Enquiry;
import com.gsmarthome.entity.Room;
import com.gsmarthome.entity.UserDetails;
import com.gsmarthome.repository.EnquiryRepository;
import com.gsmarthome.repository.UserDetailsRepository;
import com.gsmarthome.service.EnquiryService;

@Component
public class EnquiryServiceImpl  implements EnquiryService{

	@Autowired
	EnquiryRepository enquiryRepository;
	@Autowired
	UserDetailsRepository userDetailsRepository;



	@Value("${API_STATUS_SUCCESS}")
	private String API_STATUS_SUCCESS;

	@Value("${API_STATUS_FAILURE}")
	private String API_STATUS_FAILURE;

	@Override
	public getReponseDTO addEnquiry(EnquiryDTO addEnquiry) {
		getReponseDTO objgetReponseDTO=new getReponseDTO();
		JSONObject jMessage = new JSONObject();

		Activity activity= new Activity();
		Activity addactivity= new Activity();

		Enquiry enquiry=new Enquiry();
		try {
			UserDetails userDetails=new UserDetails();
			userDetails.setId(Long.parseLong(addEnquiry.getUserId()));
			enquiry.setUserDetails(userDetails);
			//enquiry.setUserId(addEnquiry.getUserId());
			enquiry.setMessageType(addEnquiry.getMessageType());
			enquiry.setMessage(addEnquiry.getMessage());
			enquiry.setDeviceId(addEnquiry.getDeviceId());
			enquiry.setCreated_date(addEnquiry.getCreated_date());
			enquiry.setStatus(1);
			enquiryRepository.save(enquiry);

			UserDetails userDetailsById = userDetailsRepository.findOne(Long.parseLong(addEnquiry.getUserId()));
			String message = "Dear customer, ";
			//1 -feedback
			if(addEnquiry.getMessageType()==1)
			{
				message="Thanks for your feedback.";
			}
			else if(addEnquiry.getMessageType()==2)//2-compl
			{
				message="We will soon consider your compaint & try to resolve it. Thank you.";	
			}
			else //3.enquiry
			{
				message="Thanks for your enquiry, we will surely update you soon.";
			}
			try {
				boolean sms = CommonController.sendSMS(userDetailsById.getPhoneNumber(), message);
			}catch (Exception e) {
				// TODO: handle exception
			}
			objgetReponseDTO.setResponse("Enquiry Added ");		
			objgetReponseDTO.setStatus(API_STATUS_SUCCESS);
		}catch (Exception e) {
			e.printStackTrace();
			objgetReponseDTO.setResponse("Enquiry FAILURE");		
			objgetReponseDTO.setStatus(API_STATUS_FAILURE);
			// TODO: handle exception
		}
		return objgetReponseDTO;

	}

}
