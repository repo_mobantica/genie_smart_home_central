package com.gsmarthome.serviceImpl;

import java.util.Date;

import org.apache.log4j.Logger;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.amazonaws.util.json.JSONObject;
import com.gsmarthome.constants.Constants;
import com.gsmarthome.dto.ScheduleProfileDTO;
import com.gsmarthome.dto.ScheduleProfileResponseDTO;
import com.gsmarthome.dto.ScheduleSwitchDTO;
import com.gsmarthome.dto.ScheduleSwitchResponseDTO;
import com.gsmarthome.entity.UserDetails;
import com.gsmarthome.repository.UserDetailsRepository;
import com.gsmarthome.service.ScheduleService;

import MqttReadDataPaho.MqttIntConnectionToHub;

@Component
public class ScheduleServiceImpl implements ScheduleService {
	
	final static Logger logger = Logger.getLogger(ScheduleServiceImpl.class.getName());
	
	@Autowired
	UserDetailsRepository userDetailsRepository;
	
	@Override
	public ScheduleSwitchResponseDTO scheduleSwitch(ScheduleSwitchDTO scheduleSwitchDTO) {

		JSONObject jMessage = new JSONObject();
		
		try {
			UserDetails deviceDetails = userDetailsRepository.findOne(scheduleSwitchDTO.getUserId());
			Long homeId = deviceDetails.getHome().getId();

			jMessage.put(Constants.USER_ID, deviceDetails.getId().toString());
			jMessage.put(Constants.SWITCH_ID, scheduleSwitchDTO.getSwitchId().toString());
			jMessage.put(Constants.SWITCH_STATUS, scheduleSwitchDTO.getSwitchStatus().toString());
			jMessage.put(Constants.DIMMER_VALUE, scheduleSwitchDTO.getDimmerValue().toString());
			jMessage.put(Constants.DIMMER_STATUS, scheduleSwitchDTO.getDimmerStatus().toString());
			jMessage.put(Constants.REPEAT_STATUS, scheduleSwitchDTO.getRepeatStatus().toString());
			jMessage.put(Constants.REPEAT_WEEK, scheduleSwitchDTO.getRepeatWeek().toString());
			jMessage.put(Constants.LOCK_STATUS, scheduleSwitchDTO.getLockStatus().toString());
			jMessage.put(Constants.SCHEDULAR_DATE_TIME, scheduleSwitchDTO.getScheduleDateTime().toString());
			jMessage.put(Constants.MESSAGE_FROM, scheduleSwitchDTO.getMessageFrom());
			String publicTopic = "refreshIntGenieHomeId_" + homeId.toString();
			// refreshBackMessage to mobile or to other thin end clients
			MqttMessage message = new MqttMessage(("wish|" + "scheduleSwitch|" + jMessage.toString()).getBytes());
			message.setQos(Constants.QoS_EXACTLY_ONCE);
			//MqttIntConnectionToHub.getObj().getMqttConnection().publish(publicTopic, message);
			return null;

		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			return null;
		}

	}
	
	@Override
	public ScheduleSwitchResponseDTO editScheduleSwitch(ScheduleSwitchDTO scheduleSwitchDTO) {
		try {
			UserDetails deviceDetails = userDetailsRepository.findOne(scheduleSwitchDTO.getUserId());
			Long homeId = deviceDetails.getHome().getId();
			JSONObject jMessage = new JSONObject();
			jMessage.put(Constants.SCHEDULE_SWITCH_ID, scheduleSwitchDTO.getScheduleSwitchId().toString());
			jMessage.put(Constants.SWITCH_ID, scheduleSwitchDTO.getSwitchId().toString());
			jMessage.put(Constants.USER_ID, deviceDetails.getId().toString());
			jMessage.put(Constants.SWITCH_STATUS, scheduleSwitchDTO.getSwitchStatus().toString());
			jMessage.put(Constants.DIMMER_VALUE, scheduleSwitchDTO.getDimmerStatus().toString());
			jMessage.put(Constants.DIMMER_STATUS, scheduleSwitchDTO.getDimmerStatus().toString());
			jMessage.put(Constants.REPEAT_STATUS, scheduleSwitchDTO.getRepeatStatus().toString());
			jMessage.put(Constants.REPEAT_WEEK, scheduleSwitchDTO.getRepeatWeek().toString());
			jMessage.put(Constants.LOCK_STATUS, scheduleSwitchDTO.getLockStatus().toString());
			jMessage.put(Constants.SCHEDULAR_DATE_TIME, scheduleSwitchDTO.getScheduleDateTime().toString());
			jMessage.put(Constants.MESSAGE_FROM, scheduleSwitchDTO.getMessageFrom());
			String publicTopic = "refreshIntGenieHomeId_" + homeId.toString();
			// refreshBackMessage to mobile or to other thin end clients
			MqttMessage message = new MqttMessage(("wish|" + "editScheduler|" + jMessage.toString()).getBytes());
			message.setQos(Constants.QoS_EXACTLY_ONCE);
			//MqttIntConnectionToHub.getObj().getMqttConnection().publish(publicTopic, message);
			return null;

		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}
	
	@Override
	public Boolean deleteScheduleSwitch(ScheduleSwitchDTO scheduleSwitchDTO) {
		try {
			UserDetails userDetails = userDetailsRepository.findOne(scheduleSwitchDTO.getUserId());
			Long homeId = userDetails.getHome().getId();
			JSONObject jMessage = new JSONObject();

			jMessage.put(Constants.SCHEDULE_SWITCH_ID, scheduleSwitchDTO.getScheduleSwitchId().toString());
			jMessage.put(Constants.SWITCH_ID, scheduleSwitchDTO.getSwitchId().toString());
			jMessage.put(Constants.USER_ID, userDetails.getId().toString());
			jMessage.put(Constants.SWITCH_STATUS, scheduleSwitchDTO.getSwitchStatus().toString());
			jMessage.put(Constants.LOCK_STATUS, scheduleSwitchDTO.getLockStatus().toString());
			jMessage.put(Constants.SCHEDULAR_DATE_TIME, scheduleSwitchDTO.getScheduleDateTime().toString());
			jMessage.put(Constants.MESSAGE_FROM, scheduleSwitchDTO.getMessageFrom());
			String publicTopic = "refreshIntGenieHomeId_" + homeId.toString();
			// refreshBackMessage to mobile or to other thin end clients
			MqttMessage message = new MqttMessage(("wish|" + "deleteScheduler|" + jMessage.toString()).getBytes());
			message.setQos(Constants.QoS_EXACTLY_ONCE);
			//MqttIntConnectionToHub.getObj().getMqttConnection().publish(publicTopic, message);
			return true;

		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public Boolean getScheduleSwitchList(ScheduleSwitchDTO scheduleSwitchDTO) throws Exception{

		JSONObject jMessage = new JSONObject();
		Date dateCurrent = new Date();
		
		try{
			UserDetails userDetails = userDetailsRepository.findOne(scheduleSwitchDTO.getUserId());
			
			if(userDetails != null){
				
				logger.info("getScheduleSwitchList() User Details found");
				
				jMessage.put(Constants.USER_ID, userDetails.getId().toString());
				jMessage.put(Constants.TIME, ""+dateCurrent.getSeconds());
				jMessage.put(Constants.MESSAGE_FROM, scheduleSwitchDTO.getMessageFrom());
				
				String publicTopic = "refreshIntGenieHomeId_" + userDetails.getHome().getId().toString();
				
				MqttMessage message = new MqttMessage(("wish|" + "get_scheduler_list|" + jMessage.toString()).getBytes());
				message.setQos(Constants.QoS_EXACTLY_ONCE);
				//MqttIntConnectionToHub.getObj().getMqttConnection().publish(publicTopic, message);
				
				logger.info("getScheduleSwitchList() MQTT Message Sent Successfully");
				
				return true;
			}else{
				logger.error("Exception in getScheduleSwitchList() User Details not found for User ID: "+scheduleSwitchDTO.getUserId());
				return false;
			}
		}catch (Exception e) {
			logger.error("Exception in getScheduleSwitchList()"+e.getMessage());
			e.printStackTrace();
			throw new Exception(e);
		}
	}

	@Override
	public ScheduleProfileResponseDTO scheduleProfile(ScheduleProfileDTO scheduleProfileDTO) throws Exception {
		JSONObject jMessage = new JSONObject();
		
		try{
			UserDetails userDetails = userDetailsRepository.findOne(scheduleProfileDTO.getUserId());
		
			if(userDetails != null){
				logger.info("scheduleProfile(), User Details found");
				jMessage.put(Constants.PROFILE_ID, scheduleProfileDTO.getProfileId().toString());
				jMessage.put(Constants.REPEAT_STATUS, scheduleProfileDTO.getRepeatStatus());
				jMessage.put(Constants.REPEAT_WEEK, scheduleProfileDTO.getRepeatWeek());
				jMessage.put(Constants.USER_ID, userDetails.getId().toString());
				jMessage.put(Constants.SCHEDULAR_DATE_TIME, scheduleProfileDTO.getScheduleDateTime().toString());
				jMessage.put(Constants.MESSAGE_FROM, scheduleProfileDTO.getMessageFrom());
				
				String publicTopic = "refreshIntGenieHomeId_" + userDetails.getHome().getId().toString();
	
				MqttMessage message = new MqttMessage(("wish|" + "schedule_profile|" + jMessage.toString()).getBytes());
				message.setQos(Constants.QoS_EXACTLY_ONCE);
				//MqttIntConnectionToHub.getObj().getMqttConnection().publish(publicTopic, message);
				
				logger.info("scheduleProfile() MQTT Message Sent Successfully");
				
			}
		}catch(Exception e){
			logger.error("scheduleProfile() Exception Message: "+e.getMessage()+"Cause: "+e.getCause());
			e.printStackTrace();
			throw new Exception(e);
		}
		
		return null;
	}

	@Override
	public ScheduleProfileResponseDTO deleteScheduleProfile(ScheduleProfileDTO scheduleProfileDTO) throws Exception {
		JSONObject jMessage = new JSONObject();
		
		try{
			UserDetails userDetails = userDetailsRepository.findOne(scheduleProfileDTO.getUserId());
		
			if(userDetails != null){
				logger.info("deleteScheduleProfile() User Details found");
				
				jMessage.put(Constants.SCHEDULE_ID, scheduleProfileDTO.getScheduleId().toString());
				jMessage.put(Constants.PROFILE_ID, scheduleProfileDTO.getProfileId().toString());
				jMessage.put(Constants.REPEAT_STATUS, scheduleProfileDTO.getRepeatStatus());
				jMessage.put(Constants.REPEAT_WEEK, scheduleProfileDTO.getRepeatWeek());
				jMessage.put(Constants.USER_ID, userDetails.getId().toString());
				jMessage.put(Constants.SCHEDULAR_DATE_TIME, scheduleProfileDTO.getScheduleDateTime().toString());
				jMessage.put(Constants.MESSAGE_FROM, scheduleProfileDTO.getMessageFrom());
				
				String publicTopic = "refreshIntGenieHomeId_" + userDetails.getHome().getId().toString();
	
				MqttMessage message = new MqttMessage(("wish|" + "delete_schedule_profile|" + jMessage.toString()).getBytes());
				message.setQos(Constants.QoS_EXACTLY_ONCE);
				//MqttIntConnectionToHub.getObj().getMqttConnection().publish(publicTopic, message);
				
				logger.info("deleteScheduleProfile() MQTT Message Sent Successfully");
				
			}
		}catch(Exception e){
			logger.error("deleteScheduleProfile() Exception Message: "+e.getMessage()+"Cause: "+e.getCause());
			e.printStackTrace();
			throw new Exception(e);
		}
		
		return null;
	}

	@Override
	public ScheduleProfileResponseDTO editScheduleProfile(ScheduleProfileDTO scheduleProfileDTO) throws Exception {
		
		JSONObject jMessage = new JSONObject();
		
		try{
			UserDetails userDetails = userDetailsRepository.findOne(scheduleProfileDTO.getUserId());
		
			if(userDetails != null){
				logger.info("editScheduleProfile() User Details found");
				
				jMessage.put(Constants.SCHEDULE_ID, scheduleProfileDTO.getScheduleId().toString());
				jMessage.put(Constants.PROFILE_ID, scheduleProfileDTO.getProfileId().toString());
				jMessage.put(Constants.REPEAT_STATUS, scheduleProfileDTO.getRepeatStatus());
				jMessage.put(Constants.REPEAT_WEEK, scheduleProfileDTO.getRepeatWeek());
				jMessage.put(Constants.USER_ID, userDetails.getId().toString());
				jMessage.put(Constants.SCHEDULAR_DATE_TIME, scheduleProfileDTO.getScheduleDateTime().toString());
				jMessage.put(Constants.MESSAGE_FROM, scheduleProfileDTO.getMessageFrom());
				
				String publicTopic = "refreshIntGenieHomeId_" + userDetails.getHome().getId().toString();
	
				MqttMessage message = new MqttMessage(("wish|" + "edit_schedule_profile|" + jMessage.toString()).getBytes());
				message.setQos(Constants.QoS_EXACTLY_ONCE);
				//MqttIntConnectionToHub.getObj().getMqttConnection().publish(publicTopic, message);
				
				logger.info("editScheduleProfile() MQTT Message Sent Successfully");
				
			}
		}catch(Exception e){
			logger.error("editScheduleProfile() Exception Message: "+e.getMessage()+"Cause: "+e.getCause());
			e.printStackTrace();
			throw new Exception(e);
		}	
		
		return null;
		
	}

}