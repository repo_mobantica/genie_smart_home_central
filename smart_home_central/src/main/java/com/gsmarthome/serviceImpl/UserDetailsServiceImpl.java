package com.gsmarthome.serviceImpl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;

import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;
import com.gsmarthome.constants.Constants;
import com.gsmarthome.controller.CommonController;
import com.gsmarthome.dto.EmailDTO;
import com.gsmarthome.dto.GuestUserDTO;
import com.gsmarthome.dto.GuestUserResponseDTO;
import com.gsmarthome.dto.ResponseDTO;
import com.gsmarthome.dto.UpdatePasswordDTO;
import com.gsmarthome.dto.UpdatePasswordOTPDTO;
import com.gsmarthome.dto.UpdateProfileDTO;
import com.gsmarthome.dto.UpdateUserDeviceDTO;
import com.gsmarthome.dto.UpdateUserTypeDTO;
import com.gsmarthome.dto.UserIdDTO;
import com.gsmarthome.dto.UserListDTO;
import com.gsmarthome.dto.UserShareControlDTO;
import com.gsmarthome.dto.UserdetailsResponseDTO;
import com.gsmarthome.dto.ValidateUserDTO;
import com.gsmarthome.dto.ValidateUserResponse;
import com.gsmarthome.dto.VerifyEmailOTP;
import com.gsmarthome.dto.VerifyOTPDTO;
import com.gsmarthome.dto.VerifyOTPResponse;
import com.gsmarthome.entity.DeviceDetails;
import com.gsmarthome.entity.Home;
import com.gsmarthome.entity.ShareControlDetails;
import com.gsmarthome.entity.UserDetails;
import com.gsmarthome.entity.UserOTP;
import com.gsmarthome.repository.DeviceDetailsRepository;
import com.gsmarthome.repository.HomeRepository;
import com.gsmarthome.repository.ShareControlDetailsRepository;
import com.gsmarthome.repository.UserDetailsRepository;
import com.gsmarthome.repository.UserOTPRepository;
import com.gsmarthome.security.domain.User;
import com.gsmarthome.security.token.TokenAuthenticationService;
import com.gsmarthome.service.UserDetailsService;
import com.gsmarthome.util.Utility;
import com.gsmarthome.util.ValidationErrorHandler;

import MqttReadDataPaho.MqttIntConnectionToHub;

@Component
public class UserDetailsServiceImpl implements UserDetailsService {

	final static Logger logger = Logger.getLogger(UserDetailsServiceImpl.class.getName());

	@Autowired
	UserDetailsRepository userDetailsRepository;

	@Autowired
	HomeRepository homeRepository;

	@Autowired
	UserOTPRepository userOTPRepository;

	@Autowired
	HTMLMailSender mailSender;

	@Autowired
	DeviceDetailsRepository deviceDetailsRepository;

	@Autowired
	ShareControlDetailsRepository shareControlDetailsRepository;

	@Autowired
	private TokenAuthenticationService tokenAuthenticationService;

	@Value("${API_STATUS_SUCCESS}")
	private String API_STATUS_SUCCESS;

	@Value("${API_STATUS_FAILURE}")
	private String API_STATUS_FAILURE;

	@Autowired
	private SecureRandom secureRandom;

	@Value("${EMAIL_ALREADY_PRESENT}")
	private String EMAIL_ALREADY_PRESENT;

	@Value("${PHONE_NUMBER_ALREADY_PRESENT}")
	private String PHONE_NUMBER_ALREADY_PRESENT;

	@Value("${USER_TYPE_SUPER_ADMIN}")
	private Integer USER_TYPE_SUPER_ADMIN;

	@Value("${USER_DETAILS_NOT_PRESENT}")
	private String USER_DETAILS_NOT_PRESENT;

	@Value("${USER_DETAILS_UPDATE_PASSWORD_MISMATCH}")
	private String USER_DETAILS_UPDATE_PASSWORD_MISMATCH;

	@Value("${USER_DETAILS_UPDATE_PASSWORD_SUCCESS}")
	private String USER_DETAILS_UPDATE_PASSWORD_SUCCESS;

	@Value("${USER_DETAILS_NOT_PRESENT_FOR_EMAIL}")
	private String USER_DETAILS_NOT_PRESENT_FOR_EMAIL;

	@Value("${USER_DETAILS_OTP_MISMATCH}")
	private String USER_DETAILS_OTP_MISMATCH;

	@Value("${USER_DETAILS_OTP_RECORD_NOT_FOUND}")
	private String USER_DETAILS_OTP_RECORD_NOT_FOUND;

	@Value("${INVALID_REQUEST}")
	private String INVALID_REQUEST;

	@Value("${DEVICE_ID_ALREADY_USED}")
	private String DEVICE_ID_ALREADY_USED;

	@Value("${DEVICE_ID_NOT_PRESENT}")
	private String DEVICE_ID_NOT_PRESENT;

	@Value("${SHARE_CONTROL_OTP_MISMATCH}")
	private String SHARE_CONTROL_OTP_MISMATCH;

	@Value("${SHARE_CONTROL_DETAILS_NOT_PRESENT}")
	private String SHARE_CONTROL_DETAILS_NOT_PRESENT;

	@Value("${EMAIL_ALREADY_PRESENT_AND_VERIFIED}")
	private String EMAIL_ALREADY_PRESENT_AND_VERIFIED;

	@Value("${EMAIL_ALREADY_PRESENT_AND_NOT_VERIFIED}")
	private String EMAIL_ALREADY_PRESENT_AND_NOT_VERIFIED;

	@Value("${USER_DETAILS_UPDATE_PROFILE_SUCCESS}")
	private String USER_DETAILS_UPDATE_PROFILE_SUCCESS;

	@Value("${PLEASE_ENTER_VALID_EMAIL}")
	private String PLEASE_ENTER_VALID_EMAIL;

	@Value("${IMAGE_LOCATION}")
	private String IMAGE_LOCATION;


	private static final SimpleDateFormat dateFormatByDate = new SimpleDateFormat("yyyy-MM-dd");


	@Override
	public ResponseDTO<UserDetails> save(UserDetails userDetails, Boolean shareControl, BindingResult bindingResult) {

		if (bindingResult.hasErrors()){

			logger.error("Binding result has error");
			return new ResponseDTO<UserDetails>(API_STATUS_FAILURE, null,
					ValidationErrorHandler.getErrorMessage(bindingResult));
		}
		if (userDetails.getId() == null) {
			UserDetails userDetailsByEmail = userDetailsRepository.findByEmail(userDetails.getEmail());
			if (userDetailsByEmail != null) {
				if (userDetailsByEmail.getIsEmailVerified()) {
					return new ResponseDTO<UserDetails>(API_STATUS_FAILURE, null, EMAIL_ALREADY_PRESENT_AND_VERIFIED);
				} else {
					sendOTP(new EmailDTO(userDetails.getEmail()));
					return new ResponseDTO<UserDetails>(API_STATUS_FAILURE, null,
							EMAIL_ALREADY_PRESENT_AND_NOT_VERIFIED);
				}
			} else if (userDetailsRepository.findByPhoneNumber(userDetails.getPhoneNumber()) != null)
				return new ResponseDTO<UserDetails>(API_STATUS_FAILURE, null, PHONE_NUMBER_ALREADY_PRESENT);

			userDetails.setIsEmailVerified(false);
			userDetails.setIsFirstLogin(true);
			userDetails.setUserType(null);


			UserDetails details = userDetailsRepository.save(userDetails);

			sendOTP(new EmailDTO(userDetails.getEmail()));
			/*
			HTMLMailSender3 HTMLMailSender3=new HTMLMailSender3();
			try {
				HTMLMailSender3.SendHTMLMail(userDetails.getEmail(),"hiiiii","hlll");
			}catch (Exception e) {
				e.printStackTrace();
				// TODO: handle exception
			}
			*/
			return new ResponseDTO<UserDetails>(API_STATUS_SUCCESS, details, API_STATUS_SUCCESS);
		} else {
			UserDetails details = userDetailsRepository.findOne(userDetails.getId());
			if (details == null)
				return new ResponseDTO<UserDetails>(API_STATUS_FAILURE, null, USER_DETAILS_NOT_PRESENT);
			if (userDetails.getEmail() != null)
				details.setEmail(userDetails.getEmail());
			if (userDetails.getFirstName() != null)
				details.setFirstName(userDetails.getFirstName());
			if (userDetails.getLastName() != null)
				details.setLastName(userDetails.getLastName());
			if (userDetails.getPhoneNumber() != null)
				details.setPhoneNumber(userDetails.getPhoneNumber());
			userDetailsRepository.save(details);
			logger.info("User Details Saved Successfully");
			return new ResponseDTO<UserDetails>(API_STATUS_SUCCESS, details, API_STATUS_SUCCESS);
		}

	}

	@Override
	public ResponseDTO<Boolean> updatePassword(UpdatePasswordDTO passwordDTO) {

		UserDetails details = userDetailsRepository.findOne(passwordDTO.getUserId());
		if (details == null)
			return new ResponseDTO<Boolean>(API_STATUS_FAILURE, false, USER_DETAILS_NOT_PRESENT);

		if (!passwordDTO.getOldPassword().equals(details.getPassword()))
			return new ResponseDTO<Boolean>(API_STATUS_FAILURE, false, USER_DETAILS_UPDATE_PASSWORD_MISMATCH);

		details.setPassword(passwordDTO.getNewPassword());
		userDetailsRepository.save(details);
		return new ResponseDTO<Boolean>(API_STATUS_SUCCESS, true, USER_DETAILS_UPDATE_PASSWORD_SUCCESS);
	}

	@Override
	public ResponseDTO<Boolean> updatePasswordOTP(UpdatePasswordOTPDTO passwordDTO) {

		UserDetails details = userDetailsRepository.findOne(passwordDTO.getUserId());
		if (details == null)
			return new ResponseDTO<Boolean>(API_STATUS_FAILURE, false, USER_DETAILS_NOT_PRESENT);

		details.setPassword(passwordDTO.getNewPassword());
		userDetailsRepository.save(details);
		return new ResponseDTO<Boolean>(API_STATUS_SUCCESS, true, USER_DETAILS_UPDATE_PASSWORD_SUCCESS);
	}

	@Override
	public ResponseDTO<UserIdDTO> sendOTP(EmailDTO emailDTO) {
		System.out.println("Send OPT code executed");

		UserDetails userDetails = userDetailsRepository.findByEmail(emailDTO.getEmail());
		if (userDetails == null)
			return new ResponseDTO<UserIdDTO>(API_STATUS_FAILURE, null, PLEASE_ENTER_VALID_EMAIL);
		else {
			UserOTP old = userOTPRepository.findByUserId(userDetails.getId());
			UserOTP userOTP = null;
			if (old != null) {
				old.setOtp(genarateMobilePin().toString());
				userOTP = old;
			} else {
				userOTP = new UserOTP(userDetails.getId(), genarateMobilePin().toString());
			}

			try {
				// String body=readFile("Mail.html");
				String body = Utility.readHTML();

				body = body.replaceAll("USER_NAME", userDetails.getFirstName());
				body = body.replaceAll("OTP_CODE", userOTP.getOtp());
				body = body.replaceAll("PROCESS", "user registration");

				//mailSender.SendHTMLMail(userDetails.getEmail(), "New OTP Request for user registration", body);
				// "Your one time password to complete user registration is " +
				// userOTP.getOtp() + ". Please do not share this OTP with
				// anyone.");

			} catch (Exception ex) {
				logger.error("Exception in Send OTP"+ex.getMessage());
			}
			// mailSenderJava.SendAWSMail(userDetails.getEmail(), "New OTP
			// Request", "<html><body>This is a text body. <strong>Foo
			// bar.</strong></body></html>");

			// mailSenderJava.sendEmail(userDetails.getEmail(), "New OTP
			// Request", "The OTP is " + userOTP.getOtp());
			String message = "To complete sign up process enter OTP " + userOTP.getOtp()
			+ ". We recommend not to share this OTP with any one.";
			boolean sms = CommonController.sendSMS(userDetails.getPhoneNumber(), message);
			logger.info("SMS Sent." + sms);
			logger.info("SMS messeage-------." + message);
			userOTPRepository.save(userOTP);
			return new ResponseDTO<UserIdDTO>(API_STATUS_SUCCESS, new UserIdDTO(userDetails.getId()), null);
		}

	}


	@Override
	public ResponseDTO<UserIdDTO> sendPasswordOTP(EmailDTO emailDTO) {
		
		System.out.println("Innnnnn sendPasswordOTP   *****************");
		
		logger.info("Send OTP code executed");

		UserDetails userDetails = userDetailsRepository.findByEmail(emailDTO.getEmail());
		if (userDetails == null)
			return new ResponseDTO<UserIdDTO>(API_STATUS_FAILURE, null, PLEASE_ENTER_VALID_EMAIL);
		else {
			UserOTP old = userOTPRepository.findByUserId(userDetails.getId());
			UserOTP userOTP = null;
			if (old != null) {
				old.setOtp(genarateMobilePin().toString());
				userOTP = old;
			} else {
				userOTP = new UserOTP(userDetails.getId(), genarateMobilePin().toString());
			}

			try {
				// String body=readFile("Mail.html");
				String body = Utility.readPaswordHTML();

				body = body.replaceAll("USER_NAME", userDetails.getFirstName());
				body = body.replaceAll("OTP_CODE", userOTP.getOtp());
				body = body.replaceAll("PROCESS", "forgot password");

				mailSender.SendHTMLMail(userDetails.getEmail(), "New OTP Request for forgot password", body);
				// "Your one time password to complete user registration is " +
				// userOTP.getOtp() + ". Please do not share this OTP with
				// anyone.");

			} catch (Exception ex) {
				ex.printStackTrace();
				logger.error("Send Password OTP Method, Exception Message: "+ex.getMessage()+" Cause:"+ex.getCause());

			}
			// mailSenderJava.SendAWSMail(userDetails.getEmail(), "New OTP
			// Request", "<html><body>This is a text body. <strong>Foo
			// bar.</strong></body></html>");

			// mailSenderJava.sendEmail(userDetails.getEmail(), "New OTP
			// Request", "The OTP is " + userOTP.getOtp());
			String message = "Enter OTP " +userOTP.getOtp() +" for forget password. We recommend not to share this OTP with any one.";
			boolean sms = CommonController.sendSMS(userDetails.getPhoneNumber(), message);
			logger.info("Send Password SMS Sent." + sms);
			logger.info("Send Password SMS messeage-------." + message);
			userOTPRepository.save(userOTP);
			return new ResponseDTO<UserIdDTO>(API_STATUS_SUCCESS, new UserIdDTO(userDetails.getId()), null);
		}

	}
	@Override
	public String sendHomeVerificationOTP(EmailDTO emailDTO) {

		UserDetails userDetails = userDetailsRepository.findByEmail(emailDTO.getEmail());
		if (userDetails == null)
			return null;
		else {
			UserOTP old = userOTPRepository.findByUserId(userDetails.getId());
			String OTP = genarateMobilePin().toString();
			UserOTP userOTP = null;
			if (old != null) {
				old.setOtp(OTP);
				userOTP = old;
			} else {
				userOTP = new UserOTP(userDetails.getId(), OTP);
			}
			String message = "To complete home verification enter OTP "+ userOTP.getOtp() + ". We recommend not to share this OTP with any one.";
			boolean sms = CommonController.sendSMS(userDetails.getPhoneNumber(), message);
			logger.info("sendHomeVerificationOTP SMS Sent." + sms);
			logger.info("sendHomeVerificationOTP SMS messeage-------." + message);
			// mailSenderJava.sendEmail(userDetails.getEmail(), "New OTP
			// Request", "The OTP is " + userOTP.getOtp());
			try {
				// String body=readFile("Mail.html");

				String body = Utility.readHTML();

				body = body.replaceAll("USER_NAME", userDetails.getFirstName());
				body = body.replaceAll("OTP_CODE", userOTP.getOtp());
				body = body.replaceAll("PROCESS", "home verification");

				//	mailSender.SendHTMLMail(userDetails.getEmail(), "New OTP Request for home verification", body);

			} catch (Exception ex) {

				logger.error("Exception in sendHomeVerificationOTP"+ex.getMessage());
			}

			userOTPRepository.save(userOTP);
			return OTP;
		}
	}

	public Integer genarateMobilePin() {

		int number = secureRandom.nextInt(9) + 1;

		for (int i = 0; i < 5; i++) {
			number = (number * 10) + secureRandom.nextInt(10);
		}
		logger.info("genarateMobilePin() Random number generated");
		return number;
	}

	@Override
	public ResponseDTO<VerifyOTPResponse> verifyOTP(VerifyOTPDTO emailDTO) {

		UserOTP userIdDTO = userOTPRepository.findByUserId(emailDTO.getUserId());
		if (userIdDTO != null && userIdDTO.getOtp().equalsIgnoreCase(emailDTO.getOtp())) {

			String x_auth_token = tokenAuthenticationService
					.getToken(new User(0, emailDTO.getUserId(), new Date().getTime() + 900000));

			logger.info("verifyOTP(): Authentication success "+API_STATUS_SUCCESS);
			return new ResponseDTO<VerifyOTPResponse>(API_STATUS_SUCCESS,
					new VerifyOTPResponse(userIdDTO.getUserId(), x_auth_token));
		} else {
			if (userIdDTO != null && !userIdDTO.getOtp().equalsIgnoreCase(emailDTO.getOtp())){
				logger.info("verifyOTP(): "+USER_DETAILS_OTP_MISMATCH);
				return new ResponseDTO<VerifyOTPResponse>(API_STATUS_FAILURE, null, USER_DETAILS_OTP_MISMATCH);
			}
		}
		logger.info("verifyOTP(): "+USER_DETAILS_OTP_MISMATCH);
		return new ResponseDTO<VerifyOTPResponse>(API_STATUS_FAILURE, null, null);
	}

	@Override
	public ResponseDTO<Boolean> verifyEmailOTP(VerifyEmailOTP emailDTO) {

		UserDetails userDetails = userDetailsRepository.findByEmail(emailDTO.getEmail());
		if (userDetails == null)
			return new ResponseDTO<Boolean>(API_STATUS_FAILURE, false, USER_DETAILS_NOT_PRESENT_FOR_EMAIL);

		UserOTP userIdDTO = userOTPRepository.findByUserId(userDetails.getId());

		if (userIdDTO != null && userIdDTO.getOtp().equalsIgnoreCase(emailDTO.getOtp())) {

			userDetails.setIsEmailVerified(true);

			userDetailsRepository.save(userDetails);

			return new ResponseDTO<Boolean>(API_STATUS_SUCCESS, true);
		} else {
			if (userIdDTO != null && !userIdDTO.getOtp().equalsIgnoreCase(emailDTO.getOtp()))
				return new ResponseDTO<Boolean>(API_STATUS_FAILURE, false, USER_DETAILS_OTP_MISMATCH);
		}
		return new ResponseDTO<Boolean>(API_STATUS_FAILURE, false, USER_DETAILS_OTP_RECORD_NOT_FOUND);
	}


	@Override
	public ResponseDTO<ValidateUserResponse> validateUser(ValidateUserDTO validateUserDTO) {
		ValidateUserResponse validateUserResponse= new ValidateUserResponse();

		System.out.println("validateUserDTO.getDeviceId()  ="+validateUserDTO.getDeviceId());
		System.out.println("validateUserDTO.getShareControlOTP()  ="+validateUserDTO.getShareControlOTP());
		System.out.println("validateUserDTO.getEmail()  ="+validateUserDTO.getEmail());



		if (validateUserDTO.getDeviceId() != null && validateUserDTO.getShareControlOTP() != null)
		{
			return new ResponseDTO<ValidateUserResponse>(API_STATUS_FAILURE, null, INVALID_REQUEST);
		}

		if (validateUserDTO.getDeviceId() != null) {

			DeviceDetails deviceDetails = deviceDetailsRepository.findOne(validateUserDTO.getDeviceId());

			if (deviceDetails == null){
				logger.error( "validateUser() "+API_STATUS_FAILURE+" "+DEVICE_ID_NOT_PRESENT);
				return new ResponseDTO<ValidateUserResponse>(API_STATUS_FAILURE, null, DEVICE_ID_NOT_PRESENT);
			}

			UserDetails userDetails = userDetailsRepository.findByEmail(validateUserDTO.getEmail());

			if (deviceDetails.getAdminUser() == null) {
				deviceDetails.setAdminUser(userDetails.getId());

				deviceDetailsRepository.save(deviceDetails);

				List<UserDetails> userList = new ArrayList<UserDetails>();

				userList.add(userDetails);

				Home home = new Home(userDetails.getFirstName() + " " + userDetails.getLastName() + "'s Home", userList,
						null);

				Home savedHome = homeRepository.save(home);

				userDetails.setHome(savedHome);
				userDetails.setUserType(USER_TYPE_SUPER_ADMIN);
				userDetails.setIsFirstLogin(false);
				userDetailsRepository.save(userDetails);
				validateUserResponse.setUserType(USER_TYPE_SUPER_ADMIN.toString());
				validateUserResponse.setHomeId(userDetails.getHome().getId().toString());
				return new ResponseDTO<ValidateUserResponse>(API_STATUS_SUCCESS, validateUserResponse, null);

			} else {
				UserDetails alreadyExist = userDetailsRepository.findOne(deviceDetails.getAdminUser());
				return new ResponseDTO<ValidateUserResponse>(API_STATUS_FAILURE, null,
						DEVICE_ID_ALREADY_USED + " by " + alreadyExist.getFirstName() + " " + alreadyExist.getLastName()
						+ " Please take share control from " + alreadyExist.getFirstName());
			}
		} else {
			ShareControlDetails controlDetails = shareControlDetailsRepository.findOne(validateUserDTO.getEmail());

			if (controlDetails == null){
				logger.error("validateUser() "+SHARE_CONTROL_DETAILS_NOT_PRESENT+ " "+API_STATUS_FAILURE);
				return new ResponseDTO<ValidateUserResponse>(API_STATUS_FAILURE, null, SHARE_CONTROL_DETAILS_NOT_PRESENT);
			}

			if (controlDetails.getOtp().equals(validateUserDTO.getShareControlOTP())) {
				UserDetails userDetailsValidate = userDetailsRepository.findByEmail(validateUserDTO.getEmail());

				UserDetails userDetails = userDetailsRepository.findOne(controlDetails.getAdminUser());

				Home home = userDetails.getHome();

				home.getUserList().add(userDetailsValidate);

				Home savedHome = homeRepository.save(home);
				logger.info("validateUser() home repository saved");

				userDetailsValidate.setUserType(controlDetails.getUserType());
				userDetailsValidate.setHome(savedHome);
				userDetailsValidate.setIsFirstLogin(false);
				userDetailsRepository.save(userDetailsValidate);
				logger.info("validateUser() user repository saved");
				validateUserResponse.setUserType(controlDetails.getUserType().toString());
				validateUserResponse.setHomeId(userDetails.getHome().getId().toString());

				logger.info("validateUser() user Validated Successfully");

				return new ResponseDTO<ValidateUserResponse>(API_STATUS_SUCCESS,validateUserResponse , "User Validated Successfully");

			} else {
				return new ResponseDTO<ValidateUserResponse>(API_STATUS_FAILURE, null, SHARE_CONTROL_OTP_MISMATCH);
			}
		}
	}

	@Override
	public ResponseDTO<Boolean> userShareControl(UserShareControlDTO shareControlDTO, BindingResult bindingResult) {


		JSONObject jMessage = new JSONObject();
		UserDetails adminUserDetails = userDetailsRepository.findOne(shareControlDTO.getAdminUserId());

		ShareControlDetails shareControlDetails = null;

		if (adminUserDetails == null)
			return new ResponseDTO<Boolean>(API_STATUS_FAILURE, false, USER_DETAILS_NOT_PRESENT);

		//check if share control user requested is present in user details table
		if(userDetailsRepository.findByEmail(shareControlDTO.getEmail()) != null && userDetailsRepository.findByPhoneNumber(shareControlDTO.getPhoneNumber()) != null)
		{		
			//Check if user email or phone number already present while giving share control

			if(shareControlDetailsRepository.findByEmail(shareControlDTO.getEmail()) == null &&
					shareControlDetailsRepository.findByPhoneNumber(shareControlDTO.getPhoneNumber()) == null){
				if(shareControlDTO.getUserType().equals(3)){
					shareControlDetails = new ShareControlDetails(shareControlDTO.getPhoneNumber(),shareControlDTO.getEmail(),
							shareControlDTO.getAdminUserId(), shareControlDTO.getUserType(), genarateMobilePin().toString(), shareControlDTO.getValidityDate());					
				}else{
					//for 0,1,2 user type date is not inserted in Database
					shareControlDetails = new ShareControlDetails(shareControlDTO.getPhoneNumber(),shareControlDTO.getEmail(),
							shareControlDTO.getAdminUserId(), shareControlDTO.getUserType(), genarateMobilePin().toString(),"");
				}

				shareControlDetailsRepository.save(shareControlDetails);

			}else{
				logger.error("userShareControl() User Email or Phone Number Already Present");
				return new ResponseDTO<Boolean>(API_STATUS_FAILURE, false, "User Email or Phone Number Already Present");
			}
		}else{
			logger.error("userShareControl() Requested User is not Registered for Share control");
			return new ResponseDTO<Boolean>(API_STATUS_FAILURE, false, "Requested User is not Registered for share control");
		}


		try {
			// String body=readFile("Mail.html");
			String body = Utility.readHTML();

			body = body.replaceAll("USER_NAME", shareControlDTO.getEmail());
			body = body.replaceAll("OTP_CODE", shareControlDetails.getOtp());
			body = body.replaceAll("PROCESS", "share control process");
			//mailSender.SendHTMLMail(shareControlDetails.getEmail(), "Share Control OTP", body);
			// mailSenderJava.SendAWSMail(shareControlDTO.getEmail(), "Share
			// Control OTP",
			// "Share Control OTP is:" + shareControlDetails.getOtp());

			//UserDetails deviceDetails = userDetailsRepository.findOne(shareControlDTO.getAdminUserId());
			Long homeId = adminUserDetails.getHome().getId();
			jMessage.put(Constants.EMAIL, shareControlDTO.getEmail().toString());
			jMessage.put(Constants.USER_TYPE, shareControlDTO.getUserType().toString());
			jMessage.put(Constants.ADMIN_USER_ID, shareControlDTO.getAdminUserId().toString());
			jMessage.put(Constants.PHONE_NUMBER, shareControlDTO.getPhoneNumber().toString());

			String publicTopic = "refreshIntGenieHomeId_" + homeId.toString();
			System.out.println("publicTopic ="+publicTopic);
			try {
				// refreshBackMessage to mobile or to other thin end clients
				MqttMessage message = new MqttMessage(("wish|" + "shareControl|" + jMessage.toString()).getBytes());
				message.setQos(Constants.QoS_EXACTLY_ONCE);
				//MqttIntConnectionToHub.getObj().getMqttConnection().publish("shareControl", message);
			}catch (Exception e) {
				System.out.println("error = "+e);
				// TODO: handle exception
			}

		} catch (Exception ex) {
			logger.error("Exception in userShareControl() Exception Message: "+ex.getMessage());
			ex.printStackTrace();
		}

		String messeage = "To complete the home share control process enter OTP "+ shareControlDetails.getOtp() +". We recommend not to share this otp with any one.";
			boolean sms = CommonController.sendSMS(shareControlDetails.getPhoneNumber(), messeage);
		return new ResponseDTO<Boolean>(API_STATUS_SUCCESS, true, "Share Control access given");

	}

	//This API Will Delete the Share Control User
	@Override
	public ResponseDTO<Boolean> deleteShareControlUser(UserShareControlDTO userShareControlDTO, BindingResult bindingResult) throws Exception{

		ShareControlDetails shareControlDetails = null;

		UserOTP userOTP = null;

		JSONObject jMessage = new JSONObject();

		//check if the the request is made by admin user
		UserDetails adminUserDetails = userDetailsRepository.findOne(userShareControlDTO.getAdminUserId());

		if (adminUserDetails == null)
			return new ResponseDTO<Boolean>(API_STATUS_FAILURE, false, USER_DETAILS_NOT_PRESENT);
		logger.info("Delete Share Control User Method, Admin User Details fetched");

		userOTP = userOTPRepository.findByUserId(adminUserDetails.getId());
		logger.info("Delete Share Control User Method, User OTP Details fetched");

		shareControlDetails = shareControlDetailsRepository.findOne(userShareControlDTO.getEmail());
		if(shareControlDetails == null)
			return new ResponseDTO<Boolean>(API_STATUS_FAILURE, false, SHARE_CONTROL_DETAILS_NOT_PRESENT);
		logger.info("Delete Share Control User Method, Share control Details Found");

		shareControlDetailsRepository.delete(shareControlDetails);
		logger.info("Delete Share Control User Method, Share control deleted Successfully");

		if(userOTP == null)
			return new ResponseDTO<Boolean>(API_STATUS_FAILURE, false, SHARE_CONTROL_DETAILS_NOT_PRESENT);

		userOTPRepository.delete(userOTP);
		logger.info("Delete Share Control User Method, User OTP deleted Successfully");


		//above share control access is deleted, now user details entry to be deleted
		UserDetails userDetails = userDetailsRepository.findByEmail(shareControlDetails.getEmail());
		userDetailsRepository.delete(userDetails);

		try {
			// String body=readFile("Mail.html");
			String body = Utility.readDeleteUserHTML();

			body = body.replaceAll("USER_NAME", userShareControlDTO.getEmail());
			//body = body.replaceAll("OTP_CODE", shareControlDetails.getOtp());
			body = body.replaceAll("PROCESS", "Delete Share Control Process");
			mailSender.SendHTMLMail(shareControlDetails.getEmail(), "Delete Share Control", body);
			logger.info("deleteShareControlUser(), E-Mail sent for share control user deleted Successfully");

			Long homeId = adminUserDetails.getHome().getId();
			jMessage.put(Constants.EMAIL, userShareControlDTO.getEmail().toString());
			jMessage.put(Constants.USER_TYPE, userShareControlDTO.getUserType().toString());
			jMessage.put(Constants.ADMIN_USER_ID, userShareControlDTO.getAdminUserId().toString());
			jMessage.put(Constants.PHONE_NUMBER, userShareControlDTO.getPhoneNumber().toString());

			String publicTopic = "refreshIntGenieHomeId_" + homeId.toString();
			// refreshBackMessage to mobile or to other thin end clients
			MqttMessage message = new MqttMessage(("wish|" + "delete_share_control|" + jMessage.toString()).getBytes());
			message.setQos(Constants.QoS_EXACTLY_ONCE);
		//	MqttIntConnectionToHub.getObj().getMqttConnection().publish(publicTopic, message);

		} catch (Exception ex) {
			logger.error("deleteShareControlUser(), Exception Message: "+ex.getMessage()+"Cause: "+ex.getCause());
			throw new Exception(ex);
		}

		String message = "Your are deactivatd from Go SmartHome mobile app.";
		boolean sms = CommonController.sendSMS(shareControlDetails.getPhoneNumber(), message);
		logger.info("deleteShareControlUser(), SMS sent for share control user deleted Successfully");
		return new ResponseDTO<Boolean>(API_STATUS_SUCCESS, true, null);

	}

	@Override
	public ResponseDTO<List<UserListDTO>> getByUser(UserIdDTO userIdDTO) {

		UserDetails userDetails = userDetailsRepository.findOne(userIdDTO.getUserId());

		System.out.println("NAme  ="+userDetails.getFirstName());
		List<UserListDTO> returnList = new ArrayList<UserListDTO>();

		if (userDetails == null)
			return new ResponseDTO<List<UserListDTO>>(API_STATUS_FAILURE, null, USER_DETAILS_NOT_PRESENT);

		List<UserDetails> userList = userDetails.getHome().getUserList();

		for (UserDetails details : userList) {
			returnList.add(new UserListDTO(details.getId(), details.getFirstName(), details.getLastName(),
					details.getEmail(), details.getPhoneNumber(), details.getUserType(), details.getImage(),
					details.getIsEmailVerified(), details.getIsFirstLogin(), details.getPassword()));

		}

		return new ResponseDTO<List<UserListDTO>>(API_STATUS_SUCCESS, returnList, null);
	}

	@Override
	public ResponseDTO<Boolean> updateUserType(UpdateUserTypeDTO updateUserTypeDTO) {

		UserDetails userDetails = userDetailsRepository.findOne(updateUserTypeDTO.getUserId());

		if (userDetails == null)
			return new ResponseDTO<Boolean>(API_STATUS_FAILURE, false, USER_DETAILS_NOT_PRESENT);

		ShareControlDetails shareControlDetails = shareControlDetailsRepository.findByEmail(userDetails.getEmail());

		if (shareControlDetails == null)
			return new ResponseDTO<Boolean>(API_STATUS_FAILURE, false, SHARE_CONTROL_DETAILS_NOT_PRESENT);

		userDetails.setUserType(updateUserTypeDTO.getNewUserType());

		userDetailsRepository.save(userDetails);

		shareControlDetails.setCreatedDate(updateUserTypeDTO.getValidityDate());
		shareControlDetails.setUserType(updateUserTypeDTO.getNewUserType());

		shareControlDetailsRepository.save(shareControlDetails);

		try {
			UserDetails deviceDetails = userDetailsRepository.findOne(updateUserTypeDTO.getAdminUserId());
			Long homeId = deviceDetails.getHome().getId();
			JSONObject jMessage = new JSONObject();
			jMessage.put(Constants.USER_ID, updateUserTypeDTO.getUserId().toString());
			jMessage.put(Constants.NEW_USER_TYPE, updateUserTypeDTO.getNewUserType().toString());
			jMessage.put(Constants.ADMIN_USER_ID, updateUserTypeDTO.getAdminUserId().toString());
			String publicTopic = "refreshIntGenieHomeId_" + homeId.toString();
			// refreshBackMessage to mobile or to other thin end clients
			MqttMessage message = new MqttMessage(("wish|" + "updateUserType|" + jMessage.toString()).getBytes());

			message.setQos(Constants.QoS_EXACTLY_ONCE);

			//MqttIntConnectionToHub.getObj().getMqttConnection().publish(publicTopic, message);
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error("Exception Message: "+ex.getMessage()+"Cause: "+ex.getCause());

		}
		return new ResponseDTO<Boolean>(API_STATUS_SUCCESS, true, null);
	}

	@Override
	public ResponseDTO<UserListDTO> updateProfile(UpdateProfileDTO updateProfileDTO) {


		UserDetails userDetails = userDetailsRepository.findOne(updateProfileDTO.getUserId());

		System.out.println("userDetails = "+userDetails.getId());
		UserListDTO userDetailsResponseDTO = new UserListDTO();

		if (userDetails == null)
			return new ResponseDTO<UserListDTO>(API_STATUS_FAILURE, userDetailsResponseDTO, USER_DETAILS_NOT_PRESENT);

		userDetails.setFirstName(updateProfileDTO.getFirstName());
		userDetails.setLastName(updateProfileDTO.getLastName());
		userDetails.setPhoneNumber(updateProfileDTO.getPhoneNumber());
		userDetails.setBirthDate(updateProfileDTO.getBirthDate());

		try {
		byte [] image = updateProfileDTO.getImage();
		String imagepath = "";
		if (image.length != 0) {
			//imagepath = CommonController.uploadImageToServer(image, updateProfileDTO.getUserId(),IMAGE_LOCATION);
			imagepath = CommonController.uploadImageToServer(updateProfileDTO.getImage(), updateProfileDTO.getUserId(),IMAGE_LOCATION);
		} else {
			imagepath = "";
		}
		if(!updateProfileDTO.getImageStatus().equalsIgnoreCase("NA")){
			userDetails.setImage(imagepath);
		}
		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		userDetailsResponseDTO.setId(userDetails.getId());
		userDetailsResponseDTO.setFirstName(userDetails.getFirstName());
		userDetailsResponseDTO.setLastName(userDetails.getLastName());
		userDetailsResponseDTO.setBirthDate(userDetails.getBirthDate());
		userDetailsResponseDTO.setImage(userDetails.getImage());

		userDetailsRepository.save(userDetails);

		return new ResponseDTO<UserListDTO>(API_STATUS_SUCCESS, userDetailsResponseDTO, USER_DETAILS_UPDATE_PROFILE_SUCCESS);
	}

	@Override
	public Boolean updateUserDevicetype(UpdateUserDeviceDTO updateUserDeviceDTO) {

		UserDetails details = userDetailsRepository.findOne(updateUserDeviceDTO.getUserId());

		if (details == null)
			return false;

		details.setDeviceId(updateUserDeviceDTO.getDeviceId());

		details.setDeviceType(updateUserDeviceDTO.getDeviceType());

		userDetailsRepository.save(details);

		return true;
	}

	@Override
	public ResponseDTO<UserDetails> syncShareControlData(UserdetailsResponseDTO userDetailsResponseDTO, Boolean shareControl,
			BindingResult bindingResult) {

		JSONObject jMessage = new JSONObject();

		if(bindingResult.hasErrors())
			return new ResponseDTO<UserDetails>(API_STATUS_FAILURE, null, ValidationErrorHandler.getErrorMessage(bindingResult));

		//fill the JSON Message if the userDetails is not null
		if(userDetailsResponseDTO != null){
			try {
				jMessage.put(Constants.ID, userDetailsResponseDTO.getId());
				jMessage.put(Constants.FIRST_NAME, userDetailsResponseDTO.getFirstName());
				jMessage.put(Constants.LAST_NAME, userDetailsResponseDTO.getLastName());
				jMessage.put(Constants.EMAIL, userDetailsResponseDTO.getEmail());
				jMessage.put(Constants.BIRTH_DATE, userDetailsResponseDTO.getBirthDate());
				jMessage.put(Constants.PASSWORD, userDetailsResponseDTO.getPassword());
				jMessage.put(Constants.PHONE_NUMBER, userDetailsResponseDTO.getPhoneNumber());
				jMessage.put(Constants.USER_TYPE, userDetailsResponseDTO.getUserType());
				jMessage.put(Constants.DEVICE_ID, userDetailsResponseDTO.getDeviceId());
				jMessage.put(Constants.DEVICE_TYPE, userDetailsResponseDTO.getDeviceType());
				jMessage.put(Constants.IS_EMAIL_VERIFIED, userDetailsResponseDTO.getIsEmailVerified());
				jMessage.put(Constants.IS_FIRST_LOGIN, userDetailsResponseDTO.getIsFirstLogin());
				jMessage.put(Constants.IMAGE, userDetailsResponseDTO.getImage());
				jMessage.put(Constants.HOME_ID, userDetailsResponseDTO.getHomeid());

				//Make MQTT Call to hit JAR file
				String publicTopic = "refreshIntGenieHomeId_" + userDetailsResponseDTO.getHomeid();

				// refreshBackMessage to mobile or to other thin end clients
				MqttMessage message = new MqttMessage(
						("wish|" + "sync_share_control_data|" + jMessage.toString()).getBytes());

				message.setQos(Constants.QoS_EXACTLY_ONCE);

			//	MqttIntConnectionToHub.getObj().getMqttConnection().publish(publicTopic, message);

			} catch (JSONException jsonException) {
				logger.error("Sync Share Control Data Method, JSON Message not valid for syncShareControlData"+"\nUserId: "+userDetailsResponseDTO.getId()
				+"\nHomeId: "+userDetailsResponseDTO.getHomeid());

				logger.error("Exception in "+jsonException.getMessage());

			}catch (Exception e) {
				logger.error("Sync Share Control Data Method, MQTT Call Exception Occured for syncShareControlData"+"\nUserId: "+userDetailsResponseDTO.getId()
				+"\nHomeId: "+userDetailsResponseDTO.getHomeid());

				logger.error("Exception in: "+e.getMessage());
				e.printStackTrace();					
			}

		}

		return null;

	}

	String readFile(String fileName) throws IOException {

		URL url = getClass().getResource(fileName);
		File file = new File(url.getPath());
		BufferedReader br = new BufferedReader(new FileReader(file));
		try {
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while (line != null) {
				sb.append(line);
				sb.append("\n");
				line = br.readLine();
			}
			return sb.toString();
		} finally {
			br.close();
		}
	}

	@Override
	public ResponseDTO<GuestUserResponseDTO> checkGuestUser(GuestUserDTO guestUserDTO) throws Exception{

		GuestUserResponseDTO guestUserResponseDTO = new GuestUserResponseDTO();

		JSONObject jMessage = new JSONObject();

		try{	

			UserDetails userDetails = userDetailsRepository.findOne(guestUserDTO.getUserId());

			UserDetails userDetailsByEmail = userDetailsRepository.findByEmail(guestUserDTO.getEmailId());

			ShareControlDetails shareControlDetails = shareControlDetailsRepository.findByEmail(guestUserDTO.getEmailId());

			logger.info("from SECOND is " + dateFormatByDate.format(new Date()));

			if(shareControlDetails != null)
			{
				if(shareControlDetails.getUserType().equals(3) || shareControlDetails.getUserType() == 3){

					String strDate = shareControlDetails.getCreatedDate();
					Date shareControlDate = new SimpleDateFormat("yyyy-MM-dd").parse(strDate);
					Date currDate = new SimpleDateFormat("yyyy-MM-dd").parse(dateFormatByDate.format(new Date()));

					if(shareControlDate.before(currDate) || shareControlDate.equals(currDate)){

						shareControlDetailsRepository.delete(shareControlDetails);
						logger.info("Guest User Deleted");

						//Guest user once register can login again next time whenever admin user adds him again as a guest or any different user
						//even if validity of guest user is ended once hence not deleting the guest user from user Details
						Integer updateStatus = userDetailsRepository.updateUserIsFirstTimeLogin(shareControlDetails.getEmail(), true);

						if(updateStatus.equals(1)){
							logger.info("User updated Successfully");

							jMessage.put(Constants.EMAIL, shareControlDetails.getEmail().toString());
							jMessage.put(Constants.USER_TYPE, shareControlDetails.getUserType().toString());
							jMessage.put(Constants.SCHEDULAR_DATE_TIME, shareControlDetails.getCreatedDate().toString());							
							jMessage.put(Constants.STATUS, "Share Control Deleted");

							String publicTopic = "refreshIntGenieHomeId_" + userDetails.getHome().getId().toString();
							// refreshBackMessage to mobile or to other thin end clients
							MqttMessage message = new MqttMessage(("wish|" + "delete_guest_user|" + jMessage.toString()).getBytes());
							message.setQos(Constants.QoS_EXACTLY_ONCE);
						//	MqttIntConnectionToHub.getObj().getMqttConnection().publish(publicTopic, message);
						}	
					}else{
						guestUserResponseDTO.setMsg("Date has not reached");
						guestUserResponseDTO.setStatus("1");
						return new ResponseDTO<GuestUserResponseDTO>(API_STATUS_SUCCESS, guestUserResponseDTO, null);
					}

				}else{
					guestUserResponseDTO.setMsg("No Guest User Found");
					guestUserResponseDTO.setStatus("0");
					return new ResponseDTO<GuestUserResponseDTO>(API_STATUS_SUCCESS, guestUserResponseDTO, null);	
				}
			}else if(userDetails == null && shareControlDetails == null && userDetailsByEmail == null){
				guestUserResponseDTO.setMsg("User Does Not exist or Deleted");
				guestUserResponseDTO.setStatus("2");
				return new ResponseDTO<GuestUserResponseDTO>(API_STATUS_SUCCESS, guestUserResponseDTO, null);				
			}
			else{
				guestUserResponseDTO.setMsg("Share Control Details not found");
				guestUserResponseDTO.setStatus("3");
				return new ResponseDTO<GuestUserResponseDTO>(API_STATUS_SUCCESS, guestUserResponseDTO, null);
			}

			guestUserResponseDTO.setMsg("Guest User Deleted");
			guestUserResponseDTO.setStatus("2");
			return new ResponseDTO<GuestUserResponseDTO>(API_STATUS_SUCCESS, guestUserResponseDTO, null);
		} catch (Exception ex) {
			logger.info("Error in Deleting the Guest user, Exception Message: "+ex.getMessage());
			ex.printStackTrace();
			throw new Exception(ex);
		}
	}

	@Override
	public UserDetails getUserDetails(long id) {



		UserDetails userDetails = userDetailsRepository.findOne(id);


		// TODO Auto-generated method stub
		return userDetails;
	}
}
