package com.gsmarthome.serviceImpl;




import com.amazonaws.services.simpleemail.*;
import com.amazonaws.services.simpleemail.model.*;
import com.amazonaws.regions.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;




@Component
public class MailSenderJava {
/*	
	@Autowired
	private JavaMailSender javaMailSender;
	*/
	
	    String FROM = "";   // Replace with your "From" address. This address must be verified.
	 //  String TO = "deepraj.mobantica@gmail.com";  // Replace with a "To" address. If your account is still in the 
	                                                       // sandbox, this address must be verified.
	    
	  //  String BODY = "This email was sent through the Amazon SES SMTP interface by using Java.";
	  //  String SUBJECT = "Amazon SES test (SMTP interface accessed using Java)";
	    
	    // Supply your SMTP credentials below. Note that your SMTP credentials are different from your AWS credentials.
	    String SMTP_USERNAME = "AKIAIIGD46PVKW4NVTUQ";  // Replace with your SMTP username.
	    String SMTP_PASSWORD = "As5da+NgEgY7IPrr7UlGCXBpwL3XruXefyQ1rTf72KJE";  // Replace with your SMTP password.
	    /* old one : ses-smtp-user.20170103-160334
SMTP Username:
AKIAJI3TGJMRXSGAPFKA
SMTP Password:
AhNC9yXogHjLOKfqI+70+NpKxJnIulftxVHAsYsdofVO
*/
	    
	    /*New one 
	     *  ses-smtp-user.20170104-131955
SMTP Username:
AKIAIIGD46PVKW4NVTUQ
SMTP Password:
As5da+NgEgY7IPrr7UlGCXBpwL3XruXefyQ1rTf72KJE

	     */
	    
	    // Amazon SES SMTP host name. This example uses the US West (Oregon) region.
	    //String HOST = "email-smtp.us-west-2.amazonaws.com";
	    String HOST = "email-smtp.us-west-2.amazonaws.com";
	  //  String HOST ="email-smtp.us-east-1.amazonaws.com";
	    
	    // The port you will connect to on the Amazon SES SMTP endpoint. We are choosing port 25 because we will use
	    // STARTTLS to encrypt the connection.
	     int PORT = 465;
	     //int PORT = 587;
	  public void SendAWSMail(String TO , String SUBJECT,String BODY ) {
	    /*	try
	    	{

	        // Create a Properties object to contain connection configuration information.
	    	Properties props = System.getProperties();
	    	props.put("mail.transport.protocol", "smtps");
	    	props.put("mail.smtp.port", PORT); 
	    	
	    	// Set properties indicating that we want to use STARTTLS to encrypt the connection.
	    	// The SMTP session will begin on an unencrypted connection, and then the client
	        // will issue a STARTTLS command to upgrade to an encrypted connection.
	    	props.put("mail.smtp.auth", "true");
	    	props.put("mail.smtp.starttls.enable", "true");
	    	props.put("mail.smtp.starttls.required", "true");

	        // Create a Session object to represent a mail session with the specified properties. 
	    	Session session = Session.getDefaultInstance(props);

	        // Create a message with the specified information. 
	        MimeMessage msg = new MimeMessage(session);
	        msg.setFrom(new InternetAddress(FROM)); 
	        msg.setRecipient(javax.mail.Message.RecipientType.TO, new InternetAddress(TO));
	        msg.setSubject(SUBJECT);
	        msg.setContent(BODY,"text/plain");
	            
	        // Create a transport.        
	        Transport transport = session.getTransport();
	                    
	        // Send the message.
	        try
	        {
	            System.out.println("Attempting to send an email through the Amazon SES SMTP interface...");
	            
	            // Connect to Amazon SES using the SMTP username and password you specified above.
	            transport.connect(HOST, SMTP_USERNAME, SMTP_PASSWORD);
	        	
	            // Send the email.
	            transport.sendMessage(msg, msg.getAllRecipients());
	            System.out.println("Email sent!");
	        }
	        catch (Exception ex) {
	            System.out.println("The email was not sent.");
	            System.out.println("Error message: " + ex.getMessage());
	        }
	        finally
	        {
	            // Close and terminate the connection.
	            transport.close();        	
	        }
	    	}
	    	catch(Exception ex)
	    	{
	    		
	    	}*/
	    	
		  System.out.println("SendAWSMail executed");

	    	
	    	 // Construct an object to contain the recipient address.
	        Destination destination = new Destination().withToAddresses(new String[]{TO});
	        
	        // Create the subject and body of the message.
	        Content subject = new Content().withData(SUBJECT);
	        Content textBody = new Content().withData(BODY); 
	       
	    //  Body body = new Body().withText(textBody);
	       	Body body= new Body().withHtml(textBody);
	        // Create a message with the specified subject and body.
	        com.amazonaws.services.simpleemail.model.Message message = new com.amazonaws.services.simpleemail.model.Message().withSubject(subject).withBody(body);
	        
	        // Assemble the email.
	        SendEmailRequest request = new SendEmailRequest().withSource(FROM).withDestination(destination).withMessage(message);
	        
	        try
	        {        
	            System.out.println("Attempting to send an email through Amazon SES by using the AWS SDK for Java...");
	        
	            // Instantiate an Amazon SES client, which will make the service call. The service call requires your AWS credentials. 
	            // Because we're not providing an argument when instantiating the client, the SDK will attempt to find your AWS credentials 
	            // using the default credential provider chain. The first place the chain looks for the credentials is in environment variables 
	            // AWS_ACCESS_KEY_ID and AWS_SECRET_KEY. 
	            // For more information, see http://docs.aws.amazon.com/sdk-for-java/v1/developer-guide/credentials.html
	            
	        	AWSCredentials awsCreds = new BasicAWSCredentials("AKIAJ4KLS4STAJDKO7CA",
						"NhtxiVG9it4CAlsazRigvNWBisqHw7IWOD04MkN0");
	        	
				//AmazonS3Client s3client = new AmazonS3Client(awsCreds);
			//	s3client.setRegion(Region.getRegion(Regions.US_WEST_2));
	      //      AmazonSimpleEmailServiceClient client = new AmazonSimpleEmailServiceClient(awsCreds);
	            
	            AmazonSimpleEmailServiceClient client = new AmazonSimpleEmailServiceAsyncClient(awsCreds);
	           // client.setRegion(Regions.US_WEST_2);
	            // Choose the AWS region of the Amazon SES endpoint you want to connect to. Note that your sandbox 
	            // status, sending limits, and Amazon SES identity-related settings are specific to a given AWS 
	            // region, so be sure to select an AWS region in which you set up Amazon SES. Here, we are using 
	            // the US West (Oregon) region. Examples of other regions that Amazon SES supports are US_EAST_1 
	            // and EU_WEST_1. For a complete list, see http://docs.aws.amazon.com/ses/latest/DeveloperGuide/regions.html 
	            Region REGION = Region.getRegion(Regions.US_WEST_2);
	            client.setRegion(REGION);
	       
	            // Send the email.
	            client.sendEmail(request);  
	            System.out.println("Email sent!");
	        }
	        catch (Exception ex) 
	        {
	            System.out.println("The email was not sent.");
	            System.out.println("Error message: " + ex.getMessage());
	        }
	    }
	    
		public void sendEmail(String email,String subject,String body)
		{
			SimpleMailMessage mailMessage = new SimpleMailMessage();
	        mailMessage.setTo(email);
	        mailMessage.setReplyTo("someone@localhost");
	        mailMessage.setFrom("someone@localhost");
	        mailMessage.setSubject(subject);
	        mailMessage.setText(body);
	        
	        Thread thread=new Thread(()->{
	        	//javaMailSender.send(mailMessage);	
	        });
	        
	        thread.start();
		}


}
