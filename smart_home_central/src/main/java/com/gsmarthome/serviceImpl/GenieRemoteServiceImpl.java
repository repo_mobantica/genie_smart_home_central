package com.gsmarthome.serviceImpl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.gsmarthome.dto.RemoteIRRequestDTO;
import com.gsmarthome.dto.RemoteIRResponseDTO;
import com.gsmarthome.dto.ResponseDTO;
import com.gsmarthome.entity.RemoteDetails;
import com.gsmarthome.entity.UserDetails;
import com.gsmarthome.repository.RemoteRepository;
import com.gsmarthome.repository.UserDetailsRepository;
import com.gsmarthome.service.IGenieRemoteService;


@Component
public class GenieRemoteServiceImpl implements IGenieRemoteService {

	static Logger logger = Logger.getLogger(GenieRemoteServiceImpl.class.getName());

	@Autowired
	RemoteRepository remoteRepository;
	
	@Autowired
	UserDetailsRepository userDetailsRepository;
	
	@Value("${API_STATUS_SUCCESS}")
	private String API_STATUS_SUCCESS;

	@Value("${API_STATUS_FAILURE}")
	private String API_STATUS_FAILURE;
	
	@Override
	public ResponseDTO<RemoteIRResponseDTO> remoteIRStorage(RemoteIRRequestDTO remoteIRReqDTO) {
		RemoteDetails remoteDetails = new RemoteDetails();
		RemoteIRResponseDTO remoteResponseDTO = new RemoteIRResponseDTO();
		
		UserDetails userDetails = userDetailsRepository.findOne(remoteIRReqDTO.getUserId());
		logger.info("remote ir storage Method, userDetails fetched");
		
		if(userDetails != null){
			logger.info("remote ir storage Method, userDetails found successfully");
					
			remoteDetails.setRemoteCommandInfo(remoteIRReqDTO.getIrRemoteinfo());
			
			if(userDetails.getHome().getId().equals(remoteIRReqDTO.getHomeId())){
				
				logger.info("remote ir storage Method, Home id Successfully validated");
				remoteDetails.setHome(userDetails.getHome());
				
			}else{
				logger.error("remote ir storage Method, invalid Home id");
				return new ResponseDTO<RemoteIRResponseDTO>(API_STATUS_FAILURE,remoteResponseDTO,"Home ID does not exist");
			}
			
			remoteRepository.save(remoteDetails);
			
			remoteResponseDTO.setUserId(remoteIRReqDTO.getUserId().toString());
			remoteResponseDTO.setIrRemoteinfo(remoteDetails.getRemoteCommandInfo());
		}else{
			logger.error("remote ir storage Method, invalid user id");
			return new ResponseDTO<RemoteIRResponseDTO>(API_STATUS_FAILURE,remoteResponseDTO,"User Details not found");
		}
		
		return new ResponseDTO<RemoteIRResponseDTO>(API_STATUS_SUCCESS,remoteResponseDTO,"");
	}

	@Override
	public ResponseDTO<List<RemoteDetails>> getAllIrRemote(RemoteIRRequestDTO remoteIRReqDTO) {
		
		List<RemoteDetails> listRemoteDetails = new ArrayList<RemoteDetails>();
		
		UserDetails userDetails = userDetailsRepository.findOne(remoteIRReqDTO.getUserId());
		logger.info("get all IR remote Method, userDetails fetched");
		
		if(userDetails != null){
			logger.info("get all IR remote Method, UserDetails found successfully");
			
			Iterable<RemoteDetails> iterableRemoteDetails = remoteRepository.findByHome(userDetails.getHome());
			
			Iterator<RemoteDetails>  itrRemoteDetails = iterableRemoteDetails.iterator();
			
			while(itrRemoteDetails.hasNext()){
				listRemoteDetails.add(itrRemoteDetails.next());
			}
			
			return new ResponseDTO<List<RemoteDetails>>(API_STATUS_SUCCESS,listRemoteDetails,"");
		}else{
			logger.error("get all IR remote Method, invalid user id");
			return new ResponseDTO<List<RemoteDetails>>(API_STATUS_FAILURE,listRemoteDetails,"User Details not found");
		}
		
	}

}
