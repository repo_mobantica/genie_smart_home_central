package com.gsmarthome.serviceImpl;

import java.util.Properties;
import java.util.logging.Logger;

import javax.mail.*;
import javax.mail.internet.*;
import org.springframework.stereotype.Component;

@Component
public class HTMLMailSender3 {

	final static Logger logger = Logger.getLogger(HTMLMailSender3.class.getName());

	String FROM = "bahirnath@mobantica.com"; // Replace with your "From"
												// address. This address must be
												// verified.
	// String TO = "kapeel45@gmail.com"; // Replace with a "To" address. If your
	// account is still in the
	// sandbox, this address must be verified.

	// String BODY = "This email was sent************** something else
	// ********through the Amazon SES SMTP interface by using Java.";
	// String SUBJECT = "Amazon SES test (SMTP interface accessed using Java)";

	// Supply your SMTP credentials below. Note that your SMTP credentials are
	// different from your AWS credentials.
	String SMTP_USERNAME = "bahirnath@mobantica.com"; // Replace with your
														// SMTP username.
	String SMTP_PASSWORD = "98221974111"; // Replace with your SMTP password.

	// Amazon SES SMTP host name. This example uses the US West (Oregon) region.
	String HOST = "smtp.gmail.com";
	// String HOST = "smtp-relay.gmail.com";

	// The port you will connect to on the Amazon SES SMTP endpoint. We are
	// choosing port 25 because we will use
	// STARTTLS to encrypt the connection.
	int PORT = 587;

	public void SendHTMLMail(String TO, String SUBJECT, String BODY) throws Exception {
System.out.println("INNNNNN");
		// Create a Properties object to contain connection configuration
		// information.
		Properties props = System.getProperties();
		props.put("mail.transport.protocol", "smtps");
		props.put("mail.smtp.port", PORT);


		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		// props.put("mail.smtp.starttls.required", "true");

		// Create a Session object to represent a mail session with the
		// specified properties.
		Session session = Session.getDefaultInstance(props);

		// Create a message with the specified information.
		MimeMessage msg = new MimeMessage(session);
		msg.setFrom(new InternetAddress(FROM));

		msg.setRecipient(Message.RecipientType.TO, new InternetAddress(TO));
		msg.setSubject(SUBJECT);
		msg.setContent(BODY, "text/html");

		// Create a transport.
		Transport transport = session.getTransport("smtp");

		// Send the message.
		try {
			logger.info("Attempting to send an email through the Amazon SES SMTP interface...");

			// Connect to Amazon SES using the SMTP username and password you
			// specified above.
			transport.connect(HOST, SMTP_USERNAME, SMTP_PASSWORD);

			// Send the email.
			transport.sendMessage(msg, msg.getAllRecipients());
			logger.info("Email sent!");
		} catch (Exception ex) {
			logger.info("The email is not sent.");
			logger.info("Error message: " + ex.getMessage());
		} finally {
			// Close and terminate the connection.
			transport.close();
		}
	}

}
