package com.gsmarthome.serviceImpl;

import java.util.Properties;
import java.util.logging.Logger;

import javax.mail.*;
import javax.mail.internet.*;
import org.springframework.stereotype.Component;

@Component
public class HTMLMailSender {

	final static Logger logger = Logger.getLogger(HTMLMailSender.class.getName());

	public void SendHTMLMail(String TO, String SUBJECT, String BODY) throws Exception {
		// Get properties object
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");
		// get Session
		Session session = null;
		try {
			session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication("", "");
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			MimeMessage message = new MimeMessage(session);
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(TO));
			message.setSubject(SUBJECT);
			message.setText(BODY);
			// send message
			//Transport.send(message);
			System.out.println("message sent successfully");
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}

	}

}
