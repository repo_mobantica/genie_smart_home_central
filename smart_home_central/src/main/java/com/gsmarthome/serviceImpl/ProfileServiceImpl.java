package com.gsmarthome.serviceImpl;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.amazonaws.util.json.JSONObject;
import com.gsmarthome.constants.Constants;
import com.gsmarthome.dto.ChangeStatusByHomeDTO;
import com.gsmarthome.dto.ChangeStatusByRoomResponseDTO;
import com.gsmarthome.dto.ProfileRequestDTO;
import com.gsmarthome.dto.ProfileResponseDTO;
import com.gsmarthome.entity.UserDetails;
import com.gsmarthome.repository.UserDetailsRepository;
import com.gsmarthome.service.ProfileService;

import MqttReadDataPaho.MqttIntConnectionToHub;

@Component
public class ProfileServiceImpl implements ProfileService {
	
	/* Get actual class name to be printed on */
	static Logger logger = Logger.getLogger(ProfileServiceImpl.class.getName());

	public static final String TURN = " turn "; //purposely added the space before and after turn
	
	public static final String SET_DIMMER_VALUE_WHEN_ON = "38"; //0-75 is dimmer value to hub hence 50% of it is 37avg
	
	@Autowired
	UserDetailsRepository userDetailsRepository;

	@Value("${SWITCH_NAME_ALREADY_PRESENT}")
	private String SWITCH_NAME_ALREADY_PRESENT;

	@Value("${IMAGE_LOCATION}")
	private String IMAGE_LOCATION;
	
	@Override
	public List<ChangeStatusByRoomResponseDTO> profileOnOff(ChangeStatusByHomeDTO changeStatusByHomeDTO) throws Exception{
		
		try {
			UserDetails deviceDetails = userDetailsRepository.findOne(changeStatusByHomeDTO.getUserId());
			Long homeId = deviceDetails.getHome().getId();
			JSONObject jMessage = new JSONObject();

			jMessage.put(Constants.PROFILE_ID, changeStatusByHomeDTO.getProfileId());
			jMessage.put(Constants.USER_ID, deviceDetails.getId().toString());
			jMessage.put(Constants.MESSAGE_FROM, changeStatusByHomeDTO.getMessageFrom());
			String publicTopic = "refreshIntGenieHomeId_" + homeId.toString();
			// refreshBackMessage to mobile or to other thin end clients
			MqttMessage message = new MqttMessage(("wish|" + "turnOffAll|" + jMessage.toString()).getBytes());
			message.setQos(Constants.QoS_EXACTLY_ONCE);
			//MqttIntConnectionToHub.getObj().getMqttConnection().publish(publicTopic, message);
			
			return null;

		} catch (Exception e) {
			logger.error("Exception Message: "+e.getMessage()+"");
			throw new Exception(e);
		}
	}


	@Override
	public ProfileResponseDTO addProfileMode(ProfileRequestDTO profileRequestDTO) throws Exception {
		
		JSONObject jMessage = new JSONObject();
		String homeId = "";
		
		try{
			UserDetails userDetails = userDetailsRepository.findOne(profileRequestDTO.getUserId());
			
			homeId = userDetails.getHome().getId().toString();
			
			jMessage.put(Constants.USER_ID, userDetails.getId().toString());
			jMessage.put(Constants.PROFILE_NAME, profileRequestDTO.getProfileName());
			jMessage.put(Constants.SWITCH_LIST_PROFILE, profileRequestDTO.getSwitchList());
			jMessage.put(Constants.DIMMER_VALUES, profileRequestDTO.getDimmerValues());
			jMessage.put(Constants.SWITCH_STATUS, profileRequestDTO.getSwitchStatus());
			jMessage.put(Constants.MESSAGE_FROM, profileRequestDTO.getMessageFrom());
			
			String publicTopic = "refreshIntGenieHomeId_" + homeId;
			MqttMessage message = new MqttMessage(("wish|" + "add_profile|" + jMessage.toString()).getBytes());
			message.setQos(Constants.QoS_EXACTLY_ONCE);
			//MqttIntConnectionToHub.getObj().getMqttConnection().publish(publicTopic, message);
		
			return null;
		
		}catch(Exception exception){
			
			logger.error("Exception in addProfileMode() Exception Message: "+exception.getMessage());
			exception.printStackTrace();
			throw new Exception(exception);
		}
		
	}


	@Override
	public ProfileResponseDTO editProfileMode(ProfileRequestDTO profileRequestDTO) throws Exception {
		JSONObject jMessage = new JSONObject();
		String homeId = "";
		try{
			
			UserDetails userDetails = userDetailsRepository.findOne(profileRequestDTO.getUserId());
			
			homeId = userDetails.getHome().getId().toString();
			
			logger.info("addProfileMode(), Profile data saved Successfully");
			
			jMessage.put(Constants.USER_ID, userDetails.getId().toString());
			jMessage.put(Constants.PROFILE_ID, profileRequestDTO.getProfileId().toString());
			jMessage.put(Constants.PROFILE_NAME, profileRequestDTO.getProfileName());
			jMessage.put(Constants.SWITCH_LIST_PROFILE, profileRequestDTO.getSwitchList());
			jMessage.put(Constants.DIMMER_VALUES, profileRequestDTO.getDimmerValues());
			jMessage.put(Constants.SWITCH_STATUS, profileRequestDTO.getSwitchStatus());
			jMessage.put(Constants.MESSAGE_FROM, profileRequestDTO.getMessageFrom());
			
			logger.info("addProfileMode(), JSON Message Created: "+jMessage);
			
			String publicTopic = "refreshIntGenieHomeId_" + homeId;
			MqttMessage message = new MqttMessage(("wish|" + "edit_profile|" + jMessage.toString()).getBytes());
			message.setQos(Constants.QoS_EXACTLY_ONCE);
			//MqttIntConnectionToHub.getObj().getMqttConnection().publish(publicTopic, message);
			
			return null;
			
		}catch(Exception exception){
			logger.error("Exception in addProfileMode() Exception Message: "+exception.getMessage());
			exception.printStackTrace();
			throw new Exception(exception);
		}
		
	}


	@Override
	public ProfileResponseDTO deleteProfileMode(ProfileRequestDTO profileRequestDTO) throws Exception {
		
		JSONObject jMessage = new JSONObject();
		String excepMessage = "";
		String homeId = "";
		String publicTopic = "";
		
		try{
			UserDetails userDetails = userDetailsRepository.findOne(profileRequestDTO.getUserId());
		
			homeId = userDetails.getHome().getId().toString();
			
			jMessage.put(Constants.USER_ID, userDetails.getId().toString());
			jMessage.put(Constants.PROFILE_ID, profileRequestDTO.getProfileId().toString());
			jMessage.put(Constants.MESSAGE_FROM, profileRequestDTO.getMessageFrom());
			
			logger.info("deleteProfileMode(), JSON Message Created: "+jMessage);
			
			if(homeId.isEmpty() || homeId.equals("")){
				throw new Exception("Home id not found");
			}else{
				publicTopic = "refreshIntGenieHomeId_" + homeId;
			}
			
			MqttMessage message = new MqttMessage(("wish|" + "delete_profile|" + jMessage.toString()).getBytes());
			message.setQos(Constants.QoS_EXACTLY_ONCE);
			//MqttIntConnectionToHub.getObj().getMqttConnection().publish(publicTopic, message);
			
			return null;
			
		}catch(Exception exception){
			excepMessage = "Error while deleting Profile Mode. Profile Id: "+profileRequestDTO.getProfileId();
			logger.error("Exception in deleteProfileMode(), "+excepMessage+" Exception Message: "+exception.getMessage()+" User Id: "+profileRequestDTO.getUserId());
			exception.printStackTrace();
			throw new Exception(exception);
		}
		
	}

	@Override
	public List<ProfileResponseDTO> getProfileList(ProfileRequestDTO profileRequestDTO) throws Exception {
		
		JSONObject jMessage = new JSONObject();
		
		String excepMessage = "";
		String homeId = "";
		Date dateCurrent = new Date();
		
		try{
			
			UserDetails userDetails = userDetailsRepository.findOne(profileRequestDTO.getUserId());
			
			homeId = userDetails.getHome().getId().toString();
			
			jMessage.put(Constants.USER_ID, profileRequestDTO.getUserId().toString());
			jMessage.put(Constants.TIME, ""+dateCurrent.getSeconds());
			jMessage.put(Constants.MESSAGE_FROM, profileRequestDTO.getMessageFrom());
			
			logger.info("getProfileList(), JSON Message Created: "+jMessage);
			
			String publicTopic = "refreshIntGenieHomeId_" + homeId;
			MqttMessage message = new MqttMessage(("wish|" + "get_profile_list|" + jMessage.toString()).getBytes());
			message.setQos(Constants.QoS_EXACTLY_ONCE);
			//MqttIntConnectionToHub.getObj().getMqttConnection().publish(publicTopic, message);
			
		}catch(Exception exception){
			excepMessage = "Error while Fetching Profile Mode List. Profile Id: "+profileRequestDTO.getProfileId();
			logger.error("Exception in getProfileList(), "+excepMessage+" Exception Message: "+exception.getMessage()+" User Id: "+profileRequestDTO.getUserId());
			exception.printStackTrace();
			throw new Exception(exception);
		}
		
		return null;
	}

}