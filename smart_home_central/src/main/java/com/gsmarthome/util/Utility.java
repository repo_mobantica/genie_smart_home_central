package com.gsmarthome.util;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.gsmarthome.constants.Constants;

public class Utility{

	
	public static String theMonth(int month) {
		String[] monthNames = { Constants.JAN, Constants.FEB, Constants.MAR, Constants.APR, Constants.MAY, Constants.JUN, 
				Constants.JUL, Constants.AUG, Constants.SEP, Constants.OCT, Constants.NOV, Constants.DEC };
		return monthNames[month];
	}

	public static String getTime(String time) {
		String time1 = "";

		try {
			String _24HourTime = time;
			SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm");
			SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");
			Date _24HourDt = _24HourSDF.parse(_24HourTime);
			System.out.println(_24HourDt);
			System.out.println(_12HourSDF.format(_24HourDt));
			time1 = _12HourSDF.format(_24HourDt);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return time1;
	}
	
	//checks for connection to the internet through dummy request
    public static boolean isInternetReachable()
    {
        try {
            //make a URL to a known source
            URL url = new URL("http://www.google.com");

            //open a connection to that source
            HttpURLConnection urlConnect = (HttpURLConnection)url.openConnection();

            //trying to retrieve data from the source. If there
            //is no connection, this line will fail
            Object objData = urlConnect.getContent();

        } catch (UnknownHostException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }
        return true;
    }
    
    public static String readHTML() {

		StringBuffer data = new StringBuffer("");
		data.append(
				"<html><head><title>hi</title><Body><table align='center' border='0' cellpadding='0' cellspacing='0' width=60% ' style='background-color: #2196f3; height: 52px;'>	<tr><td align='center'><center><table border='0' cellpadding='0' cellspacing='0' width='600px'style='height: 100%;'><tr><td align='center' valign='middle' style='padding-left: 20px;'><div>");
		data.append(
				"<a href='#' style='text-decoration: none;'><span id='logo' style='color: white; font-size: 28px; font-weight: bold'>Genie SmartHome</span></a></div></td></tr></table></center></td></tr></table><table border='0' cellpadding='0' cellspacing='0' width='100%'><tr><td colspan='2' height='20px'></td></tr><tr><td><center><table border='0' cellpadding='0' cellspacing='0' width='600px'style='height: 100%;'>");
		data.append(
				"<tr><td align='center' valign='bottom' style='padding-left: 20px; padding-bottom: 20px'><h3	style='color: #444444; display: block; font-size: 18px; font-weight: 400; margin-top: 2%; margin-right: 0; margin-bottom: 1%; margin-left: 0; font-weight: bold;'>Hello	USER_NAME!</h3>	<div");
		data.append(
				"style='line-height: 150%; font-size: 22px; color: #707f8b; margin-bottom: 20px;'>Your one time password to complete PROCESS is OTP_CODE.<br>Please do not share this OTP with anyone</div></td></tr></table></center></td></tr></table>");
		data.append(
				"<table border='0' cellpadding='0' cellspacing='0' width='100%'><tr><td><center><table border='0' cellpadding='0' cellspacing='0' width='400px' style='height: 100%;'><tr><td align='center' style='padding: 10px 20px; background: #fafbff; border: 1px solid #f4f4f4; border-radius: 5px;'><p");
		data.append(
				"style='color: #444444; font-size: 18px; font-weight: 300;'>Thank you.<br /><br /><br><span style='color: #F58020; font-size: 35px'> <strong>genie<span style='vertical-align: text-bottom; font-size: 20px'>iot</span></strong></span><br>Mobile: +91-89830 12131<br><a href='www.gsmarthome.com'> www.gsmarthome.com</a><br>Genieiot office no 4, Ujwal Regalia,<br>Near Prabhavi Tech Park, Baner, Pune - 411045");
		data.append(
				"</p></td></tr><tr><td align='center' valign='top' style='padding: 20px;'></td></tr></table></center></Body></head></html>");

		return data.toString();
	}
    
    public static String readDeleteUserHTML() {

  		StringBuffer data = new StringBuffer("");
  		data.append(
  				"<html><head><title>hi</title><Body><table align='center' border='0' cellpadding='0' cellspacing='0' width=60% ' style='background-color: #2196f3; height: 52px;'>	<tr><td align='center'><center><table border='0' cellpadding='0' cellspacing='0' width='600px'style='height: 100%;'><tr><td align='center' valign='middle' style='padding-left: 20px;'><div>");
  		data.append(
  				"<a href='#' style='text-decoration: none;'><span id='logo' style='color: white; font-size: 28px; font-weight: bold'>Genie SmartHome</span></a></div></td></tr></table></center></td></tr></table><table border='0' cellpadding='0' cellspacing='0' width='100%'><tr><td colspan='2' height='20px'></td></tr><tr><td><center><table border='0' cellpadding='0' cellspacing='0' width='600px'style='height: 100%;'>");
  		data.append(
  				"<tr><td align='center' valign='bottom' style='padding-left: 20px; padding-bottom: 20px'><h3	style='color: #444444; display: block; font-size: 18px; font-weight: 400; margin-top: 2%; margin-right: 0; margin-bottom: 1%; margin-left: 0; font-weight: bold;'>Hello	USER_NAME!</h3>	<div");
  		data.append(
  				"style='line-height: 150%; font-size: 22px; color: #707f8b; margin-bottom: 20px;'>Your account is Deactivated from Genie Smart Home App.<br>Thank you for using Genie Smart Home.</div></td></tr></table></center></td></tr></table>");
  		data.append(
  				"<table border='0' cellpadding='0' cellspacing='0' width='100%'><tr><td><center><table border='0' cellpadding='0' cellspacing='0' width='400px' style='height: 100%;'><tr><td align='center' style='padding: 10px 20px; background: #fafbff; border: 1px solid #f4f4f4; border-radius: 5px;'><p");
  		data.append(
  				"style='color: #444444; font-size: 18px; font-weight: 300;'>Thank you.<br /><br /><br><span style='color: #F58020; font-size: 35px'> <strong>genie<span style='vertical-align: text-bottom; font-size: 20px'>iot</span></strong></span><br>Mobile: +91-89830 12131<br><a href='www.gsmarthome.com'> www.gsmarthome.com</a><br>Genieiot office no 4, Ujwal Regalia,<br>Near Prabhavi Tech Park, Baner, Pune - 411045");
  		data.append(
  				"</p></td></tr><tr><td align='center' valign='top' style='padding: 20px;'></td></tr></table></center></Body></head></html>");

  		return data.toString();
  	}
    
	public static String readPaswordHTML() {

		StringBuffer data = new StringBuffer("");
		data.append(
				"<html><head><title>hi</title><Body><table align='center' border='0' cellpadding='0' cellspacing='0' width=60% ' style='background-color: #2196f3; height: 52px;'>	<tr><td align='center'><center><table border='0' cellpadding='0' cellspacing='0' width='600px'style='height: 100%;'><tr><td align='center' valign='middle' style='padding-left: 20px;'><div>");
		data.append(
				"<a href='#' style='text-decoration: none;'><span id='logo' style='color: white; font-size: 28px; font-weight: bold'>Genie SmartHome</span></a></div></td></tr></table></center></td></tr></table><table border='0' cellpadding='0' cellspacing='0' width='100%'><tr><td colspan='2' height='20px'></td></tr><tr><td><center><table border='0' cellpadding='0' cellspacing='0' width='600px'style='height: 100%;'>");
		data.append(
				"<tr><td align='center' valign='bottom' style='padding-left: 20px; padding-bottom: 20px'><h3	style='color: #444444; display: block; font-size: 18px; font-weight: 400; margin-top: 2%; margin-right: 0; margin-bottom: 1%; margin-left: 0; font-weight: bold;'>Hello	USER_NAME!</h3>	<div");
		data.append(
				"style='line-height: 150%; font-size: 22px; color: #707f8b; margin-bottom: 20px;'>Your OTP to complete PROCESS is OTP_CODE.<br>Please do not share this OTP with anyone</div></td></tr></table></center></td></tr></table>");
		data.append(
				"<table border='0' cellpadding='0' cellspacing='0' width='100%'><tr><td><center><table border='0' cellpadding='0' cellspacing='0' width='400px' style='height: 100%;'><tr><td align='center' style='padding: 10px 20px; background: #fafbff; border: 1px solid #f4f4f4; border-radius: 5px;'><p");
		data.append(
				"style='color: #444444; font-size: 18px; font-weight: 300;'>Thank you.<br /><br /><br><span style='color: #F58020; font-size: 35px'> <strong>genie<span style='vertical-align: text-bottom; font-size: 20px'>iot</span></strong></span><br>Mobile: +91-89830 12131<br><a href='www.gsmarthome.com'> www.gsmarthome.com</a><br>Genieiot office no 4, Ujwal Regalia,<br>Near Prabhavi Tech Park, Baner, Pune - 411045");
		data.append(
				"</p></td></tr><tr><td align='center' valign='top' style='padding: 20px;'></td></tr></table></center></Body></head></html>");

		return data.toString();
	}
    
/*    public static void main(String args[]){
    	
    	String TO = "kapeel45@gmail.com";
    	//String BODY = "This email was sent something else through the Amazon SES SMTP interface by using Java.";
	    String SUBJECT = "Amazon SES test (SMTP interface accessed using Java)";
	    
    	System.out.println("kapil");
    	
    	String body = Utility.readHTML();

		body = body.replaceAll("USER_NAME", TO);
		//body = body.replaceAll("OTP_CODE", shareControlDetails.getOtp());
		body = body.replaceAll("PROCESS", "Delete Share Control Process");
		
		
    	
    	AwsMail aws = new AwsMail();
    	try {
			aws.SendAWSMail(TO, SUBJECT, body);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }*/

}
