package com.gsmarthome.util;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

public class ValidationErrorHandler {

	public static String getErrorMessage(BindingResult result) {
		StringBuilder builder = new StringBuilder();

		Set<String> errorMsgList = new HashSet<String>();

		List<FieldError> list1 = result.getFieldErrors();

		List<ObjectError> objectErrorList = result.getAllErrors();

		for (FieldError fieldError : list1) {
			builder.append(fieldError.getField() + " " + fieldError.getDefaultMessage() + ",");
			errorMsgList.add(fieldError.getDefaultMessage());
		}

		for (ObjectError objectError : objectErrorList) {
			if (!errorMsgList.contains(objectError.getDefaultMessage()))
				builder.append(objectError.getDefaultMessage() + ",");
		}

		return builder.toString();

	}

}
