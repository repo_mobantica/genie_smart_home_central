package com.gsmarthome.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Kapil S Shinde
 */

@Entity
public class GenieCam {
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
	private String camName;
	private String localIP;
	private String internetIP;
	private String localPort;
	private String InternetPort;
	
/*	@ManyToOne
	@JoinColumn(name = "HOMEID")
	private Home home;*/
	
	public GenieCam()
	{
		super();
	}

	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}

	public String getCamName() {
		return camName;
	}

	public void setCamName(String camName) {
		this.camName = camName;
	}

	public String getLocalIP() {
		return localIP;
	}

	public void setLocalIP(String localIP) {
		this.localIP = localIP;
	}

	public String getInternetIP() {
		return internetIP;
	}

	public void setInternetIP(String internetIP) {
		this.internetIP = internetIP;
	}

	public String getLocalPort() {
		return localPort;
	}

	public void setLocalPort(String localPort) {
		this.localPort = localPort;
	}

	public String getInternetPort() {
		return InternetPort;
	}

	public void setInternetPort(String internetPort) {
		InternetPort = internetPort;
	}

	@Override
	public String toString() {
		return "GenieCam [id=" + id + ", camName=" + camName + ", localIP=" + localIP + ", internetIP=" + internetIP
				+ ", localPort=" + localPort + ", InternetPort=" + InternetPort + "]";
	}
}
