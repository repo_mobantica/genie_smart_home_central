package com.gsmarthome.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class DeviceDetails {
	
	@Id
	private String deviceId;
	
	private Long adminUser;	

	public DeviceDetails() {
	}

	public DeviceDetails(String deviceId, Long adminUser) {
		this.deviceId = deviceId;
		this.adminUser = adminUser;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public Long getAdminUser() {
		return adminUser;
	}

	public void setAdminUser(Long adminUser) {
		this.adminUser = adminUser;
	}

	@Override
	public String toString() {
		return "DeviceDetails [deviceId=" + deviceId + ", adminUser="
				+ adminUser + "]";
	}
}
