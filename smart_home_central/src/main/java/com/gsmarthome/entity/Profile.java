package com.gsmarthome.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Profile {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@ManyToOne
	@JsonIgnore
	@JoinColumn(name = "userId", nullable = false)
	private UserDetails user;
	
	private String profileName;
	private String switchList;
	private String dimmerValues;
	private String switchOnOffStatus;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UserDetails getUser() {
		return user;
	}

	public void setUser(UserDetails user) {
		this.user = user;
	}

	public String getProfileName() {
		return profileName;
	}

	public void setProfileName(String profileName) {
		this.profileName = profileName;
	}

	public String getSwitchList() {
		return switchList;
	}

	public void setSwitchList(String switchList) {
		this.switchList = switchList;
	}

	public String getDimmerValues() {
		return dimmerValues;
	}

	public void setDimmerValues(String dimmerValues) {
		this.dimmerValues = dimmerValues;
	}

	public String getSwitchOnOffStatus() {
		return switchOnOffStatus;
	}

	public void setSwitchOnOffStatus(String switchOnOffStatus) {
		this.switchOnOffStatus = switchOnOffStatus;
	}

	@Override
	public String toString() {
		return "Profile [id=" + id + ", user=" + user + ", profileName=" + profileName + ", switchList=" + switchList
				+ ", dimmerValues=" + dimmerValues + ", switchOnOffStatus=" + switchOnOffStatus + "]";
	}

	

}
