package com.gsmarthome.entity;

import javax.persistence.Entity;

import javax.persistence.Id;


@Entity
public class Panel {
	@Id
	private Integer panelId;
	private String panelIp;

	private String panelCommand;
	private String notImplimented;
	private String panelName;

	public String getPanelIp() {
		return panelIp;
	}

	public void setPanelIp(String panelIp) {
		this.panelIp = panelIp;
	}

	public Integer getPanelId() {
		return panelId;
	}

	public void setPanelId(Integer panelId) {
		this.panelId = panelId;
	}

	public String getPanelCommand() {
		return panelCommand;
	}

	public void setPanelCommand(String panelCommand) {
		this.panelCommand = panelCommand;
	}

	public String getNotImplimented() {
		return notImplimented;
	}

	public void setNotImplimented(String notImplimented) {
		this.notImplimented = notImplimented;
	}

	@Override
	public String toString() {
		return "Panel [panelIp=" + panelIp + ", panelId=" + panelId + ", panelCommand=" + panelCommand
				+ ", notImplimented=" + notImplimented + "]";
	}

	public Panel(String panelIp, Integer panelId, String panelCommand, String notImplimented) {
		super();
		this.panelIp = panelIp;
		this.panelId = panelId;
		this.panelCommand = panelCommand;
		this.notImplimented = notImplimented;
	}

	public String getPanelName() {
		return panelName;
	}

	public void setPanelName(String panelName) {
		this.panelName = panelName;
	}

	public Panel() {
	}
}
