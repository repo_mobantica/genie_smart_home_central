package com.gsmarthome.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Activity {

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
/*	@ManyToOne
	@JoinColumn(name="homeId", nullable=false)
	@JsonIgnore
	private Home home;*/
	
	private String image;
	private String roomname;
	private String message;
	private String messageType;
	private String created_date;
	
	private String messageFrom;
	
	private Long homeid;
	
	
	
	public Long getHomeid() {
		return homeid;
	}

	public void setHomeid(Long homeid) {
		this.homeid = homeid;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getRoomname() {
		return roomname;
	}

	public void setRoomname(String roomname) {
		this.roomname = roomname;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getCreated_date() {
		return created_date;
	}

	public void setCreated_date(String created_date) {
		this.created_date = created_date;
	}

	public String getMessageFrom() {
		return messageFrom;
	}

	public void setMessageFrom(String messageFrom) {
		this.messageFrom = messageFrom;
	}
	
	
	 
}
