package com.gsmarthome.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;


@Entity
public class VersionType {

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private double versionType;
	private String versionName;
	private Date createDate;
	
	public VersionType() {}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getVersionType() {
		return versionType;
	}

	public void setVersionType(double versionType) {
		this.versionType = versionType;
	}

	public String getVersionName() {
		return versionName;
	}

	public void setVersionName(String versionName) {
		this.versionName = versionName;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
}
