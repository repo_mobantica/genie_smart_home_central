package com.gsmarthome.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
@Entity
public class EnquiryHistories {

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	private String issue_Action;
	private String created_date;
	private int status;

	@ManyToOne
	@JsonIgnore
	@JoinColumn(name = "ENQUIRYID")
	private Enquiry enquiryDetails;
	
	@ManyToOne
	@JsonIgnore
	@JoinColumn(name = "LOGINID")
	private Login login;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIssue_Action() {
		return issue_Action;
	}

	public void setIssue_Action(String issue_Action) {
		this.issue_Action = issue_Action;
	}

	public String getCreated_date() {
		return created_date;
	}

	public void setCreated_date(String created_date) {
		this.created_date = created_date;
	}

	public Enquiry getEnquiryDetails() {
		return enquiryDetails;
	}

	public void setEnquiryDetails(Enquiry enquiryDetails) {
		this.enquiryDetails = enquiryDetails;
	}

	public Login getLogin() {
		return login;
	}

	public void setLogin(Login login) {
		this.login = login;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
}
