package com.gsmarthome.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class IOTProduct {

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
	
	private String productName;
	
	private Integer iotProductNumber;
	
	@OneToMany(mappedBy = "iotProduct", cascade = CascadeType.ALL)
	private List<Switch> switchList=new ArrayList<Switch>();
	
	@ManyToOne
	@JoinColumn(name="ROOMID", nullable=false)
	@JsonIgnore
	private Room room;

	
	
	public IOTProduct(String productName) {
		this.productName = productName;
	}

	public IOTProduct(String productName, List<Switch> switchList, Room room) {
		this.productName = productName;
		this.switchList = switchList;
		this.room = room;
	}

	public IOTProduct() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public List<Switch> getSwitchList() {
		return switchList;
	}

	public void setSwitchList(List<Switch> switchList) {
		this.switchList = switchList;
	}

	public Room getRoom() {
		return room;
	}
	public void setRoom(Room room) {
		this.room = room;
	}		

	public Integer getIotProductNumber() {
		return iotProductNumber;
	}

	public void setIotProductNumber(Integer iotProductNumber) {
		this.iotProductNumber = iotProductNumber;
	}

	@Override
	public String toString() {
		return "IOTProduct [id=" + id + ", productName=" + productName
				+ ", switchList=" + switchList + "]";
	}	
	
}
