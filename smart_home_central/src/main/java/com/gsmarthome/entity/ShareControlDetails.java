package com.gsmarthome.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ShareControlDetails {

	@Id
	private String email;

	private Long adminUser;

	private Integer userType;

	private String otp;
	
	private String phoneNumber;
	
	private String createdDate;
	
	public ShareControlDetails() {
		
	}

	public ShareControlDetails(String phoneNumber,String email, Long adminUser, Integer userType, String otp, String createdDate) {
		this.email = email;
		this.adminUser = adminUser;
		this.userType = userType;
		this.otp = otp;
		this.phoneNumber=phoneNumber;
		this.createdDate=createdDate;
	}
	
	public ShareControlDetails(String phoneNumber,String email, Long adminUser, Integer userType) {
		this.email = email;
		this.adminUser = adminUser;
		this.userType = userType;
		this.phoneNumber=phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getAdminUser() {
		return adminUser;
	}

	public void setAdminUser(Long adminUser) {
		this.adminUser = adminUser;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public Integer getUserType() {
		return userType;
	}

	public void setUserType(Integer userType) {
		this.userType = userType;
	}
	

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	@Override
	public String toString() {
		return "ShareControlDetails [email=" + email + ", adminUser=" + adminUser + ", userType=" + userType + ", otp="
				+ otp + ", phoneNumber=" + phoneNumber + ", createdDate=" + createdDate + "]";
	}

	
}
