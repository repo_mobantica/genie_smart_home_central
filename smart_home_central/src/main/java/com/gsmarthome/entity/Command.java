package com.gsmarthome.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Command {
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
	private String targetCommand;
	private String timeStamp;
	private String panelId;
	private String soleCommand;
	
	public String getTargetCommand() {
		return targetCommand;
	}
	public void setTargetCommand(String targetCommand) {
		this.targetCommand = targetCommand;
	}
	public String getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
	public String getPanelId() {
		return panelId;
	}
	public void setPanelId(String panelId) {
		this.panelId = panelId;
	}
	public String getSoleCommand() {
		return soleCommand;
	}
	public void setSoleCommand(String soleCommand) {
		this.soleCommand = soleCommand;
	}
	@Override
	public String toString() {
		return "Command [targetCommand=" + targetCommand + ", timeStamp=" + timeStamp + ", panelId=" + panelId
				+ ", soleCommand=" + soleCommand + "]";
	}
	public Command()
	{
		super();
	}
	public Command(String switchOnOffCommand, String timeStamp) {
		super();
		this.targetCommand = switchOnOffCommand;
		this.timeStamp = timeStamp;
		this.panelId = switchOnOffCommand.substring(1, 3);
		this.soleCommand = switchOnOffCommand.substring(3,switchOnOffCommand.length());
		
	}
	
	public Command(String switchOnOffCommand) {
		super();
		this.targetCommand = switchOnOffCommand;
		this.panelId = switchOnOffCommand.substring(1, 3);
		this.soleCommand = switchOnOffCommand.substring(3,switchOnOffCommand.length());
	}
}
