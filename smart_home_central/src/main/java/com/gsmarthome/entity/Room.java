package com.gsmarthome.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Room {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String roomName;

	private String roomType;

	private String roomIdentifier;

	private String hideStatus;
	
	private Integer roomImageId;

	@OneToMany(mappedBy = "room", cascade = CascadeType.ALL)
	private List<IOTProduct> productList = new ArrayList<IOTProduct>();

	public String getHideStatus() {
		return hideStatus;
	}

	public void setHideStatus(String hideStatus) {
		this.hideStatus = hideStatus;
	}

	public Integer getRoomImageId() {
		return roomImageId;
	}

	public void setRoomImageId(Integer roomImageId) {
		this.roomImageId = roomImageId;
	}

	@ManyToOne
	@JsonIgnore
	@JoinColumn(name = "HOMEID", nullable = false)
	private Home home;

	public String getRoomIdentifier() {
		return roomIdentifier;
	}

	public void setRoomIdentifier(String roomIdentifier) {
		this.roomIdentifier = roomIdentifier;
	}

	public Room() {
	}

	public Room(String roomName, String roomType) {
		this.roomName = roomName;
		this.roomType = roomType;
	}

	public Room(String roomName, List<IOTProduct> productList, Home home) {
		this.roomName = roomName;
		this.productList = productList;
		this.home = home;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public List<IOTProduct> getProductList() {
		return productList;
	}

	public void setProductList(List<IOTProduct> productList) {
		this.productList = productList;
	}

	public Home getHome() {
		return home;
	}

	public void setHome(Home home) {
		this.home = home;
	}

	public String getRoomType() {
		return roomType;
	}

	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}

	@Override
	public String toString() {
		return "Room [id=" + id + ", roomName=" + roomName + ", roomType=" + roomType + ", productList=" + productList
				+ "]";
	}

}
