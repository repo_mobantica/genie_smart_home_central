package com.gsmarthome.constants;

public class Constants {

/*	public static final String FAILURE_MSG = "Failure";
	public static final String EXCEPTION_MSG = "Exception";	
	public static final String SUCCESS_MSG = "Success";
	public static final String REFRESH_BACK = "refreshBack";*/
	
	public static final String GENIE_CAM_LIST = "camList";
	public static final String GENIE_CAM_ID = "genieCamId";
	public static final String GENIE_CAM_NAME = "camName";
	public static final String GENIE_LOCAL_IP = "localIP";
	public static final String GENIE_LOCAL_PORT = "localPort";
	public static final String GENIE_INTERNET_IP = "internetIP";
	public static final String GENIE_INTERNET_PORT = "internetPort";
	
	public static final String GENIE_CAM_LIST_STATUS = "GENIE_CAM_LIST";
	public static final String GENIE_CAM_ADD_STATUS = "GENIE_CAM_ADD";
	public static final String GENIE_CAM_EDIT_STATUS = "GENIE_CAM_EDIT";
	public static final String GENIE_CAM_DELETE_STATUS = "GENIE_CAM_DELETE";
	public static final String GENIE_COLOR ="genieColor";

	public static final String SUCCESS_MSG = "Success";
	public static final String CURTAIN_STATUS ="curtainStatus";
	public static final String IS_ARMED ="isArmed";

	public static final String SWITCH_NAME_UPDATE_MESSAGE_TYPE = "Switch_Name_update";
	public static final String SWITCH_LOCK_UNLOCK_MESSAGE_TYPE = "Switch_Lock_Unlock";

	public static final int HOME_ARMED = 1;
	public static final int HOME_NOT_ARMED = 0;
	
	public static final String LOCK = "lock";
	public static final String UNLOCK = "unlock";

	
	public static final String UNHIDE = "unhide";
	public static final String HIDE = "hide";

	public static final int HOME_BLOCKED = 1;
	public static final int HOME_NOT_BLOCKED = 0;
	
	//Delivery Message in MQTT Quality of Service(Qos)
	public static final int QoS_ATMOST_ONCE = 0;
	public static final int QoS_ATLEAST_ONCE = 1;
	public static final int QoS_EXACTLY_ONCE = 2;
	/*
	public static final String SWITCH_ON_OFF_MESSAGE_TYPE = "Switch_ON_OFF";
	public static final String SWITCH_NAME_UPDATE_MESSAGE_TYPE = "Switch_Name_update";
	public static final String SWITCH_LOCK_UNLOCK_MESSAGE_TYPE = "Switch_Lock_Unlock";
	
	
	public static final String SWITCH_STATUS_ON = "on";
	public static final String SWITCH_STATUS_OFF = "off";
	
	public static final String LOCK = "lock";
	public static final String UNLOCK = "unlock";
	
	public static final String UNHIDE = "unhide";
	public static final String HIDE = "hide";
	*/
	
	public static final String BIRTH_DATE = "birthDate";
	
	public static final String REPEAT_STATUS = "repeatStatus";
	public static final String REPEAT_WEEK = "repeatWeek";
	
	//Profile Parameter
	public static final String PROFILE_NAME = "profileName";
	public static final String DIMMER_VALUES = "dimmerValues";
	public static final String PROFILE_ID = "profileId";
	public static final String SWITCH_LIST_PROFILE = "switchList";
	
	//JSON Param
	public static final String FAILURE = "failure";
	public static final String LOCKBIT = "lockbit";
	
	public static final String TIME = "time";
	
	public static final String SWITCH_ID = "switchId";
	public static final String MODE_NAME = "modeName";
	public static final String PROFILE_SWITCH_ID= "profileSwitchId";
	public static final String PROFILE_ON_OFF_STATUS = "profileOnOffStatus";
	public static final String SWITCH_NUMBER = "switchnumber";
	public static final String SWITCH_NAME = "switchName";
	public static final String SWITCH_IMAGE_ID = "switchImageId";
	public static final String SWITCH_STATUS = "switchStatus";
	
	public static final String DIMMER_STATUS = "dimmerStatus";
	public static final String DIMMER_VALUE = "dimmerValue";
	public static final String ACTIVITY_ID = "activityid";
	public static final String LOCK_STATUS = "lockStatus";
	public static final String MESSAGE_FROM = "messageFrom";
	
	public static final String ROOM_ID = "roomId";
	public static final String ROOM_NAME = "roomName";
	public static final String ROOM_TYPE = "roomType";
	
	
	public static final String STATUS = "status";
	public static final String MESSAGE = "message";
	public static final String HIDE_STATUS = "hideStatus";
	public static final String SCHEDULE_ID = "scheduleId";
	public static final String SCHEDULAR_DATE_TIME = "scheduleDateTime";
	public static final String SCHEDULE_SWITCH_ID = "scheduleSwitchId";
	
	public static final String ROOM_LIST = "Roomlist";
	public static final String SWITCH_LIST = "Switchlist";
	public static final String ROOM_IMAGE_ID = "roomImageId";
	public static final String SWITCH_AND_ROOM_LIST = "switchAndRoomList";
	
	public static final String ID = "id";
	public static final String FIRST_NAME = "firstName";
	public static final String LAST_NAME= "lastName";
	public static final String EMAIL = "email";
	public static final String PASSWORD = "password";
	public static final String PHONE_NUMBER = "phoneNumber";
	
	public static final String USER_ID = "userId";
	public static final String USER_iD = "userid";
	public static final String USER_NAME = "username";
	public static final String USER_TYPE = "userType";
	public static final String USER_IMAGE = "userimage";
	
	public static final String DEVICE_ID = "deviceId";
	public static final String DEVICE_TYPE = "deviceType";
	public static final String IS_EMAIL_VERIFIED = "isEmailVerified";
	public static final String IS_FIRST_LOGIN = "isFirstLogin";
	public static final String IMAGE = "image";
	public static final String HOME_ID = "homeid";
	public static final String ACTIVITY_HOME_ID = "homeId";
	
	
	public static final String NEW_USER_TYPE = "newUserType";	
	public static final String ADMIN_USER_ID = "adminUserId";
	
	public static final String ON_OFF_STATUS = "onoffstatus";
	
	//Months name
	public static final String JAN = "Jan";
	public static final String FEB = "Feb";
	public static final String MAR = "Mar";
	public static final String APR = "Apr";
	public static final String MAY = "May";
	public static final String JUN = "June";
	public static final String JUL = "July";
	public static final String AUG = "Aug";
	public static final String SEP = "Sept";
	public static final String OCT = "Oct";
	public static final String NOV = "Nov";
	public static final String DEC = "Dec";
	//UserDetailsServiceIMPL Constants
	/*public static final String USER_TYPE_ADMIN = "Admin";
	public static final String USER_TYPE_NORMAL = "Normal";
	public static final String USER_TYPE_MODERATE = "Moderate";
	*/
	
	/*//FCMNotifier
	public static final String FCM_NOTIFIER_TITLE = "title";
	public static final String FCM_NOTIFIER_BODY = "body";
	public static final String FCM_NOTIFIER_TO = "to";
	*/
	
}
