package com.gsmarthome;

import java.security.SecureRandom;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages={"com.gsmarthome"})
public class Application extends SpringBootServletInitializer {
		
	public static void main(String[] args) {
		
		SpringApplication.run(Application.class);
		
		Thread t = new Thread(new Runnable() {
		    public void run() {
		        /*
		         * Do something
		         */
		    }
		});

		t.start();			
	}
	@Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Application.class);
    }	
	@Bean
    public SecureRandom secureRandom()
    {
    	return new SecureRandom();
    }	
	@Bean()
	public  ThreadPoolTaskScheduler  taskScheduler(){
	    ThreadPoolTaskScheduler taskScheduler = new ThreadPoolTaskScheduler();
	    taskScheduler.setPoolSize(5);
	    return  taskScheduler;
	}
}
