package com.gsmarthome.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gsmarthome.dto.EmailDTO;
import com.gsmarthome.dto.GuestUserDTO;
import com.gsmarthome.dto.GuestUserResponseDTO;
import com.gsmarthome.dto.HomeIdDto;
import com.gsmarthome.dto.ResponseDTO;
import com.gsmarthome.dto.UpdatePasswordDTO;
import com.gsmarthome.dto.UpdatePasswordOTPDTO;
import com.gsmarthome.dto.UpdateProfileDTO;
import com.gsmarthome.dto.UpdateUserTypeDTO;
import com.gsmarthome.dto.UserIdDTO;
import com.gsmarthome.dto.UserListDTO;
import com.gsmarthome.dto.UserShareControlDTO;
import com.gsmarthome.dto.UserdetailsResponseDTO;
import com.gsmarthome.dto.ValidateUserDTO;
import com.gsmarthome.dto.ValidateUserResponse;
import com.gsmarthome.dto.VerifyEmailOTP;
import com.gsmarthome.dto.VerifyOTPDTO;
import com.gsmarthome.dto.VerifyOTPResponse;
import com.gsmarthome.entity.UserDetails;
import com.gsmarthome.repository.UserDetailsRepository;
import com.gsmarthome.service.UserDetailsService;

import MqttReadDataPaho.MqttIntConnectionToHub;


@RestController
@RequestMapping(value="/user")
public class UserDetailsController {
	
    @Autowired
    UserDetailsService userDetailsService;

	@Autowired
	UserDetailsRepository userDetailsRepository;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<ResponseDTO<UserDetails>> save(@Valid @RequestBody UserDetails customer,BindingResult bindingResult) {
    	ResponseDTO<UserDetails> savedCustomer = userDetailsService.save(customer,false,bindingResult);    	
        return new ResponseEntity<ResponseDTO<UserDetails>>(savedCustomer,HttpStatus.OK);    
    }
    
    @RequestMapping(value="/updatepassword",method = RequestMethod.POST)
    public ResponseEntity<ResponseDTO<Boolean>> updatePassword(@Valid @RequestBody UpdatePasswordDTO updatePasswordDTO,BindingResult bindingResult) {
    	ResponseDTO<Boolean> savedCustomer = userDetailsService.updatePassword(updatePasswordDTO);
    	
        return new ResponseEntity<ResponseDTO<Boolean>>(savedCustomer,HttpStatus.OK);
    }
        
    @RequestMapping(value="/updatepasswordOTP",method = RequestMethod.POST)
    public ResponseEntity<ResponseDTO<Boolean>> updatePasswordOTP(@Valid @RequestBody UpdatePasswordOTPDTO updatePasswordDTO,BindingResult bindingResult) {	
    	   	   	
    	ResponseDTO<Boolean> savedCustomer = userDetailsService.updatePasswordOTP(updatePasswordDTO);
    	
        return new ResponseEntity<ResponseDTO<Boolean>>(savedCustomer,HttpStatus.OK);
    }
    
    @RequestMapping(value="/sendOTP",method = RequestMethod.POST)
    public ResponseEntity<ResponseDTO<UserIdDTO>> sendOTP(@Valid @RequestBody EmailDTO emailDTO,BindingResult bindingResult) {
    	
    	ResponseDTO<UserIdDTO> savedCustomer = userDetailsService.sendOTP(emailDTO);
    	
        return new ResponseEntity<ResponseDTO<UserIdDTO>>(savedCustomer,HttpStatus.OK);
    }
    
    @RequestMapping(value="/sendPasswordOTP",method = RequestMethod.POST)
    public ResponseEntity<ResponseDTO<UserIdDTO>> sendPasswordOTP(@Valid @RequestBody EmailDTO emailDTO,BindingResult bindingResult) {
    	   	   	
    	ResponseDTO<UserIdDTO> savedCustomer = userDetailsService.sendPasswordOTP(emailDTO);
    	
        return new ResponseEntity<ResponseDTO<UserIdDTO>>(savedCustomer,HttpStatus.OK);
    }
    
    @RequestMapping(value="/verifyOTP",method = RequestMethod.POST)
    public ResponseEntity<ResponseDTO<VerifyOTPResponse>> verifyOTP(@Valid @RequestBody VerifyOTPDTO emailDTO,BindingResult bindingResult) {
    	   	   	
    	ResponseDTO<VerifyOTPResponse> savedCustomer = userDetailsService.verifyOTP(emailDTO);
    	
        return new ResponseEntity<ResponseDTO<VerifyOTPResponse>>(savedCustomer,HttpStatus.OK);
    }
    
    @RequestMapping(value="/verifyEmailOTP",method = RequestMethod.POST)
    public ResponseEntity<ResponseDTO<Boolean>> verifyEmailOTP(@Valid @RequestBody VerifyEmailOTP emailDTO,BindingResult bindingResult) {
    	   	   	
    	ResponseDTO<Boolean> savedCustomer = userDetailsService.verifyEmailOTP(emailDTO);
    	
        return new ResponseEntity<ResponseDTO<Boolean>>(savedCustomer,HttpStatus.OK);
    }
    
    @RequestMapping(value="/validateUser",method = RequestMethod.POST)
    public ResponseEntity<ResponseDTO<ValidateUserResponse>> validateUser(@Valid @RequestBody ValidateUserDTO emailDTO,BindingResult bindingResult) {
    	   	   	
    	ResponseDTO<ValidateUserResponse> savedCustomer = userDetailsService.validateUser(emailDTO);
    	
        return new ResponseEntity<ResponseDTO<ValidateUserResponse>>(savedCustomer, HttpStatus.OK);
    }
    
    @RequestMapping(value="/shareControl",method = RequestMethod.POST)	
    public ResponseEntity<ResponseDTO<Boolean>> shareControl(@Valid @RequestBody UserShareControlDTO shareControlDTO,BindingResult bindingResult) {
    	
    	ResponseDTO<Boolean> savedCustomer = null;
		try {
			savedCustomer = userDetailsService.userShareControl(shareControlDTO, bindingResult);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	return new ResponseEntity<ResponseDTO<Boolean>>(savedCustomer, HttpStatus.OK);
    }
    
    @RequestMapping(value="/deletesharecontroluser",method = RequestMethod.POST)
    public ResponseEntity<ResponseDTO<Boolean>> deleteShareControlUser(@Valid @RequestBody UserShareControlDTO shareControlDTO,BindingResult bindingResult) {
    	
    	ResponseDTO<Boolean> savedCustomer = null;
    	
		try {
			savedCustomer = userDetailsService.deleteShareControlUser(shareControlDTO, bindingResult);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseDTO<Boolean>>(savedCustomer, HttpStatus.BAD_REQUEST);
		}
    	
    	return new ResponseEntity<ResponseDTO<Boolean>>(savedCustomer, HttpStatus.OK);
    }
    
    @RequestMapping(value="/list",method = RequestMethod.POST)
    public ResponseEntity<ResponseDTO<List<UserListDTO>>> userList(@Valid @RequestBody UserIdDTO userIdDTO,BindingResult bindingResult) {
    	   	   	
    	ResponseDTO<List<UserListDTO>> savedCustomer = userDetailsService.getByUser(userIdDTO);
    	
    	return new ResponseEntity<ResponseDTO<List<UserListDTO>>>(savedCustomer, HttpStatus.OK);
    }
    
    @RequestMapping(value="/updatetype",method = RequestMethod.POST)
    public ResponseEntity<ResponseDTO<Boolean>> updateType(@Valid @RequestBody UpdateUserTypeDTO userIdDTO,BindingResult bindingResult) {
    	   	   	
    	ResponseDTO<Boolean> savedCustomer = null;
		try {
			savedCustomer = userDetailsService.updateUserType(userIdDTO);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseDTO<Boolean>>(savedCustomer, HttpStatus.BAD_REQUEST);			
		}
    	
    	return new ResponseEntity<ResponseDTO<Boolean>>(savedCustomer,HttpStatus.OK);
    }
    
    @RequestMapping(value="/updateprofile",method = RequestMethod.POST)
    public ResponseEntity<ResponseDTO<UserListDTO>> updateProfile(@Valid @RequestBody UpdateProfileDTO updateProfileDTO,BindingResult bindingResult) {
    	ResponseDTO<UserListDTO> savedCustomer = userDetailsService.updateProfile(updateProfileDTO);
    	
        return new ResponseEntity<ResponseDTO<UserListDTO>>(savedCustomer,HttpStatus.OK);
    }
    
    @RequestMapping(value="/syncShareControlData",method=RequestMethod.POST)
    public ResponseEntity<ResponseDTO<UserDetails>> syncShareControldata(@Valid @RequestBody UserdetailsResponseDTO userDetailsResponseDTO,BindingResult bindingResult) {
   
		ResponseDTO<UserDetails> savedCustomer = userDetailsService.syncShareControlData(userDetailsResponseDTO,false,bindingResult);
		
        return new ResponseEntity<ResponseDTO<UserDetails>>(savedCustomer,HttpStatus.OK);
    } 
    

    @RequestMapping(value="/checkguestuser",method = RequestMethod.POST)
    public ResponseEntity<ResponseDTO<GuestUserResponseDTO>> checkGuestUser(@Valid @RequestBody GuestUserDTO guestUserDTO) {
    	   	   	
    	ResponseDTO<GuestUserResponseDTO> respGuestUser = null;
		try {
			respGuestUser = userDetailsService.checkGuestUser(guestUserDTO);
		} catch (Exception e) {
			e.printStackTrace();
	    	return new ResponseEntity<ResponseDTO<GuestUserResponseDTO>>(respGuestUser, HttpStatus.OK);
			
		}
 
    	return new ResponseEntity<ResponseDTO<GuestUserResponseDTO>>(respGuestUser, HttpStatus.OK);
    }
    
    @RequestMapping(value="/alluserList",method = RequestMethod.POST)
    public ResponseEntity<ResponseDTO<List<UserListDTO>>> alluserList(@Valid @RequestBody HomeIdDto homeIdDto,BindingResult bindingResult) {
    	   	   	
    	UserDetails[] userDetails = userDetailsRepository.findByHomeId(homeIdDto.getHomeid());
    	UserIdDTO userIdDTO=new UserIdDTO();
    	userIdDTO.setUserId(userDetails[0].getId());
    	userIdDTO.setMessageFrom("Local");
    	ResponseDTO<List<UserListDTO>> savedCustomer = userDetailsService.getByUser(userIdDTO);
    	try {
        	MqttIntConnectionToHub.getObj().getMqttConnection();
    		}catch (Exception e) {
    			// TODO: handle exception
    		}
        	
    	return new ResponseEntity<ResponseDTO<List<UserListDTO>>>(savedCustomer, HttpStatus.OK);
    }
    
}
