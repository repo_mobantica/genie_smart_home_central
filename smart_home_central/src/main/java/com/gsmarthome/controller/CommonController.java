package com.gsmarthome.controller;

import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;

import javax.imageio.ImageIO;

import org.apache.http.client.ClientProtocolException;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;

public class CommonController {

	/*
	 * Purpose : To send SMS using msg91.com
	 */

	public static boolean sendSMS(String mobile, String sms) {

		try {
			
			 URL url = new
			 URL("https://control.msg91.com/api/sendhttp.php?authkey=286324AgnuiC1J5d35d10c&mobiles="
			 + mobile + "&message=" + URLEncoder.encode(sms, "UTF-8") +
			 "&sender=GSMART&route=4&country=91");
			
			/*
			URL url = new URL("https://control.msg91.com/api/sendhttp.php?authkey=286324AgnuiC1J5d35d10c*mobiles="
					+ mobile + "&message=" + URLEncoder.encode(sms, "UTF-8") + "&sender=GENIEI&route=4&country=91");
*/
			 
			/*
			URL url = new URL("https://control.msg91.com/api/sendhttp.php?authkey=286324AgnuiC1J5d35d10c&mobiles=" 
					+ mobile + "&message=" + URLEncoder.encode(sms, "UTF-8") + "&sender=GENIE&route=4&country=+91");

*/

			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			// optional default is GET
			con.setRequestMethod("GET");

			int responseCode = con.getResponseCode();
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			// print result
			if (responseCode == 200 && response.toString().contains("sent,success"))
			{
				return true;
			}
			else
			{
				return false;
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (ProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	/*
	 * Purpose : To upload user profile image on AWS s3 bucket
	 */
	public static String uploadImageToPath(byte[] image, long user_id) {

		try {

			if (image.length != 0) {
				BufferedImage src = ImageIO.read(new ByteArrayInputStream(image));
				String filename = user_id + ".png";
				/*
				 * Access Key ID: AKIAJ4KLS4STAJDKO7CA Secret Access Key:
				 * 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
				 */
				BasicAWSCredentials awsCreds = new BasicAWSCredentials("AKIAJ4KLS4STAJDKO7CA",
						"NhtxiVG9it4CAlsazRigvNWBisqHw7IWOD04MkN0");
				AmazonS3Client s3client = new AmazonS3Client(awsCreds);
				s3client.setRegion(Region.getRegion(Regions.US_WEST_2));

				try {
					InputStream fis = new ByteArrayInputStream(image);
					ObjectMetadata metadata = new ObjectMetadata();
					metadata.setContentLength(image.length);
					metadata.setContentType("image/png");
					metadata.setCacheControl("public, max-age=31536000");
					System.out.println("Uploading a new object to S3 from a file\n");
					s3client.putObject("elasticbeanstalk-us-west-2-945738961374", filename, fis, metadata);
					s3client.setObjectAcl("elasticbeanstalk-us-west-2-945738961374", filename,
							CannedAccessControlList.PublicRead);

				} catch (AmazonServiceException ase) {
					System.out.println("Caught an AmazonServiceException, which " + "means your request made it "
							+ "to Amazon S3, but was rejected with an error response" + " for some reason.");
					System.out.println("Error Message:    " + ase.getMessage());
					System.out.println("HTTP Status Code: " + ase.getStatusCode());
					System.out.println("AWS Error Code:   " + ase.getErrorCode());
					System.out.println("Error Type:       " + ase.getErrorType());
					System.out.println("Request ID:       " + ase.getRequestId());
				} catch (AmazonClientException ace) {
					System.out.println("Caught an AmazonClientException, which " + "means the client encountered "
							+ "an internal error while trying to " + "communicate with S3, "
							+ "such as not being able to access the network.");
					System.out.println("Error Message: " + ace.getMessage());
				}
				return "elasticbeanstalk-us-west-2-945738961374/" + filename;

			} else {
				return "";
			}

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/*
	 * Purpose : To upload user profile image on AWS s3 bucket
	 */
	public static String uploadImageToServer(byte[] image, long user_id, String path) {

		String filename = user_id + ".png";

		try {
			// byte[] imageByte=Base64.decodeBase64(imageValue);
			String directory = path + filename;
			//new FileOutputStream(new File(directory)).write(image);

			/*
			//byte[] bytes = profile_img.getBytes();
			BufferedOutputStream stream =new BufferedOutputStream(new FileOutputStream(new File(directory)));
			stream.write(image);
			stream.flush();
			stream.close();
			 */

			//  BufferedImage originalImage = ImageIO.read(new File(directory));
			BufferedImage src = ImageIO.read(new ByteArrayInputStream(image));
			File destinstion = new File(directory);
			ImageIO.write(src, "png", destinstion);


			File f = new File(directory);

			if (f.exists() && !f.isDirectory()) 
			{
				return filename;
			} 
			else 
			{
				return "";
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return "error = " + e;
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			return "error = " + e;
		} catch (IOException e) {
			e.printStackTrace();
			return "error = " + e;
		} catch (Exception e) {
			e.printStackTrace();
			return "error = " + e;
		}

	}
}
