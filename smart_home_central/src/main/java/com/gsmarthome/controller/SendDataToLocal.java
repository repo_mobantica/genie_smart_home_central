package com.gsmarthome.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gsmarthome.dto.GuestUserResponseDTO;
import com.gsmarthome.dto.ResponseDTO;
import com.gsmarthome.dto.UserIdDTO;
import com.gsmarthome.dto.UserListDTO;
import com.gsmarthome.entity.UserDetails;
import com.gsmarthome.service.UserDetailsService;

@RestController
@RequestMapping(value="/send")
public class SendDataToLocal {


    @Autowired
    UserDetailsService userDetailsService;
    
    
    @RequestMapping(value="/shareControlData",method = RequestMethod.POST)
    public UserDetails data() {
    	   	   
    	ResponseDTO<UserDetails> respGuestUser = null;
    	
    	Long id=(long) 133;
    	UserIdDTO userIdDTO=new UserIdDTO();
    	userIdDTO.setUserId(id);
    	ResponseDTO<List<UserListDTO>> userDetails = userDetailsService.getByUser(userIdDTO);
    	
    	UserDetails  UserDetails=userDetailsService.getUserDetails(id);
    	
    	return UserDetails;
    }
    
}
