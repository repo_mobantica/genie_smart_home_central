package com.gsmarthome.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gsmarthome.dto.AddUpdateRoomDTO;
import com.gsmarthome.dto.EnquiryDTO;
import com.gsmarthome.dto.getReponseDTO;
import com.gsmarthome.repository.EnquiryRepository;
import com.gsmarthome.service.EnquiryService;

@RestController
@RequestMapping(value="/enquiry")
public class EnquiryController {


	@Autowired
	EnquiryService enquiryService;

	@Autowired
	EnquiryRepository enquiryRepository;

	

	@RequestMapping(value="/add",method=RequestMethod.POST)
	public ResponseEntity<getReponseDTO> addEnquiry(@RequestBody EnquiryDTO dto)
	{
		getReponseDTO objgetReponseDTO = new getReponseDTO();
		try{
			 objgetReponseDTO=enquiryService.addEnquiry(dto);				
			 
			 return new ResponseEntity<getReponseDTO>(objgetReponseDTO,HttpStatus.OK);
			}catch (RuntimeException e) {
				return new ResponseEntity<getReponseDTO>(objgetReponseDTO,HttpStatus.OK);
			}
	}
	
	
}
