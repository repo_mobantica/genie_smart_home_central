package com.gsmarthome.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gsmarthome.dto.ResponseDTO;
import com.gsmarthome.entity.RoomType;
import com.gsmarthome.repository.RoomTypeRepository;

@RestController
@RequestMapping(value="/roomtype")
public class RoomTypeController {

	@Autowired
	RoomTypeRepository roomTypeRepository;
	
	@Value("${API_STATUS_SUCCESS}")
	private String API_STATUS_SUCCESS;
	
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public ResponseEntity<ResponseDTO<RoomType>> addRoom(@RequestBody RoomType dto)
	{
		RoomType isSave = roomTypeRepository.save(dto); 		
		return new ResponseEntity<ResponseDTO<RoomType>>(new ResponseDTO<RoomType>(API_STATUS_SUCCESS,isSave),HttpStatus.OK);
	}
	
	@RequestMapping(value="/getall",method=RequestMethod.GET)
	public ResponseEntity<ResponseDTO<List<RoomType>>> getAll()
	{
		List<RoomType> isSave = (List<RoomType>)roomTypeRepository.findAll();		
		return new ResponseEntity<ResponseDTO<List<RoomType>>>(new ResponseDTO<List<RoomType>>(API_STATUS_SUCCESS,isSave),HttpStatus.OK);
	}
}
