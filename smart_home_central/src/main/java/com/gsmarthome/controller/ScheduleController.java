package com.gsmarthome.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.apache.log4j.Logger;
import com.gsmarthome.dto.ResponseDTO;
import com.gsmarthome.dto.ScheduleProfileDTO;
import com.gsmarthome.dto.ScheduleProfileResponseDTO;
import com.gsmarthome.dto.ScheduleSwitchDTO;
import com.gsmarthome.dto.ScheduleSwitchResponseDTO;
import com.gsmarthome.service.ScheduleService;
import com.gsmarthome.serviceImpl.ScheduleServiceImpl;


@RestController
@RequestMapping(value = "/schedule")
public class ScheduleController{

	static Logger logger = Logger.getLogger(ScheduleController.class.getName());
	
	@Autowired
	ScheduleService scheduleService;
	
	
	@Value("${API_STATUS_SUCCESS}")
	private String API_STATUS_SUCCESS;

	@Value("${API_STATUS_FAILURE}")
	private String API_STATUS_FAILURE;


	@RequestMapping(value="/scheduleswitch",method=RequestMethod.POST)
	public ResponseEntity<ResponseDTO<ScheduleSwitchResponseDTO>> scheduleswitch(@RequestBody ScheduleSwitchDTO dto)
	{
		ScheduleSwitchResponseDTO schedulswitchdetails = null;
		try {
			schedulswitchdetails = scheduleService.scheduleSwitch(dto);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}				
		return new ResponseEntity<ResponseDTO<ScheduleSwitchResponseDTO>>(new ResponseDTO<ScheduleSwitchResponseDTO>(API_STATUS_SUCCESS,schedulswitchdetails),HttpStatus.OK);
	}
	
	@RequestMapping(value = "/editscheduleswitch", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO<ScheduleSwitchResponseDTO>> editscheduleswitch(
			@RequestBody ScheduleSwitchDTO scheduleSwitchDTO) {
		ScheduleSwitchResponseDTO schedulswitchdetails = null;
		try {
			schedulswitchdetails = scheduleService.editScheduleSwitch(scheduleSwitchDTO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new ResponseEntity<ResponseDTO<ScheduleSwitchResponseDTO>>(
				new ResponseDTO<ScheduleSwitchResponseDTO>(API_STATUS_SUCCESS, schedulswitchdetails), HttpStatus.OK);
	}

	@RequestMapping(value = "/deletescheduleswitch", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO<Boolean>> deletescheduleswitch(@RequestBody ScheduleSwitchDTO scheduleSwitchDTO) {
		Boolean result = null;
		try {
			result = scheduleService.deleteScheduleSwitch(scheduleSwitchDTO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (result)
			return new ResponseEntity<ResponseDTO<Boolean>>(new ResponseDTO<Boolean>(API_STATUS_SUCCESS, true),
					HttpStatus.OK);
		else
			return new ResponseEntity<ResponseDTO<Boolean>>(new ResponseDTO<Boolean>(API_STATUS_FAILURE, true),
					HttpStatus.OK);

	}
	
	@RequestMapping(value = "/getscheduleswitch", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO<Boolean>> getScheduleSwitch(@RequestBody ScheduleSwitchDTO scheduleSwitchDTO) {
		
		Boolean listScheduleSwitch = null;
		try {
			listScheduleSwitch = scheduleService.getScheduleSwitchList(scheduleSwitchDTO);
		} catch (Exception e) {
			logger.error("getScheduleSwitch(), Exception Message: "+e.getMessage()+"Cause: "+e.getCause());
			e.printStackTrace();
			
		}
		
		if(listScheduleSwitch){
			return new ResponseEntity<ResponseDTO<Boolean>>(
					new ResponseDTO<Boolean>(API_STATUS_SUCCESS, listScheduleSwitch, "getScheduleSwitch Call executed"), HttpStatus.OK);
		}
		else{
			return new ResponseEntity<ResponseDTO<Boolean>>(
					new ResponseDTO<Boolean>(API_STATUS_FAILURE, listScheduleSwitch,"getScheduleSwitch call not executed"), HttpStatus.OK);
		}
	}
	
	// Schedule Profile
	@RequestMapping(value = "/scheduleprofile", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO<ScheduleProfileResponseDTO>> scheduleProfile(@RequestBody ScheduleProfileDTO reqScheduleProfileDTO) {
		ScheduleProfileResponseDTO respSchedulProfileDetails = null;
		
		try{
			respSchedulProfileDetails = scheduleService.scheduleProfile(reqScheduleProfileDTO);	 
			logger.info("scheduleProfile(), Schedule Successfully applied for Switch ID: "+reqScheduleProfileDTO.getProfileId());  
			return new ResponseEntity<ResponseDTO<ScheduleProfileResponseDTO>>(
						new ResponseDTO<ScheduleProfileResponseDTO>(API_STATUS_SUCCESS, respSchedulProfileDetails, "Profile Schedule Successfully applied"), HttpStatus.OK);
			
			
		}catch(Exception e){
			logger.error("Exception in scheduleProfile() Exception Message: "+e.getMessage());
			e.printStackTrace();
			return new ResponseEntity<ResponseDTO<ScheduleProfileResponseDTO>>(
					new ResponseDTO<ScheduleProfileResponseDTO>(API_STATUS_FAILURE, respSchedulProfileDetails,"Exception Message: "+e.getMessage()), HttpStatus.BAD_REQUEST);
		}
		
	
	}
			
	@RequestMapping(value = "/editscheduleprofile", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO<ScheduleProfileResponseDTO>> editScheduleProfile(
			@RequestBody ScheduleProfileDTO scheduleProfileDTO) {
		ScheduleProfileResponseDTO schedulProfileDetails = null;
		
		try{
			schedulProfileDetails = scheduleService.editScheduleProfile(scheduleProfileDTO);		
			return new ResponseEntity<ResponseDTO<ScheduleProfileResponseDTO>>(
						new ResponseDTO<ScheduleProfileResponseDTO>(API_STATUS_SUCCESS, schedulProfileDetails,"Edit Scheduler Succesfully edited the Scheduler"), HttpStatus.OK);
		
		}catch(Exception e){
			logger.error("Exception in editScheduleswitch() Exception Message: "+e.getMessage());
			e.printStackTrace();
			return new ResponseEntity<ResponseDTO<ScheduleProfileResponseDTO>>(
					new ResponseDTO<ScheduleProfileResponseDTO>(API_STATUS_FAILURE, schedulProfileDetails,"Exception Message: "+e.getMessage()), HttpStatus.BAD_REQUEST);
		}
		
	}
	
	@RequestMapping(value = "/deletescheduleprofile", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO<ScheduleProfileResponseDTO>> deleteScheduleProfile(@RequestBody ScheduleProfileDTO scheduleProfileDTO) {
		
		ScheduleProfileResponseDTO schedulProfileDetails = null;
		
		try {
			schedulProfileDetails = scheduleService.deleteScheduleProfile(scheduleProfileDTO);
		} catch (Exception e) {
			logger.error("Exception Message: "+e.getMessage()+" Cause: "+e.getCause());
			e.printStackTrace();
			return new ResponseEntity<ResponseDTO<ScheduleProfileResponseDTO>>(
					new ResponseDTO<ScheduleProfileResponseDTO>(API_STATUS_FAILURE, schedulProfileDetails,"Exception Message: "+e.getMessage()+"Cause: "+e.getCause()), HttpStatus.BAD_REQUEST);
			
		}
		
		return new ResponseEntity<ResponseDTO<ScheduleProfileResponseDTO>>(
				new ResponseDTO<ScheduleProfileResponseDTO>(API_STATUS_SUCCESS, schedulProfileDetails), HttpStatus.OK);

	}
	
	
}
