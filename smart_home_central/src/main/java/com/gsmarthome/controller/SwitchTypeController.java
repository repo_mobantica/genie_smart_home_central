package com.gsmarthome.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gsmarthome.dto.ResponseDTO;
import com.gsmarthome.entity.SwitchType;
import com.gsmarthome.repository.SwitchTypeRepository;

@RestController
@RequestMapping(value="/switchtype")
public class SwitchTypeController {
	
	@Autowired
	SwitchTypeRepository switchTypeRepository;
	
	@Value("${API_STATUS_SUCCESS}")
	private String API_STATUS_SUCCESS;
	
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public ResponseEntity<ResponseDTO<SwitchType>> addRoom(@RequestBody SwitchType dto)
	{
		SwitchType isSave = switchTypeRepository.save(dto); 
		
		return new ResponseEntity<ResponseDTO<SwitchType>>(new ResponseDTO<SwitchType>(API_STATUS_SUCCESS,isSave),HttpStatus.OK);
	}
	
	@RequestMapping(value="/getall",method=RequestMethod.GET)
	public ResponseEntity<ResponseDTO<List<SwitchType>>> getAll()
	{
		List<SwitchType> isSave = (List<SwitchType>)switchTypeRepository.findAll(); 
		
		return new ResponseEntity<ResponseDTO<List<SwitchType>>>(new ResponseDTO<List<SwitchType>>(API_STATUS_SUCCESS,isSave),HttpStatus.OK);
	}
	
	

}
