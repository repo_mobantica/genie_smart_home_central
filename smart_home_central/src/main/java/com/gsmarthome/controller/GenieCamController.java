package com.gsmarthome.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gsmarthome.dto.GenieCamRequestDTO;
import com.gsmarthome.dto.GenieCamResponseDTO;
import com.gsmarthome.dto.ResponseDTO;
import com.gsmarthome.entity.GenieCam;
import com.gsmarthome.service.GenieCamService;



@RestController
@RequestMapping(value="/geniecam")
public class GenieCamController {
	
	static Logger logger = Logger.getLogger(GenieCamController.class.getName());
	
	@Autowired
	GenieCamService genieCamService;
	
	@Value("${API_STATUS_SUCCESS}")
	private String API_STATUS_SUCCESS;
	
	@Value("${API_STATUS_FAILURE}")
	private String API_STATUS_FAILURE;
	
	/*****************************************************************************
	 * Method name: getAllGenieCamDetails
	 * 
	 * Parameters: GenieCamRequestDTO JSON POJO
	 * 
	 * Description: This will return all the genie cameras integrated in home.
	 * 
	 * Response: List<GenieCam> 
	 * 
	 *****************************************************************************/
	@RequestMapping(value="/getallcam",method=RequestMethod.POST)
	public ResponseEntity<ResponseDTO<List<GenieCam>>> getAllGenieCamDetails(@RequestBody GenieCamRequestDTO genieCamRequestDTO)
	{
		logger.info("Get Genie Cam Details, genie Camera controller request to service. Genie Camera Id: "+genieCamRequestDTO.getGenieCamId());
		
		ResponseDTO<List<GenieCam>> listGenieCam = genieCamService.getAllGenieCamDetails(genieCamRequestDTO);	
		
		try{
			logger.info("Get Genie Cam Details, genie Camera request came from service. Genie Camera Id: "+genieCamRequestDTO.getGenieCamId());
			return new ResponseEntity<ResponseDTO<List<GenieCam>>>(listGenieCam,HttpStatus.OK);
			
		}catch (Exception e) {
			logger.info("Get Genie Cam Details, genie Camera Exception occur. Genie Camera Id: "+genieCamRequestDTO.getGenieCamId());
			return new ResponseEntity<ResponseDTO<List<GenieCam>>>(listGenieCam,HttpStatus.OK);

		}
	}
	
	/*****************************************************************************
	 * Method name: addGenieCamDetails
	 * 
	 * Parameters: GenieCamRequestDTO JSON POJO
	 * 
	 * Description: This will add the Genie details in DB integrated in home.
	 * 
	 * Response: Boolean
	 * 
	 *****************************************************************************/
	@RequestMapping(value="/addcam",method=RequestMethod.POST)
	public ResponseEntity<ResponseDTO<GenieCamResponseDTO>> addGenieCamDetails(@RequestBody GenieCamRequestDTO genieCamRequestDTO)
	{
		logger.info("Add Genie Cam Details, genie Camera controller request to service. Genie Camera Id: "+genieCamRequestDTO.getGenieCamId());
		
		ResponseDTO<GenieCamResponseDTO> resultGenieCam = genieCamService.addGenieCamDetails(genieCamRequestDTO);	
		
		try{
			logger.info("Add Genie Cam Details, genie Camera request came from service. Genie Camera Id: "+genieCamRequestDTO.getGenieCamId());
			return new ResponseEntity<ResponseDTO<GenieCamResponseDTO>>(resultGenieCam,HttpStatus.OK);
			
		}catch (Exception e) {
			logger.info("Add Genie Cam Details, genie Camera Exception occur. Genie Camera Id: "+genieCamRequestDTO.getGenieCamId());
			return new ResponseEntity<ResponseDTO<GenieCamResponseDTO>>(resultGenieCam,HttpStatus.OK);

		}
	}
	
	/*****************************************************************************
	 * Method name: deleteGenieCam
	 * 
	 * Parameters: GenieCamRequestDTO JSON POJO
	 * 
	 * Description: This will delete the Genie details from DB integrated in home.
	 * 
	 * Response: Boolean
	 * 
	 *****************************************************************************/
	@RequestMapping(value="/deletecam",method=RequestMethod.POST)
	public ResponseEntity<ResponseDTO<GenieCamResponseDTO>> deleteGenieCam(@RequestBody GenieCamRequestDTO genieCamRequestDTO)
	{
		logger.info("Delete Genie Cam Details, genie Camera controller request to service. Genie Camera Id: "+genieCamRequestDTO.getGenieCamId());
		
		ResponseDTO<GenieCamResponseDTO> resultGenieCam = genieCamService.deleteGenieCamDetails(genieCamRequestDTO);	
		
		try{
			logger.info("Delete Genie Cam Details, genie Camera request came from service. Genie Camera Id: "+genieCamRequestDTO.getGenieCamId());
			return new ResponseEntity<ResponseDTO<GenieCamResponseDTO>>(resultGenieCam,HttpStatus.OK);
			
		}catch (Exception e) {
			logger.info("Delete Genie Cam Details, genie Camera Exception occur. Genie Camera Id: "+genieCamRequestDTO.getGenieCamId());
			return new ResponseEntity<ResponseDTO<GenieCamResponseDTO>>(resultGenieCam,HttpStatus.OK);

		}
	}
	
	
}
