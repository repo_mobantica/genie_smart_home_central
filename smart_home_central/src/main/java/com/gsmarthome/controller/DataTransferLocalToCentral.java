package com.gsmarthome.controller;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.amazonaws.util.json.JSONArray;
import com.amazonaws.util.json.JSONObject;
import com.gsmarthome.dto.AddUpdateSwitchDTO;
import com.gsmarthome.dto.HomeIdDto;
import com.gsmarthome.dto.HomeidVerificationDTO;
import com.gsmarthome.dto.IOTProductNewDTO;
import com.gsmarthome.dto.ResponseDTO;
import com.gsmarthome.dto.RoomListByUserDTO;
import com.gsmarthome.dto.SwitchDTO;
import com.gsmarthome.dto.UpdateUserDeviceDTO;
import com.gsmarthome.dto.UserIdDTO;
import com.gsmarthome.entity.Home;
import com.gsmarthome.entity.IOTProduct;
import com.gsmarthome.entity.Panel;
import com.gsmarthome.entity.Room;
import com.gsmarthome.entity.Switch;
import com.gsmarthome.repository.IOTProductRepository;
import com.gsmarthome.repository.PanelRepository;
import com.gsmarthome.repository.SwitchRepository;
import com.gsmarthome.repository.RoomRepository;
import com.gsmarthome.serviceImpl.SwitchServiceImpl;
import com.gsmarthome.util.Utility;
import com.mysql.jdbc.Connection;

import MqttReadDataPaho.JdbcConnection;

@RestController
@RequestMapping(value = "/datatransfer")
public class DataTransferLocalToCentral {

	static Logger logger = Logger.getLogger(SwitchServiceImpl.class.getName());

	private static final String DEVICE_ID_HEADER_NAME = "DEVICE-ID";
	private static final String DEVICE_TYPE_HEADER_NAME = "DEVICE-TYPE";

	Connection conn = null;
	/*String url = "jdbc:mysql://localhost:3306/";
	String dbName ="test";
	String driver = "com.mysql.jdbc.Driver";
	String userName = "root";
	String password = "root";
	public static String home_id="69";*/

	String url = JdbcConnection.url;
	String dbName =JdbcConnection.dbName;
	String driver = JdbcConnection.driver;
	String userName = JdbcConnection.userName;
	String password = JdbcConnection.password;


	@Value("${API_STATUS_SUCCESS}")
	private String API_STATUS_SUCCESS;
	@Autowired
	RoomRepository roomRepository;

	@Autowired
	PanelRepository panelRepository;
	@Autowired
	IOTProductRepository iOTProductRepository;
	@Autowired
	SwitchRepository switchRepository;


	//@RequestMapping(value = "/getSwitchDetails", method = RequestMethod.POST)
	public void getSwitchDetails(@RequestBody Switch dto)
	{
		System.out.println("getSwitchDetails"+dto.getId());
		int flag =0;

		try {
			Class.forName(driver).newInstance();
			conn = (Connection) DriverManager.getConnection(url + dbName, userName, password);

			java.sql.Statement statement1 = conn.createStatement();
			java.sql.Statement statement2 = conn.createStatement();
			java.sql.Statement statement3 = conn.createStatement();

			try {

				String sql2 = "SELECT id FROM switch where id="+dto.getId()+"";
				flag=0;
				ResultSet resultSet2 = statement2.executeQuery(sql2);
				while (resultSet2.next()) {
					flag=1;
				}
				if(flag==1)
				{
					String sql3 = " UPDATE switch " + " SET dimmer_value = "+dto.getDimmerValue()+", switch_status= "+dto.getSwitchStatus()+" , lock_status= "+dto.getLockStatus()+"   WHERE id = "+dto.getId()+"";
					statement3.executeUpdate(sql3);
				}
			}
			catch (Exception e) {
				// TODO: handle exception
			}


		}catch (Exception e) {
			System.out.println("error "+e);
			// TODO: handle exception
		}

	}



	@RequestMapping(value="/iotProductDetails",method = RequestMethod.POST)
	public List<IOTProductNewDTO> iotProductDetails(@RequestBody HomeidVerificationDTO homeidVerificationDTO) 
	{

		long homeId=Long.parseLong(homeidVerificationDTO.getHomeId());
		System.out.println("homeId = "+homeId);
		Home home=new Home();
		home.setId(homeId);
		List<IOTProductNewDTO> IOTProductNewDTOList=new ArrayList<IOTProductNewDTO>();

		try {

			List<Room> roomList=roomRepository.findByHome(home);

			IOTProductNewDTO iotproductnewDTO=new IOTProductNewDTO();
			for(int i=0;i<roomList.size();i++)
			{
				iotproductnewDTO=new IOTProductNewDTO();
				List<IOTProduct> iotproductList=iOTProductRepository.findByRoom(roomList.get(i));
				for(int j=0;j<iotproductList.size();j++)
				{
					iotproductnewDTO.setId(iotproductList.get(j).getId());
					iotproductnewDTO.setIotProductNumber(iotproductList.get(j).getIotProductNumber());
					iotproductnewDTO.setProductName(iotproductList.get(j).getProductName());
					iotproductnewDTO.setRoomId(iotproductList.get(j).getRoom().getId());
					iotproductnewDTO.setProduct_type("");
					IOTProductNewDTOList.add(iotproductnewDTO);
				}

			}

		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}


		return IOTProductNewDTOList;
	}


	@RequestMapping(value = "/getSwitchDetails", method = RequestMethod.POST)
	void data(@RequestBody SwitchDTO switchDTO)
	{
		for(int i=0;i<switchDTO.getSwitchList().size();i++)
		{
			try {
				
				Panel panel=panelRepository.findByPanelNameAndPanelCommand(switchDTO.getSwitchList().get(i).getPanelName(), switchDTO.getSwitchList().get(i).getPanelCommand());
				IOTProduct iotProduct = iOTProductRepository.findByiotProductNumber(panel.getPanelId());
				//IOTProduct iotProduct=new IOTProduct();
				//iotProduct.setId(switchDTO.getSwitchList().get(i).getIotProduct());
				Switch switchDetails=switchRepository.findBySwitchNumberAndIotProduct(switchDTO.getSwitchList().get(i).getSwitchNumber(),iotProduct);
				if(switchDetails!=null)
				{
					switchDetails.setDimmerValue(switchDTO.getSwitchList().get(i).getDimmerValue());
					switchDetails.setSwitchStatus(switchDTO.getSwitchList().get(i).getSwitchStatus());
					switchDetails.setLockStatus(switchDTO.getSwitchList().get(i).getLockStatus());
					switchRepository.save(switchDetails);
				}
			}catch (Exception e) {
				e.printStackTrace();
				// TODO: handle exception
			}

		}
	}
}
