package com.gsmarthome.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gsmarthome.constants.Constants;
import com.gsmarthome.dto.AddUpdateSwitchDTO;
import com.gsmarthome.dto.ChangeCurtainStatusDTO;
import com.gsmarthome.dto.ChangeHideStatusDTO;
import com.gsmarthome.dto.ChangeHideStatusResponseDTO;
import com.gsmarthome.dto.ChangeLockStatusDTO;
import com.gsmarthome.dto.ChangeLockStatusResponseDTO;
import com.gsmarthome.dto.ChangeStatusByHomeDTO;
import com.gsmarthome.dto.ChangeStatusByRoomDTO;
import com.gsmarthome.dto.ChangeStatusByRoomResponseDTO;
import com.gsmarthome.dto.ChangeSwitchStatusDTO;
import com.gsmarthome.dto.ChangeSwitchStatusResponseDTO;
import com.gsmarthome.dto.ResponseDTO;
import com.gsmarthome.dto.RoomIdDTO;
import com.gsmarthome.dto.ScheduleSwitchDTO;
import com.gsmarthome.dto.ScheduleSwitchResponseDTO;
import com.gsmarthome.dto.SwitchIdDTO;
import com.gsmarthome.dto.SwitchListByRoomDTO;
import com.gsmarthome.dto.UpdateUserDeviceDTO;
import com.gsmarthome.dto.UserIdDTO;
import com.gsmarthome.entity.Room;
import com.gsmarthome.entity.UserDetails;
import com.gsmarthome.service.RoomService;
import com.gsmarthome.service.SwitchService;
import com.gsmarthome.service.UserDetailsService;

import MqttReadDataPaho.MqttIntConnectionToHub;;
@RestController
@RequestMapping(value="/switch")
public class SwitchController {
	
	private static final String DEVICE_ID_HEADER_NAME = "DEVICE-ID";
	private static final String DEVICE_TYPE_HEADER_NAME = "DEVICE-TYPE";
	
	@Autowired
	RoomService roomService;
	
	@Autowired
	SwitchService switchService;
	
	@Autowired    
	UserDetailsService usersDetailsService;
	
	@Value("${API_STATUS_SUCCESS}")
	private String API_STATUS_SUCCESS;
	
	@Value("${API_STATUS_FAILURE}")
	private String API_STATUS_FAILURE;
	
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public ResponseEntity<ResponseDTO<Room>> addSwitch(@RequestBody AddUpdateSwitchDTO dto)
	{
		try{
		Room room = switchService.addEditSwitch(dto);
		
		return new ResponseEntity<ResponseDTO<Room>>(new ResponseDTO<Room>(API_STATUS_SUCCESS,room),HttpStatus.OK);
		}catch (RuntimeException e) {
			return new ResponseEntity<ResponseDTO<Room>>(new ResponseDTO<Room>(API_STATUS_FAILURE,null,e.getMessage()),HttpStatus.OK);
		}
	}
	
	@RequestMapping(value="/edit",method=RequestMethod.POST)
	public ResponseEntity<ResponseDTO<Room>> editSwitch(@RequestBody AddUpdateSwitchDTO dto)
	{
		
		try{
		Room room = switchService.addEditSwitch(dto);
		
		return new ResponseEntity<ResponseDTO<Room>>(new ResponseDTO<Room>(API_STATUS_SUCCESS,room),HttpStatus.OK);
		}catch (RuntimeException e) {
			return new ResponseEntity<ResponseDTO<Room>>(new ResponseDTO<Room>(API_STATUS_FAILURE,null,e.getMessage()),HttpStatus.OK);
		}
	}
	
	@RequestMapping(value="/delete",method=RequestMethod.POST)
	public ResponseEntity<ResponseDTO<Boolean>> delete(@RequestBody SwitchIdDTO dto)
	{
		Boolean status = switchService.deleteSwitch(dto.getSwitchId());
		
		return new ResponseEntity<ResponseDTO<Boolean>>(new ResponseDTO<Boolean>(API_STATUS_SUCCESS,status),HttpStatus.OK);
	}
	
	@RequestMapping(value="/changestatus",method=RequestMethod.POST)
	public ResponseEntity<ResponseDTO<ChangeSwitchStatusResponseDTO>> changeStatus(@RequestBody ChangeSwitchStatusDTO dto)
	{
		
		ChangeSwitchStatusResponseDTO changeSwitchStatusResponseDTO = switchService.changeStatus(dto);		
		
/*
		if (changeSwitchStatusResponseDTO.getStatus().equals("Already lock")) {
			return new ResponseEntity<ResponseDTO<ChangeSwitchStatusResponseDTO>>(
					new ResponseDTO<ChangeSwitchStatusResponseDTO>(API_STATUS_FAILURE, changeSwitchStatusResponseDTO, "Already Lock"),
					HttpStatus.OK);
		} else if (changeSwitchStatusResponseDTO.getStatus().equals("Hidden")) {
			return new ResponseEntity<ResponseDTO<ChangeSwitchStatusResponseDTO>>(
					new ResponseDTO<ChangeSwitchStatusResponseDTO>(API_STATUS_FAILURE, changeSwitchStatusResponseDTO, "Already Hidden"),
					HttpStatus.OK);
		} else {
			if (changeSwitchStatusResponseDTO.getStatus().equals(Constants.SUCCESS_MSG))
				return new ResponseEntity<ResponseDTO<ChangeSwitchStatusResponseDTO>>(
						new ResponseDTO<ChangeSwitchStatusResponseDTO>(API_STATUS_SUCCESS, changeSwitchStatusResponseDTO, "Success"),
						HttpStatus.OK);
			else
				return new ResponseEntity<ResponseDTO<ChangeSwitchStatusResponseDTO>>(
						new ResponseDTO<ChangeSwitchStatusResponseDTO>(API_STATUS_FAILURE, changeSwitchStatusResponseDTO, "Failure"),
						HttpStatus.OK);
		}*/
		return null;
	}
	
	@RequestMapping(value="/changelockstatus",method=RequestMethod.POST)
	public ResponseEntity<ResponseDTO<ChangeLockStatusResponseDTO>> changeLockstatus(@RequestBody ChangeLockStatusDTO dto)
	{
		ChangeLockStatusResponseDTO status = switchService.changeLockStatus(dto);
		return new ResponseEntity<ResponseDTO<ChangeLockStatusResponseDTO>>(new ResponseDTO<ChangeLockStatusResponseDTO>(API_STATUS_SUCCESS,status),HttpStatus.OK);
	}
	
	@RequestMapping(value="/changehidestatus",method=RequestMethod.POST)
	public ResponseEntity<ResponseDTO<ChangeHideStatusResponseDTO>> changeHidestatus(@RequestBody ChangeHideStatusDTO dto)
	{
		ChangeHideStatusResponseDTO status=switchService.changeHidestatus(dto);
		return new ResponseEntity<ResponseDTO<ChangeHideStatusResponseDTO>>(new ResponseDTO<ChangeHideStatusResponseDTO>(API_STATUS_SUCCESS,status),HttpStatus.OK);
	}
	
	@RequestMapping(value="/getswitchbyroom",method=RequestMethod.POST)
	public ResponseEntity<ResponseDTO<List<SwitchListByRoomDTO>>> getSwitchList(@RequestBody RoomIdDTO dto)
	{
		/*
		try {
		MqttIntConnectionToHub.getObj().getMqttConnection();
		}catch (Exception e) {
			// TODO: handle exception
		}
		*/
		List<SwitchListByRoomDTO> switchList = switchService.getSwitchListByRoom(dto.getRoomId());		
		return new ResponseEntity<ResponseDTO<List<SwitchListByRoomDTO>>>(new ResponseDTO<List<SwitchListByRoomDTO>>(API_STATUS_SUCCESS,switchList),HttpStatus.OK);
	}
	
	@RequestMapping(value="/changeStatusByRoom",method=RequestMethod.POST)
	public ResponseEntity<ResponseDTO<List<ChangeStatusByRoomResponseDTO>>> changeStatusByRoom(@RequestBody ChangeStatusByRoomDTO changeStatusByRoomDTO)
	{
		List<ChangeStatusByRoomResponseDTO> switchList = switchService.changeStatusByRoom(changeStatusByRoomDTO);		
		return new ResponseEntity<ResponseDTO<List<ChangeStatusByRoomResponseDTO>>>(new ResponseDTO<List<ChangeStatusByRoomResponseDTO>>(API_STATUS_SUCCESS, switchList), HttpStatus.OK);
	}
	
	@RequestMapping(value="/getswitchbyuser", method=RequestMethod.POST)
	public ResponseEntity<ResponseDTO<List<SwitchListByRoomDTO>>> getServiceAndRoomList(@RequestBody UserIdDTO dto,HttpServletRequest request)
	{
		//List<SwitchListByRoomDTO> switchList = null;
		List<SwitchListByRoomDTO> switchList = switchService.getSwitchListByUserId(dto.getUserId() , dto.getMessageFrom());
		
		/*final String device_id = request.getHeader(DEVICE_ID_HEADER_NAME);
		final String device_type = request.getHeader(DEVICE_TYPE_HEADER_NAME);
		UpdateUserDeviceDTO updateUserDeviceDTO = new UpdateUserDeviceDTO();
		
		updateUserDeviceDTO.setUserId(dto.getUserId());
		updateUserDeviceDTO.setDeviceId(device_id);
		updateUserDeviceDTO.setDeviceType(device_type);
		
		boolean flag = usersDetailsService.updateUserDevicetype(updateUserDeviceDTO);*/
				
		return new ResponseEntity<ResponseDTO<List<SwitchListByRoomDTO>>>(new ResponseDTO<List<SwitchListByRoomDTO>>(API_STATUS_SUCCESS,switchList),HttpStatus.OK); //new ResponseEntity<ResponseDTO<List<SwitchListByRoomDTO>>>(new ResponseDTO<List<SwitchListByRoomDTO>>(API_STATUS_SUCCESS , switchList , "Switch List By User Id", dto.getUserId()),HttpStatus.OK);
	}
	
	@RequestMapping(value = "/changecurtainstatus", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO<ChangeSwitchStatusResponseDTO>> changeCurtainStatus(
			@RequestBody ChangeCurtainStatusDTO changeCurtainStatusDTO) {
		ChangeSwitchStatusResponseDTO changeSwitchStatusRespDTO = switchService.changeCurtainStatus(changeCurtainStatusDTO);
		
		return null;
	}
	
	/***************************************************************************
	* Method Name: changeGenieColor
	* 
	* Parameters: ChangeSwitchStatusDTO POJO JSON request body
	* 
	* Description: This method will change the color of the bulb and make 
	* bulb ON or OFF as well.
	*
	* Response: ChangeSwitchStatusResponseDTO POJO Response body
	*
	***************************************************************************/
	// Change Switch Status
	@RequestMapping(value = "/changegeniecolor", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO<Boolean>> changeGenieColor(
			@RequestBody ChangeSwitchStatusDTO changeSwitchStatusDTO) {
		
		Boolean boolStatus = switchService.changeGenieColor(changeSwitchStatusDTO);
		
		// if(status.get)
		return new ResponseEntity<ResponseDTO<Boolean>>(
				new ResponseDTO<Boolean>(API_STATUS_SUCCESS, boolStatus), HttpStatus.OK);
	}
	

}
