package com.gsmarthome.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gsmarthome.dto.RemoteIRRequestDTO;
import com.gsmarthome.dto.RemoteIRResponseDTO;
import com.gsmarthome.dto.ResponseDTO;
import com.gsmarthome.entity.RemoteDetails;
import com.gsmarthome.service.IGenieRemoteService;
import com.gsmarthome.serviceImpl.GenieCamServiceImpl;

@RestController
@RequestMapping(value = "/remote")
public class GenieRemoteController {

	static Logger logger = Logger.getLogger(GenieCamServiceImpl.class.getName());

	@Autowired
	IGenieRemoteService genieRemoteService;
	
	@Value("${API_STATUS_SUCCESS}")
	private String API_STATUS_SUCCESS;
	
	@Value("${API_STATUS_FAILURE}")
	private String API_STATUS_FAILURE;

	@RequestMapping(value="/irstorage",method=RequestMethod.POST)
	public ResponseEntity<ResponseDTO<RemoteIRResponseDTO>> remoteIRStorage(@RequestBody RemoteIRRequestDTO remoteIRReqDTO)
	{
		try{
			
			ResponseDTO<RemoteIRResponseDTO> response = genieRemoteService.remoteIRStorage(remoteIRReqDTO);			
			 
			return new ResponseEntity<ResponseDTO<RemoteIRResponseDTO>>(response,HttpStatus.OK);
			 
			}catch (RuntimeException e) {
				return new ResponseEntity<ResponseDTO<RemoteIRResponseDTO>>(HttpStatus.BAD_REQUEST);
			}
	}
	
	@RequestMapping(value="/getallremoteinfo",method=RequestMethod.POST)
	public ResponseEntity<ResponseDTO<List<RemoteDetails>>> getAllRemoteInfo(@RequestBody RemoteIRRequestDTO remoteIRReqDTO)
	{
		
		try{
			
			ResponseDTO<List<RemoteDetails>> response = genieRemoteService.getAllIrRemote(remoteIRReqDTO);			
			 
			return new ResponseEntity<ResponseDTO<List<RemoteDetails>>>(response,HttpStatus.OK);
			 
			}catch (RuntimeException e) {
				return new ResponseEntity<ResponseDTO<List<RemoteDetails>>>(HttpStatus.BAD_REQUEST);
			}
	}

}


	
