package com.gsmarthome.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gsmarthome.dto.IOTProductDTO;
import com.gsmarthome.entity.IOTProduct;
import com.gsmarthome.entity.Room;
import com.gsmarthome.repository.IOTProductRepository;
import com.gsmarthome.repository.RoomRepository;

@RestController
@RequestMapping(value="/iotproduct")
public class IOTProductController {

	@Autowired
	IOTProductRepository iOTProductRepository;
	
	@Autowired
	RoomRepository roomRepository;
	
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public ResponseEntity<Room> addRoom(@RequestBody IOTProductDTO dto)
	{
		Room room=roomRepository.findOne(dto.getRoomId());
		
		IOTProduct iotProduct=new IOTProduct(dto.getProductName());
		iotProduct.setRoom(room);
		room.getProductList().add(iotProduct);
		
		iOTProductRepository.save(iotProduct);
		
		Room savedRoom=roomRepository.findOne(dto.getRoomId());
		
		return new ResponseEntity<Room>(savedRoom,HttpStatus.OK);
	}

	
}
