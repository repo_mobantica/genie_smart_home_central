package com.gsmarthome.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gsmarthome.dto.NotificationHistoryDTO;
import com.gsmarthome.dto.NotificationHistoryResponseDTO;
import com.gsmarthome.dto.ResponseDTO;
import com.gsmarthome.service.NotificationService;



@RestController
@RequestMapping(value="/notification")

public class NotificationController {
	
	@Autowired
	NotificationService notificationService;
	
	@Value("${API_STATUS_SUCCESS}")
	private String API_STATUS_SUCCESS;
	
	@Value("${API_STATUS_FAILURE}")
	private String API_STATUS_FAILURE;
	
	@RequestMapping(value="/getNotificationHistory",method=RequestMethod.POST)
	public ResponseEntity<ResponseDTO<List<NotificationHistoryResponseDTO>>> getNotificationHistory(@RequestBody NotificationHistoryDTO dto)
	{
	
		List<NotificationHistoryResponseDTO> historyList = notificationService.getNotificationHistory(dto);		
		
		try{
				return new ResponseEntity<ResponseDTO<List<NotificationHistoryResponseDTO>>>(new ResponseDTO<List<NotificationHistoryResponseDTO>>(API_STATUS_SUCCESS,historyList),HttpStatus.OK);
	
		}catch (RuntimeException e) {
			//return new ResponseEntity<ResponseDTO<Home>>(new ResponseDTO<Home>(API_STATUS_FAILURE,null,e.getMessage()),HttpStatus.OK);
			return new ResponseEntity<ResponseDTO<List<NotificationHistoryResponseDTO>>>(new ResponseDTO<List<NotificationHistoryResponseDTO>>(API_STATUS_FAILURE,historyList),HttpStatus.OK);

		}
	}
}
