package com.gsmarthome.controller;

import java.sql.DriverManager;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gsmarthome.dto.ActivityDTO;
import com.gsmarthome.dto.ActivityResponseDTO;
import com.gsmarthome.dto.EmailDTO;
import com.gsmarthome.dto.HomeArmANDBlockResponseDTO;
import com.gsmarthome.dto.HomeDTO;
import com.gsmarthome.dto.HomeDetailsResponseDTO;
import com.gsmarthome.dto.HomeVerificationResponseDTO;
import com.gsmarthome.dto.HomeidVerificationDTO;
import com.gsmarthome.dto.IotProductNumber;
import com.gsmarthome.dto.PanelDetailsDTO;
import com.gsmarthome.dto.ResponseDTO;
import com.gsmarthome.dto.RoomListByUserDTO;
import com.gsmarthome.dto.UserIdDTO;
import com.gsmarthome.dto.UserListDTO;
import com.gsmarthome.dto.UserdetailsResponseDTO;
import com.gsmarthome.dto.deviceDetailsDTO;
import com.gsmarthome.dto.panelListDTO;
import com.gsmarthome.dto.verifygeniehubDTO;
import com.gsmarthome.entity.Activity;
import com.gsmarthome.entity.DeviceDetails;
import com.gsmarthome.entity.Home;
import com.gsmarthome.entity.IOTProduct;
import com.gsmarthome.entity.Panel;
import com.gsmarthome.entity.Room;
import com.gsmarthome.entity.RoomType;
import com.gsmarthome.entity.Switch;
import com.gsmarthome.entity.SwitchType;
import com.gsmarthome.entity.UserDetails;
import com.gsmarthome.repository.ActivityRepository;
import com.gsmarthome.repository.CommandRepository;
import com.gsmarthome.repository.DeviceDetailsRepository;
import com.gsmarthome.repository.HomeRepository;
import com.gsmarthome.repository.IOTProductRepository;
import com.gsmarthome.repository.InstallationConfigurationRepository;
import com.gsmarthome.repository.NotificationRepository;
import com.gsmarthome.repository.PanelRepository;
import com.gsmarthome.repository.RoomRepository;
import com.gsmarthome.repository.RoomTypeRepository;
import com.gsmarthome.repository.SwitchRepository;
import com.gsmarthome.repository.SwitchTypeRepository;
import com.gsmarthome.repository.UserDetailsRepository;
import com.gsmarthome.service.HomeService;
import com.gsmarthome.service.NotificationService;
import com.gsmarthome.service.RoomService;
import com.gsmarthome.service.SwitchService;
import com.gsmarthome.service.UserDetailsService;
import com.gsmarthome.serviceImpl.MailSenderJava;
import com.gsmarthome.util.Utility;
import com.mysql.jdbc.Connection;

import MqttReadDataPaho.MqttIntConnectionToHub;

@RestController
@RequestMapping(value="/home")
public class HomeController {

	static Logger logger = Logger.getLogger(HomeController.class.getName());

	@Autowired
	RoomService roomService;

	@Autowired
	IOTProductRepository iOTProductRepository;

	@Autowired
	HomeRepository homeRepository;

	@Autowired
	HomeService homeService;

	@Autowired
	UserDetailsRepository userDetailsRepository;

	@Autowired
	MailSenderJava mailSender;

	@Autowired
	SwitchTypeRepository switchTypeRepository;

	@Autowired
	RoomTypeRepository roomTypeRepository;

	@Autowired
	DeviceDetailsRepository deviceDetailsRepository;

	@Value("${API_STATUS_SUCCESS}")
	private String API_STATUS_SUCCESS;

	@Value("${API_STATUS_FAILURE}")
	private String API_STATUS_FAILURE;

	@Autowired
	UserDetailsService userDetailsService;



	@Autowired
	CommandRepository commandRepository;
	@Autowired
	SwitchService switchService;

	@Autowired
	SwitchRepository switchRepository;

	@Autowired
	PanelRepository panelRepository;

	@Autowired
	RoomRepository roomRepository;

	@Autowired
	IOTProductRepository iotProductRepository;

	@Autowired
	NotificationService notificationService;

	@Autowired
	InstallationConfigurationRepository installationConfigurationRepository;

	@Autowired
	NotificationRepository notificationRepository;

	@Autowired
	ActivityRepository activityRepository;


	@RequestMapping(value = "/getactivitylist", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO<List<ActivityResponseDTO>>> getActivityList(@RequestBody ActivityDTO activityDTO) {

		// notificationService.getActivityList(activityDTO);


		final long homeid = activityDTO.getHomeId();
		Home home = homeRepository.findOne(homeid);

		UserDetails userDetails[] = userDetailsRepository.findByHomeId(homeid);

		Page<Integer> page = activityRepository.findLatestId(homeid, new PageRequest(0, 20));

		Activity activity[] = activityRepository.selectByExcludedId(page.getContent());

		List<ActivityResponseDTO> listActivityResp = new ArrayList<ActivityResponseDTO>();
		for (int i = 0; i < activity.length && activity[i].getHomeid()==homeid; i++) {

			if (i == 20)
				break;
			ActivityResponseDTO activityResponseDTO = new ActivityResponseDTO();
			try {
				Date date;
				DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:00");
				date = format.parse(activity[i].getCreated_date().toString());
				activityResponseDTO.setCreated_date((date.getDate() + " " + Utility.theMonth(date.getMonth()) + " "
						+ Utility.getTime(date.getHours() + ":" + date.getMinutes()).toString()));
			} catch (ParseException e) {
				e.printStackTrace();
			}

			/*
			 * if(activity[i].getImage().length() > 0){
			 * System.out.println("Image Length"+activity[i].getImage().
			 * length()+"Image Sub String"+activity[i].getImage().substring(
			 * 40,47));
			 * activityResponseDTO.setImage(activity[i].getImage().substring
			 * (40, 47)); }else{
			 */
			activityResponseDTO.setImage(activity[i].getImage());
			// }

			activityResponseDTO.setId(activity[i].getId());

			activityResponseDTO.setMessage(activity[i].getMessage());
			activityResponseDTO.setMessageType(activity[i].getMessageType());
			activityResponseDTO.setRoomname(activity[i].getRoomname());
			listActivityResp.add(activityResponseDTO);

		}

		return new ResponseEntity<ResponseDTO<List<ActivityResponseDTO>>>(
				new ResponseDTO<List<ActivityResponseDTO>>(API_STATUS_SUCCESS, listActivityResp), HttpStatus.OK);


		/*return new ResponseEntity<ResponseDTO<List<ActivityResponseDTO>>>(
				new ResponseDTO<List<ActivityResponseDTO>>(API_STATUS_SUCCESS, activityResponseDTO), HttpStatus.OK);*/
	}

	/* Purpose TO add New home*/

	@RequestMapping(value="/add",method=RequestMethod.POST)
	public ResponseEntity<Home> addRoom(@RequestBody HomeDTO dto)
	{
		UserDetails userDetails=userDetailsRepository.findOne(dto.getUserId());		
		Home home=new Home(dto.getHomeName());		
		home.getUserList().add(userDetails);		
		Home savedHome = homeRepository.save(home);		
		return new ResponseEntity<Home>(savedHome,HttpStatus.OK);
	}


	/* Purpose Get home details like switch type and Room type for that Home along with no of switches configures 
	 * as we have configured 6 switches only so returning Noofswitchs=6
	 * 
	 */
	@RequestMapping(value="/homedetails",method=RequestMethod.GET)
	public ResponseEntity<HomeDetailsResponseDTO> homedetails()
	{
		List<SwitchType> isSave = (List<SwitchType>)switchTypeRepository.findAll(); 
		List<RoomType> roomType = (List<RoomType>)roomTypeRepository.findAll();

		HomeDetailsResponseDTO homeDetailsResponseDTO= new HomeDetailsResponseDTO();

		homeDetailsResponseDTO.setRoomtypeList(roomType);
		homeDetailsResponseDTO.setSwitchtypeList(isSave);
		homeDetailsResponseDTO.setNoofswitchs("6");

		return new ResponseEntity<HomeDetailsResponseDTO>(homeDetailsResponseDTO,HttpStatus.OK);
	}

	/*Purpose :
	 * as per the deviceid parameter
	 * check the device exist or not and is device is allocated to respective user only.
	 * 
	 */

	@RequestMapping(value="/homeverification",method=RequestMethod.POST)
	public  ResponseEntity<HomeVerificationResponseDTO> HomeidVerification(@RequestBody HomeidVerificationDTO homeidVerificationDTO)
	{
		System.out.println("homeidVerificationDTO.getHomeId() = "+homeidVerificationDTO.getHomeId());
		MqttIntConnectionToHub.getObj().getMqttConnection();
		DeviceDetails deviceDetails = deviceDetailsRepository.findBydeviceId(homeidVerificationDTO.getHomeId());
		if(deviceDetails !=null)
		{

			System.out.println(deviceDetails.getAdminUser());

			UserDetails userDetailsById = userDetailsRepository.findOne(deviceDetails.getAdminUser());
			HomeVerificationResponseDTO homeVerificationResponseDTO=new HomeVerificationResponseDTO();
			EmailDTO emailDTO = new EmailDTO();
			emailDTO.setEmail(userDetailsById.getEmail().toString());
			String Otp = userDetailsService.sendHomeVerificationOTP(emailDTO);
			homeVerificationResponseDTO.setDeviceDetails(deviceDetails);
			homeVerificationResponseDTO.setOtpcode(Otp);
			homeVerificationResponseDTO.setStatus(API_STATUS_SUCCESS);
			homeVerificationResponseDTO.setUsername(userDetailsById.getFirstName() + "  "  + userDetailsById.getLastName());
			homeVerificationResponseDTO.setUserDetails(userDetailsById);
			homeVerificationResponseDTO.setHomeId(userDetailsById.getHome().getId().toString());

			return new ResponseEntity<HomeVerificationResponseDTO>(homeVerificationResponseDTO,HttpStatus.OK);
		}
		else
		{
			HomeVerificationResponseDTO homeVerificationResponseDTO = new HomeVerificationResponseDTO();
			homeVerificationResponseDTO.setDeviceDetails(null);
			homeVerificationResponseDTO.setOtpcode(null);
			homeVerificationResponseDTO.setStatus(API_STATUS_FAILURE);
			homeVerificationResponseDTO.setUsername(null);

			return new ResponseEntity<HomeVerificationResponseDTO>(homeVerificationResponseDTO,HttpStatus.OK);
		}
	}

	/*
	 * Purpose: for checking the Hub exist or not
	 * 
	 */

	@RequestMapping(value="/verifygeniehub",method=RequestMethod.GET)
	public  ResponseEntity<verifygeniehubDTO> verifyGenieHub()
	{	

		try {
			MqttIntConnectionToHub.getObj().getMqttConnection();
		}catch (Exception e) {
			// TODO: handle exception
		}
		try
		{	
			System.out.println("INNNN ");

			verifygeniehubDTO objverifygeniehubDTO=new verifygeniehubDTO();
			objverifygeniehubDTO.setResponse("GenieHub is now connected");
			objverifygeniehubDTO.setStatus(API_STATUS_SUCCESS);			
			return new ResponseEntity<verifygeniehubDTO>(objverifygeniehubDTO,HttpStatus.OK);
		}
		catch(Exception ex)
		{
			verifygeniehubDTO objverifygeniehubDTO=new verifygeniehubDTO();
			objverifygeniehubDTO.setResponse("GenieHub is not connected");		
			objverifygeniehubDTO.setStatus(API_STATUS_FAILURE);
			return new ResponseEntity<verifygeniehubDTO>(objverifygeniehubDTO,HttpStatus.OK);

		}
	}

	/*
	 * 
	 * Purpose :To get User information from device details
	 */

	@RequestMapping(value="/findBydeviceId",method=RequestMethod.POST)
	public  ResponseEntity<UserdetailsResponseDTO> findBydeviceId(@RequestBody deviceDetailsDTO d)
	{
		DeviceDetails devicedetails= deviceDetailsRepository.findBydeviceId(d.getDeviceId().toString());
		UserDetails userDetails=userDetailsRepository.findOne(devicedetails.getAdminUser());
		UserdetailsResponseDTO userdetailsResponseDTO=new UserdetailsResponseDTO();

		if(devicedetails!= null)
		{			
			userdetailsResponseDTO.setId(userDetails.getId());
			userdetailsResponseDTO.setFirstName(userDetails.getFirstName());
			userdetailsResponseDTO.setLastName(userDetails.getLastName());
			userdetailsResponseDTO.setEmail(userDetails.getEmail());
			userdetailsResponseDTO.setImage(userDetails.getImage());
			userdetailsResponseDTO.setPhoneNumber(userDetails.getPhoneNumber());
			userdetailsResponseDTO.setDeviceId(userDetails.getDeviceId());
			userdetailsResponseDTO.setDeviceType(userDetails.getDeviceType());
			userdetailsResponseDTO.setHomeid( userDetails.getHome().getId());
			userdetailsResponseDTO.setIsEmailVerified(userDetails.getIsEmailVerified());
			userdetailsResponseDTO.setIsFirstLogin(userDetails.getIsFirstLogin());
			userdetailsResponseDTO.setUserType(userDetails.getUserType());
			userdetailsResponseDTO.setPassword(userDetails.getPassword());		

		}		
		return new ResponseEntity<UserdetailsResponseDTO>(userdetailsResponseDTO,HttpStatus.OK);

	}	

	@RequestMapping(value = "/fillPanel", method = RequestMethod.POST)
	public ResponseEntity<verifygeniehubDTO> fillPanel(@RequestBody panelListDTO getpanel)  {

		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		UserDetails userDetails = new UserDetails();
		List<String> AllPanel = new ArrayList<String>();
		UserdetailsResponseDTO userdetailsResponseDTO = new UserdetailsResponseDTO();
		String ipConfigration = getpanel.getIpConfigration().toString();

		/*InstallationConfiguration installationConfiguration = new InstallationConfiguration(ipConfigration);
		installationConfigurationRepository.save(installationConfiguration);*/		
		try {

			String panelList = getpanel.getPanellist().toString();

			ArrayList pList = new ArrayList(Arrays.asList(panelList.split(",")));
			
			for (int cpanelid = 0; cpanelid < pList.size(); cpanelid++) {
				String res = pList.get(cpanelid).toString();

				String[] roomConfig = pList.get(cpanelid).toString().split("-");
				// Panel panel = new Panel(temp[i].toString(),
				// roomConfig[1].toString(), res.toString(), "walla");
				// GenieHubDb db = new GenieHubDb(); //
				// db.insertPanel(panel);

				Panel panel = new Panel();
				Panel newPanel = new Panel();
				// iotproduct--- kit-11 pro ductname
				// switch
				// panel table empty
				// if (count == 0) {
				long roomTypeid = 1;
				AllPanel.add(roomConfig[0].toString());

				int PanelId = Integer.parseInt(roomConfig[2].toString());

				panel = panelRepository.findBypanelId(PanelId);

				//panel = panelRepository.checkPanelalreadyExit(PanelId,res);

				//room = roomRepository.checkRoomalreadyExit(roomConfig[1],userDetails.getHome().getId());


				// if (panel.getPanelIp() != temp[i].toString()) {
				int socketfound = 0;


				if (panel == null) {

					newPanel.setNotImplimented("walla");
					newPanel.setPanelCommand(res);
					newPanel.setPanelId(Integer.parseInt(roomConfig[2].toString()));
					newPanel.setPanelName(roomConfig[0].toString());
					newPanel.setPanelIp("");
					panelRepository.save(newPanel);
					panel = newPanel;
				} else {

					// panel already exist
					// if (panel.getPanelIp() != temp[i].toString()) {
					panel.setNotImplimented("walla");
					panel.setPanelCommand(res);
					panel.setPanelId(Integer.parseInt(roomConfig[2].toString()));
					panel.setPanelName(roomConfig[0].toString());
					panel.setPanelIp("");
					panelRepository.save(panel);
					// }
				}

				// check Home already exist or not
				Home chkhome = new Home();
				Room room = new Room();

				DeviceDetails deviceDetails = new DeviceDetails();

				deviceDetails = deviceDetailsRepository.findBydeviceId(roomConfig[4].toString());

				// device information needs to add in devicedeatisl table
				if (deviceDetails == null) {
					deviceDetails = new DeviceDetails();
					deviceDetails.setDeviceId(roomConfig[4].toString());
					deviceDetails = deviceDetailsRepository.save(deviceDetails);

				}


				if (deviceDetails.getAdminUser() == null) {
					//RestTemplate restTemplate1 = new RestTemplate();

					deviceDetailsDTO d = new deviceDetailsDTO();
					d.setDeviceId(roomConfig[4].toString());


					//	DeviceDetails devicedetails = deviceDetailsRepository.findBydeviceId(getpanel.getDeviceId());
					UserDetails userDetails1 = userDetailsRepository.findOne(getpanel.getId());

					UserDetails newuser = new UserDetails();

					Home home = new Home();
					home.setId(getpanel.getHomeId());
					home.setHomeName(getpanel.getFirstName() + "'s Home");
					homeRepository.save(home);

					Home home1 = new Home();
					home1 = homeRepository.findOne(getpanel.getHomeId());


					try {
						newuser.setId(getpanel.getId());
						newuser.setFirstName(getpanel.getFirstName());
						newuser.setLastName(getpanel.getLastName());
						newuser.setEmail(getpanel.getEmail());
						newuser.setPhoneNumber(getpanel.getPhoneNumber());
						newuser.setDeviceId(getpanel.getDeviceId());
						newuser.setDeviceType(getpanel.getDeviceType());
						newuser.setHome(home1);
						newuser.setIsEmailVerified(getpanel.getIsEmailVerified());
						newuser.setIsFirstLogin(getpanel.getIsFirstLogin());
						newuser.setUserType(getpanel.getUserType());
						newuser.setPassword(getpanel.getPassword());

						userDetailsRepository.save(newuser);

						System.out.println("Save newuser");

					} catch (Exception ex) {
						System.out.println("Eoor  1 = "+ex);

					}
					DeviceDetails dnew = new DeviceDetails();
					dnew.setDeviceId(roomConfig[4].toString());
					dnew.setAdminUser(getpanel.getId());
					deviceDetailsRepository.save(dnew);

					long roomid = 1;
					RoomType r = new RoomType();
					r.setId(roomid);
					r.setRoomTypeName("BedRoom");
					roomTypeRepository.save(r);

					RoomType r1 = new RoomType();
					r1.setId(++roomid);
					r1.setRoomTypeName("Hall");
					roomTypeRepository.save(r1);

					RoomType r2 = new RoomType();
					r2.setId(++roomid);
					r2.setRoomTypeName("Kitchen");
					roomTypeRepository.save(r2);

					RoomType r3 = new RoomType();
					r3.setId(++roomid);
					r3.setRoomTypeName("Room_A");
					roomTypeRepository.save(r3);

					RoomType r4 = new RoomType();
					r4.setId(++roomid);
					r4.setRoomTypeName("Room_B");
					roomTypeRepository.save(r4);

					RoomType r5 = new RoomType();
					r5.setId(++roomid);
					r5.setRoomTypeName("Room_C");
					roomTypeRepository.save(r5);

					long switchtypeid = 1;

					SwitchType s1 = new SwitchType();
					s1.setId(switchtypeid);
					s1.setSwitchTypeName("simple");
					s1.setDimmerStatus("0");
					switchTypeRepository.save(s1);

					SwitchType s2 = new SwitchType();
					s2.setId(++switchtypeid);
					s2.setSwitchTypeName("power");
					s2.setDimmerStatus("0");
					switchTypeRepository.save(s2);

					SwitchType s3 = new SwitchType();
					s3.setId(++switchtypeid);
					s3.setSwitchTypeName("dimmer");
					s3.setDimmerStatus("0");
					switchTypeRepository.save(s3);

					SwitchType s4 = new SwitchType();
					s4.setId(++switchtypeid);
					s4.setSwitchTypeName("socket");
					s4.setDimmerStatus("0");
					switchTypeRepository.save(s4);

					SwitchType s5 = new SwitchType();
					s5.setId(++switchtypeid);
					s5.setSwitchTypeName("Dimmer1");
					s5.setDimmerStatus("0");
					switchTypeRepository.save(s5);

					SwitchType s6 = new SwitchType();
					s6.setId(++switchtypeid);
					s6.setSwitchTypeName("Dimmer2");
					s6.setDimmerStatus("0");
					switchTypeRepository.save(s6);

				}

				long userid = deviceDetails.getAdminUser();

				userDetails = userDetailsRepository.findOne(userid);

				chkhome = homeRepository.findOne(userDetails.getHome().getId());

				// room=roomRepository.findRoomByhomeid(Long.parseUnsignedLong(chkhome.getId().toString()));
				// room not yet added , so add new room

				// insert into Room table
				RoomType roomType = new RoomType();
				Room newRoom = new Room();

				// roomType=roomTypeRepository.findByRoomTypeNamelike(roomConfig[0]);
				roomType = roomTypeRepository.findOne(roomTypeid);

				//room = roomRepository.findRoomByroomIdentifier(roomConfig[1]);

				room = roomRepository.checkRoomalreadyExit(roomConfig[1],userDetails.getHome().getId());

				/*
				chkSwitch = switchRepository.checkSwitchalreadyExist(
						Integer.parseInt(a)+1,
						chkioproduct.getId());
				 */

				//System.out.println("room = "+room.getId());

				if (room == null) {			
					String roomname=roomConfig[1].toUpperCase();
					String mapRoomName="";
					switch(roomname)
					{
					case "TO" :
						mapRoomName="Toilet";
						break;
					case "BA" :
						mapRoomName="Balcony";
						break;
					case "SR" :
						mapRoomName="Store room";
						break;
					case "RO" :
						mapRoomName="Rooms";
						break;
					case "OS" :
						mapRoomName="Outside space";
						break;
					case "TE" :
						mapRoomName="Terrace";
						break;
					case "ST" :
						mapRoomName="Stage";
						break;
					case "LF" :
						mapRoomName="Lift";
						break;
					case "GR" :
						mapRoomName="Guest Room";
						break;
					case "PO" :
						mapRoomName="Porch";
						break;
					case "GA" :
						mapRoomName="Garden";
						break;
					case "PS" :
						mapRoomName="Parking Space";
						break;
					case "GE" :
						mapRoomName="Garrage";
						break;
					case "BD1" :
						mapRoomName="Bedroom1";
						break;
					case "BD2" :
						mapRoomName="Bedroom2";
						break;
					case "KT" :
						mapRoomName="Kitchen";
						break;
					case "HA1" :
						mapRoomName="Hall1";
						break;
					case "HA2" :
						mapRoomName="Hall2";
						break;
					default: mapRoomName="Other Room";						
					}								//str1.toLowerCase().contains(str2.toLowerCase())
					newRoom.setRoomName(mapRoomName);
					newRoom.setRoomType(roomType.getId().toString());
					newRoom.setRoomIdentifier(roomConfig[1]);
					newRoom.setHome(chkhome);
					room = roomRepository.save(newRoom);
				}

				// new room added and filled the room object with value
				// or taken room details from extsing table
				// but room data wth user
				if (room != null) {

					IOTProduct ioproduct = new IOTProduct();
					ioproduct.setProductName(roomConfig[3].toString());
					ioproduct.setIotProductNumber(Integer.parseInt(roomConfig[2].toString()));
					ioproduct.setRoom(room);
					IOTProduct chkioproduct = new IOTProduct();
					Switch chkSwitch = new Switch();


					chkioproduct = iotProductRepository
							.findByiotProductNumber(Integer.parseInt(roomConfig[2].toString()));


					if (chkioproduct == null) {
						chkioproduct = iotProductRepository.save(ioproduct);
					}
					// now add iotproduct details

					// chkioproduct=iotProductRepository.findByiotProductNumber(Integer.parseInt(roomConfig[1].toString()));
					if (chkioproduct != null) {


						String Switches = roomConfig[3].toString();

						for (int index = 0; index < Switches.length(); index++) {

							String a=Character.toString(Switches.charAt(index));

							if (!Character.toString(Switches.charAt(index)).equals("0")) {
								// System.out.println("1" +
								// Switches.charAt(index));

								if (index < 4) {


									try {
										chkSwitch = switchRepository.checkSwitchalreadyExist(
												Integer.parseInt(a)+1,
												chkioproduct.getId());

									} catch (Exception ex) {
										System.out.println(ex.getMessage().toString());
									}
									if (chkSwitch == null) {
										int Switchtypecount = index + 1;
										Switch s1 = new Switch();
										s1.setSwitchName("Bulb");
										s1.setDimmerStatus("0");
										s1.setDimmerValue("0");
										s1.setLockStatus("0");
										s1.setSwitchStatus("0");
										s1.setHideStatus("0");
										s1.setSwitchType(Switchtypecount + "");


										s1.setSwitchNumber(
												Integer.parseInt(Character.toString(Switches.charAt(index))));

										s1.setSwitchNumber(index+1);

										s1.setIotProduct(chkioproduct);

										// switchRepository
										switchRepository.save(s1);

										s1 = null;
									}
								} else {
									chkSwitch = switchRepository.checkSwitchalreadyExist(
											index+1,
											chkioproduct.getId());
									if (chkSwitch == null) {
										int Switchtypecount = index + 1;

										Switch s2 = new Switch();
										s2.setSwitchName("Fan");
										s2.setDimmerStatus("1");
										s2.setDimmerValue("20");
										s2.setLockStatus("0");

										s2.setSwitchNumber(index+1);

										s2.setIotProduct(chkioproduct);
										s2.setSwitchStatus("0");
										s2.setHideStatus("0");
										s2.setSwitchType(Switchtypecount + "");
										switchRepository.save(s2);
										s2 = null;
									}
								}
							}
						}
					}
				}
				roomTypeid++;

			}

		} catch (Exception ex) {

			System.out.println("error  = "+ex);
			verifygeniehubDTO objverifygeniehubDTO = new verifygeniehubDTO();
			objverifygeniehubDTO.setResponse("Error while configuring GenieHub");
			objverifygeniehubDTO.setStatus(API_STATUS_SUCCESS);
			return new ResponseEntity<verifygeniehubDTO>(objverifygeniehubDTO, HttpStatus.OK);

		}

		verifygeniehubDTO objVerifyGeniehubDTO = new verifygeniehubDTO();
		objVerifyGeniehubDTO.setResponse("GenieHub is now configured");
		objVerifyGeniehubDTO.setStatus(API_STATUS_SUCCESS);
		return new ResponseEntity<verifygeniehubDTO>(objVerifyGeniehubDTO, HttpStatus.OK);

	}


	@RequestMapping(value = "/armhome", method = RequestMethod.POST)
	public ResponseEntity<HomeArmANDBlockResponseDTO>  armHome(@RequestBody HomeDTO homeDTO) {

		try {
			HomeArmANDBlockResponseDTO homeArmResponse = homeService.armHome(homeDTO);

		} catch (Exception e) {

			e.printStackTrace();
		}

		return null;
	}

	@RequestMapping(value = "/blockhome", method = RequestMethod.POST)
	public ResponseEntity<HomeArmANDBlockResponseDTO>  blockHome(@RequestBody HomeDTO homeDTO) {

		HomeArmANDBlockResponseDTO homeArm  = null;

		try {
			homeArm = homeService.blockHome(homeDTO);
		} catch (Exception e) {
			logger.info("Exception while checking home is Blocked or not");
			e.printStackTrace();
		}

		if(homeArm != null){
			return new ResponseEntity<HomeArmANDBlockResponseDTO>(homeArm, HttpStatus.OK);
		}else{
			return new ResponseEntity<HomeArmANDBlockResponseDTO>(homeArm, HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value="/getLastIotProductNumber",method = RequestMethod.POST)
	public IotProductNumber getLastIotProductNumber(@RequestBody HomeidVerificationDTO homeidVerificationDTO) {

		long homeId=Long.parseLong(homeidVerificationDTO.getHomeId());

		Home home=new Home();
		home.setId(homeId);
		//long total_room_size=roomRepository.totalCountByHomeWise(homeId);
		long total_panel_count=0;
		List<Room> roomList=roomRepository.findByHome(home);
		long n=0;
		for(int i=0; i<roomList.size();i++)
		{
			n=iotProductRepository.getTotalCountByRoom(roomList.get(i).getId());
			total_panel_count=total_panel_count+n;
		}
		List<IOTProduct> IOTProduct=(List<IOTProduct>) iOTProductRepository.findAll();

		String last_iot_product_number="11";
		if(IOTProduct.size()!=0)
		{
			last_iot_product_number=IOTProduct.get(IOTProduct.size()-1).getIotProductNumber().toString();

			System.out.println("last_iot_product_number = "+last_iot_product_number);
		}

		IotProductNumber IotProductNumber=new IotProductNumber();
		IotProductNumber.setTotal_room_size(total_panel_count);
		IotProductNumber.setLast_iot_product_number(last_iot_product_number);

		return IotProductNumber;
	}


	@RequestMapping(value="/getPanelListByHome",method = RequestMethod.POST)
	public List<Panel> getPanelListByHome(@RequestBody HomeidVerificationDTO homeidVerificationDTO) {

		long homeId=Long.parseLong(homeidVerificationDTO.getHomeId());
		List<Panel> panelList= new ArrayList<Panel>();
		Home home=new Home();
		home.setId(homeId);
		List<Room> roomList=roomRepository.findByHome(home);
		Room room=new Room();
		for(int i=0; i<roomList.size();i++)
		{
			room=new Room();
			room.setId(roomList.get(i).getId());
			List<IOTProduct> IOTProduct=iotProductRepository.findByRoom(room);
			for(int j=0;j<IOTProduct.size();j++)
			{
				Panel panel=panelRepository.findByPanelId(IOTProduct.get(j).getIotProductNumber());
				panelList.add(panel);
			}

		}

		return panelList;
	}


}
