package com.gsmarthome.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gsmarthome.dto.ActivityDTO;
import com.gsmarthome.dto.ActivityResponseDTO;
import com.gsmarthome.dto.ResponseDTO;
import com.gsmarthome.entity.Activity;
import com.gsmarthome.entity.Home;
import com.gsmarthome.entity.UserDetails;
import com.gsmarthome.entity.VersionType;
import com.gsmarthome.repository.VersionTypeRepository;
import com.gsmarthome.util.Utility;

@RestController
@RequestMapping(value="/versionType")
public class VersionTypeController {


	@Autowired
	VersionTypeRepository versiontypeRepository;

	@RequestMapping(value = "/getCurrentVersionType", method = RequestMethod.POST)
	public ResponseEntity<VersionType> getCurrentVersionType() {

		VersionType versionType = new VersionType();
		
		try {
			List<VersionType> versionTypeList=versiontypeRepository.findAll();
			versionType.setId(versionTypeList.get(0).getId());
			versionType.setVersionName(versionTypeList.get(0).getVersionName());
			versionType.setVersionType(versionTypeList.get(0).getVersionType());
			return new ResponseEntity<VersionType>(versionType,HttpStatus.OK);

		} catch (Exception ex) {
			return new ResponseEntity<VersionType>(versionType,HttpStatus.OK);
		}
	}
	
}
