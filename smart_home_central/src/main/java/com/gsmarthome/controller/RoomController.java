package com.gsmarthome.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gsmarthome.dto.AddUpdateRoomDTO;
import com.gsmarthome.dto.ResponseDTO;
import com.gsmarthome.dto.RoomIdDTO;
import com.gsmarthome.dto.RoomListByUserDTO;
import com.gsmarthome.dto.UpdateHideRoomDTO;
import com.gsmarthome.dto.UpdateUserDeviceDTO;
import com.gsmarthome.dto.UserIdDTO;
import com.gsmarthome.dto.getReponseDTO;
import com.gsmarthome.service.RoomService;
import com.gsmarthome.service.UserDetailsService;
import com.gsmarthome.util.Utility;

@RestController
@RequestMapping(value="/room")
public class RoomController {
	private static final String DEVICE_ID_HEADER_NAME = "DEVICE-ID";
	private static final String DEVICE_TYPE_HEADER_NAME = "DEVICE-TYPE";
	  
	@Autowired  
	UserDetailsService usersDetailsService;
	
	@Autowired
	RoomService roomService;
	
	@Value("${API_STATUS_SUCCESS}")
	private String API_STATUS_SUCCESS;
	
	@Value("${API_STATUS_FAILURE}")
	private String API_STATUS_FAILURE;

	@RequestMapping(value="/add",method=RequestMethod.POST)
	public ResponseEntity<getReponseDTO> addRoom(@RequestBody AddUpdateRoomDTO dto)
	{
		getReponseDTO objgetReponseDTO = new getReponseDTO();
		try{
			 objgetReponseDTO=roomService.addUpdateRoom(dto);				
			 
			 return new ResponseEntity<getReponseDTO>(objgetReponseDTO,HttpStatus.OK);
			}catch (RuntimeException e) {
				return new ResponseEntity<getReponseDTO>(objgetReponseDTO,HttpStatus.OK);
			}
	}
	
	@RequestMapping(value="/getlistbyuser",method=RequestMethod.POST)
	public ResponseEntity<ResponseDTO<List<RoomListByUserDTO>>> getRoomList(@RequestBody UserIdDTO dto , HttpServletRequest request)
	{
		
		List<RoomListByUserDTO> roomList = roomService.getRoomListByVendorId(dto.getUserId(),dto.getMessageFrom());
		
		boolean isInternet = Utility.isInternetReachable();
		String isInternetStatus = "";
		
		if(isInternet){
			isInternetStatus = "Y";
		}else{
			isInternetStatus = "N";
		}
		
		final String device_id = request.getHeader(DEVICE_ID_HEADER_NAME);
		final String device_type = request.getHeader(DEVICE_TYPE_HEADER_NAME);
		UpdateUserDeviceDTO updateUserDeviceDTO= new UpdateUserDeviceDTO();
		updateUserDeviceDTO.setUserId(dto.getUserId());
		updateUserDeviceDTO.setDeviceId(device_id);
		updateUserDeviceDTO.setDeviceType(device_type);
		boolean flag = usersDetailsService.updateUserDevicetype(updateUserDeviceDTO);
				
		//return new ResponseEntity<ResponseDTO<List<RoomListByUserDTO>>>(new ResponseDTO<List<RoomListByUserDTO>>(API_STATUS_SUCCESS,roomList),HttpStatus.OK);
		return new ResponseEntity<ResponseDTO<List<RoomListByUserDTO>>>(new ResponseDTO<List<RoomListByUserDTO>>(API_STATUS_SUCCESS, roomList, isInternetStatus), HttpStatus.OK);
	}
	
	@RequestMapping(value="/hideroom",method=RequestMethod.POST)
	public ResponseEntity<ResponseDTO<Boolean>> changeHideRoomStatus(@RequestBody UpdateHideRoomDTO updateHideRoomDTO) 
	{
		Boolean boolUpdateHideRoom = false;
		try {
			boolUpdateHideRoom = roomService.changeRoomHideStatus(updateHideRoomDTO);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseDTO<Boolean>>(new ResponseDTO<Boolean>(API_STATUS_FAILURE,false),HttpStatus.BAD_REQUEST);
			
		}
		return new ResponseEntity<ResponseDTO<Boolean>>(new ResponseDTO<Boolean>(API_STATUS_FAILURE,boolUpdateHideRoom),HttpStatus.OK);
	}
	
	@RequestMapping(value="/delete",method=RequestMethod.POST)
	public ResponseEntity<ResponseDTO<Boolean>> deleteRoom(@RequestBody RoomIdDTO entityIdDTO)
	{
		Boolean result=roomService.deleteRoom(entityIdDTO.getRoomId());
		
		return new ResponseEntity<ResponseDTO<Boolean>>(new ResponseDTO<Boolean>(API_STATUS_SUCCESS,true),HttpStatus.OK);
		
	}
	
	
}
