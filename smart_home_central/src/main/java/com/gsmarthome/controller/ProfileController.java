package com.gsmarthome.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gsmarthome.dto.ChangeStatusByHomeDTO;
import com.gsmarthome.dto.ChangeStatusByRoomResponseDTO;
import com.gsmarthome.dto.ProfileRequestDTO;
import com.gsmarthome.dto.ProfileResponseDTO;
import com.gsmarthome.dto.ResponseDTO;
import com.gsmarthome.entity.Profile;
import com.gsmarthome.service.ProfileService;

@RestController
@RequestMapping(value = "/profile")
public class ProfileController {
	
	static Logger logger = Logger.getLogger(ProfileController.class.getName());
	
	@Autowired
	ProfileService profileService;
	
	@Value("${API_STATUS_SUCCESS}")
	private String API_STATUS_SUCCESS;

	@Value("${API_STATUS_FAILURE}")
	private String API_STATUS_FAILURE;
	
	/******************************************************************************************
	* Method Name: addProfile
	* 
	* Parameters: ProfileRequestDTO POJO request body
	* 
	* Description: This method is used to add a profile mode for user where the user will select 
	* Switches they want to turn on and respective Dimmer Status of dimmer Switches.
	* The Method will add all the switches status and profile name in database and publish MQTT 
	* Message to local and Internet topic by creating JSON Message
	* 
	* Response: ProfileResponseDTO POJO request body
	*
	******************************************************************************************/
	
	@RequestMapping(value = "/addprofile", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO<ProfileResponseDTO>> addProfile(
			@RequestBody ProfileRequestDTO reqProfileDTO) {
		
		ProfileResponseDTO profileResponseDTO = null;
		
		if(reqProfileDTO != null){
			try {
				profileResponseDTO = profileService.addProfileMode(reqProfileDTO);
			} catch (Exception e) {
				logger.error("Exception in addProfile()");
				e.printStackTrace();
				return new ResponseEntity<ResponseDTO<ProfileResponseDTO>>(
						new ResponseDTO<ProfileResponseDTO>(API_STATUS_FAILURE, profileResponseDTO,"Exception Message: "+e.getMessage()+" Cause: "+e.getCause()), HttpStatus.BAD_REQUEST);	
			}
		}
		
		return new ResponseEntity<ResponseDTO<ProfileResponseDTO>>(
					new ResponseDTO<ProfileResponseDTO>(API_STATUS_SUCCESS, profileResponseDTO,"Profile Added Successfully",reqProfileDTO.getUserId()), HttpStatus.OK);
		
		
	}
	
	/******************************************************************************************
	* Method Name: editProfile
	* 
	* Parameters: ProfileRequestDTO POJO request body
	* 
	* Description: This method is used to edit a profile mode for user.
	* The Method will update all the switches status and profile name in database and publish 
	* MQTT Message to local and Internet topic by creating JSON Message
	*
	* Response : ProfileResponseDTO POJO request body
	*
	******************************************************************************************/
	
	@RequestMapping(value = "/editprofile", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO<ProfileResponseDTO>> editProfile(
			@RequestBody ProfileRequestDTO reqProfileDTO) {
		
		ProfileResponseDTO respProfileDTO = null;
		
		if(reqProfileDTO != null){
			try {
				respProfileDTO = profileService.editProfileMode(reqProfileDTO);
				logger.info("editProfile(), Edit Profile Mode Updated Successfully");
				
			} catch (Exception e) {
				logger.error(e.getMessage());
				e.printStackTrace();
				return new ResponseEntity<ResponseDTO<ProfileResponseDTO>>(
						new ResponseDTO<ProfileResponseDTO>(API_STATUS_FAILURE, respProfileDTO,"Exception Message: "+e.getMessage()+" Cause: "+e.getCause()), HttpStatus.BAD_REQUEST);
			}
		}
		
		return new ResponseEntity<ResponseDTO<ProfileResponseDTO>>(
				new ResponseDTO<ProfileResponseDTO>(API_STATUS_SUCCESS, respProfileDTO,"Profile Edited Successfully",reqProfileDTO.getUserId()), HttpStatus.OK);
		
	}
	
	/******************************************************************************************
	* Method Name: deleteProfile
	* 
	* Parameters: ProfileRequestDTO POJO request body
	* 
	* Description: This method is used to delete the profile mode and publish MQTT 
	* Message to local and Internet topic by creating JSON Message
	* 
	* Response: ProfileResponseDTO POJO Response body
	*
	******************************************************************************************/
	
	@RequestMapping(value = "/deleteprofile", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO<ProfileResponseDTO>> deleteProfile(
			@RequestBody ProfileRequestDTO reqProfileDTO) {
		
		ProfileResponseDTO respProfileDTO = null;
		
		if(reqProfileDTO != null){
			try {
				respProfileDTO = profileService.deleteProfileMode(reqProfileDTO);
			} catch (Exception e) {
				logger.error(e.getMessage());
				e.printStackTrace();
				return new ResponseEntity<ResponseDTO<ProfileResponseDTO>>(
						new ResponseDTO<ProfileResponseDTO>(API_STATUS_FAILURE, respProfileDTO,"Exception Message: "+e.getMessage()+" Cause: "+e.getCause()), HttpStatus.BAD_REQUEST);
			}
		}
		
		return new ResponseEntity<ResponseDTO<ProfileResponseDTO>>(
					new ResponseDTO<ProfileResponseDTO>(API_STATUS_SUCCESS, respProfileDTO,"Profile Deleted Successfully"), HttpStatus.OK);
	}

	/******************************************************************************************
	* Function Name: getProfileList
	* 
	* Parameters: ProfileRequestDTO POJO request body
	* 
	* Description: This method is used to get all the profile list w.r.t user id.
	* The Method will check if user id exist in DB and return the profile created by that 
	* respective User if no profiles created by user then empty list will be given in response 
	* and publish over MQTT Message to local and Internet topic by creating JSON Message
	*
	* Response: List<ProfileResponseDTO>> List request body
	******************************************************************************************/
	@RequestMapping(value = "/getprofilelist", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO<List<ProfileResponseDTO>>> getProfileList(
			@RequestBody ProfileRequestDTO reqProfileDTO) {
		
		List<ProfileResponseDTO> listProfile = null;
		
		if(reqProfileDTO != null){
			try {
				listProfile = profileService.getProfileList(reqProfileDTO);
			} catch (Exception e) {
				logger.error(e.getMessage());
				e.printStackTrace();
				return new ResponseEntity<ResponseDTO<List<ProfileResponseDTO>>>(
						new ResponseDTO<List<ProfileResponseDTO>>(API_STATUS_FAILURE, listProfile,"Exception Message: "+e.getMessage()+" Cause: "+e.getCause()), HttpStatus.BAD_REQUEST);
			}
		}
		
		return new ResponseEntity<ResponseDTO<List<ProfileResponseDTO>>>(
					new ResponseDTO<List<ProfileResponseDTO>>(API_STATUS_SUCCESS, listProfile,"Profile List Fetched Successfully"), HttpStatus.OK);
	}
	
	/******************************************************************************************
	* Function Name: profileOnOff
	* 
	* Parameters: ChangeStatusByRoomResponseDTO POJO request body
	* 
	* Description: This method is used to change the profile mode for user where the user will 
	* select switches they want to turn on and respective Dimmer Status of dimmer Switches.
	* The Method will give command to panels and their switches all the switches status and profile name in also updated database and publish MQTT 
	* Message to local and Internet topic by creating JSON Message
	*
	* Response: ChangeStatusByHomeDTO POJO request body
	******************************************************************************************/
	
	//To change profile status of all switch of Home
	@RequestMapping(value = "/profileonoff", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO<List<ChangeStatusByRoomResponseDTO>>> profileOnOff(
			@RequestBody ChangeStatusByHomeDTO changeStatusByHomeDTO) {
		
		List<ChangeStatusByRoomResponseDTO> changeProfileStatus = null;
		try {
			changeProfileStatus = profileService.profileOnOff(changeStatusByHomeDTO);
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			
			return new ResponseEntity<ResponseDTO<List<ChangeStatusByRoomResponseDTO>>>(
					new ResponseDTO<List<ChangeStatusByRoomResponseDTO>>(API_STATUS_FAILURE, changeProfileStatus,"Exception Message: "+e.getMessage()+" Cause: "+e.getStackTrace(),changeStatusByHomeDTO.getUserId()), HttpStatus.BAD_REQUEST);
		}
		
		return new ResponseEntity<ResponseDTO<List<ChangeStatusByRoomResponseDTO>>>(
				new ResponseDTO<List<ChangeStatusByRoomResponseDTO>>(API_STATUS_SUCCESS, changeProfileStatus,"",changeStatusByHomeDTO.getUserId()), HttpStatus.OK);
	}
	
}
