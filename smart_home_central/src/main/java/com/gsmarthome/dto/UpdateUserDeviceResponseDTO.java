package com.gsmarthome.dto;

public class UpdateUserDeviceResponseDTO {
private Long userId;
	
	private Integer newUserType;

	public UpdateUserDeviceResponseDTO() {
	}

	public UpdateUserDeviceResponseDTO(Long userId, Integer newUserType) {
		this.userId = userId;
		this.newUserType = newUserType;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Integer getNewUserType() {
		return newUserType;
	}

	public void setNewUserType(Integer newUserType) {
		this.newUserType = newUserType;
	}
}
