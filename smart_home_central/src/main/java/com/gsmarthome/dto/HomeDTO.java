package com.gsmarthome.dto;

public class HomeDTO {

	private Long userId;

	private String homeName;
	private String isArmed;
	private String messageFrom;

	public HomeDTO() {
	}

	public HomeDTO(Long userId, String homeName) {
		this.userId = userId;
		this.homeName = homeName;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getHomeName() {
		return homeName;
	}

	public void setHomeName(String homeName) {
		this.homeName = homeName;
	}

	public String getIsArmed() {
		return isArmed;
	}

	public void setIsArmed(String isArmed) {
		this.isArmed = isArmed;
	}

	public String getMessageFrom() {
		return messageFrom;
	}

	public void setMessageFrom(String messageFrom) {
		this.messageFrom = messageFrom;
	}

}
