package com.gsmarthome.dto;

public class AddUpdateSwitchDTO {
	private Long userId;

	private Long roomId;

	private Long iotProductId;

	private Long switchId;

	private String switchName;

	private String switchType;

	private String dimmerStatus;

	private String dimmerValue;

	private String switchStatus;
	private String switchidentifier;
	private String messageFrom;
	
	private Integer switchImageId;

	public AddUpdateSwitchDTO() {
	}

	public AddUpdateSwitchDTO(Long roomId, Long iotProductId, Long switchId, String switchName, String switchType,
			String switchidentifier) {
		this.roomId = roomId;
		this.iotProductId = iotProductId;
		this.switchId = switchId;
		this.switchName = switchName;
		this.switchType = switchType;
		this.switchidentifier = switchidentifier;
	}

	public AddUpdateSwitchDTO(Long roomId, Long switchId, String switchName, String switchType, String dimmerStatus,
			String dimmerValue, String switchStatus, String switchidentifier) {
		super();
		this.roomId = roomId;
		this.switchId = switchId;
		this.switchName = switchName;
		this.switchType = switchType;
		this.dimmerStatus = dimmerStatus;
		this.dimmerValue = dimmerValue;
		this.switchStatus = switchStatus;
		this.switchidentifier = switchidentifier;
	}

	public Long getRoomId() {
		return roomId;
	}

	public void setRoomId(Long roomId) {
		this.roomId = roomId;
	}

	public Long getIotProductId() {
		return iotProductId;
	}

	public void setIotProductId(Long iotProductId) {
		this.iotProductId = iotProductId;
	}

	public String getSwitchName() {
		return switchName;
	}

	public void setSwitchName(String switchName) {
		this.switchName = switchName;
	}

	public String getSwitchType() {
		return switchType;
	}

	public void setSwitchType(String switchType) {
		this.switchType = switchType;
	}

	public Long getSwitchId() {
		return switchId;
	}

	public void setSwitchId(Long switchId) {
		this.switchId = switchId;
	}

	public String getDimmerStatus() {
		return dimmerStatus;
	}

	public void setDimmerStatus(String dimmerStatus) {
		this.dimmerStatus = dimmerStatus;
	}

	public String getDimmerValue() {
		return dimmerValue;
	}

	public void setDimmerValue(String dimmerValue) {
		this.dimmerValue = dimmerValue;
	}

	public String getSwitchStatus() {
		return switchStatus;
	}

	public void setSwitchStatus(String switchStatus) {
		this.switchStatus = switchStatus;
	}

	public String getSwitchidentifier() {
		return switchidentifier;
	}

	public void setSwitchidentifier(String switchidentifier) {
		this.switchidentifier = switchidentifier;
	}

	@Override
	public String toString() {
		return "AddUpdateSwitchDTO [roomId=" + roomId + ", iotProductId=" + iotProductId + ", switchId=" + switchId
				+ ", switchName=" + switchName + ", switchType=" + switchType + ", dimmerValue=" + dimmerValue
				+ ", dimmerStatus=" + dimmerStatus + "]";
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getMessageFrom() {
		return messageFrom;
	}

	public void setMessageFrom(String messageFrom) {
		this.messageFrom = messageFrom;
	}
	public Integer getSwitchImageId() {
		return switchImageId;
	}

	public void setSwitchImageId(Integer switchImageId) {
		this.switchImageId = switchImageId;
	}

}
