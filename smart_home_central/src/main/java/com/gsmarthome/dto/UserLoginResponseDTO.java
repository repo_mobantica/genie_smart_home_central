package com.gsmarthome.dto;

public class UserLoginResponseDTO {

	private Long id;
	private String firstName;
	private String lastName;
	private String email;
	private String birthDate;
	private Integer userType;
	private String phoneNumber;
	private String image;
	private Boolean isFirstTimeLogin;
	private String token;
	private String homeId;

	public UserLoginResponseDTO() {
		super();
	}

	public UserLoginResponseDTO(Long id, String firstName, String lastName, String email, String birthDate, Integer userType,
			String phoneNumber, String image, Boolean isFirstTimeLogin, String token,String homeId) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.birthDate = birthDate;
		this.userType = userType;
		this.phoneNumber = phoneNumber;
		this.image = image;
		this.isFirstTimeLogin = isFirstTimeLogin;
		this.token = token;
		this.homeId=homeId;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public Integer getUserType() {
		return userType;
	}

	public void setUserType(Integer userType) {
		this.userType = userType;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Boolean getIsFirstTimeLogin() {
		return isFirstTimeLogin;
	}

	public void setIsFirstTimeLogin(Boolean isFirstTimeLogin) {
		this.isFirstTimeLogin = isFirstTimeLogin;
	}
	

	public String getHomeId() {
		return homeId;
	}

	public void setHomeId(String homeId) {
		this.homeId = homeId;
	}

	@Override
	public String toString() {
		return "UserLoginResponseDTO [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", email="
				+ email + ", userType=" + userType + ", phoneNumber=" + phoneNumber + ", image=" + image
				+ ", isFirstTimeLogin=" + isFirstTimeLogin + ", token=" + token + "]";
	}

}
