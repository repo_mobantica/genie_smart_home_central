package com.gsmarthome.dto;

public class UserShareControlDTO {

	private String email;

	private Integer userType;

	private Long adminUserId;
	
	private String phoneNumber;

	private String validityDate;

	public UserShareControlDTO() {
		super();
	}

	public UserShareControlDTO(String phoneNumber,String email, Integer userType, Long adminUserId) {
		this.email = email;
		this.userType = userType;
		this.adminUserId = adminUserId;
		this.phoneNumber=phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getUserType() {
		return userType;
	}

	public void setUserType(Integer userType) {
		this.userType = userType;
	}

	public Long getAdminUserId() {
		return adminUserId;
	}

	public void setAdminUserId(Long adminUserId) {
		this.adminUserId = adminUserId;
	}	

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	public String getValidityDate() {
		return validityDate;
	}

	public void setValidityDate(String validityDate) {
		this.validityDate = validityDate;
	}

	@Override
	public String toString() {
		return "UserShareControlDTO [email=" + email + ", userType=" + userType + ", adminUserId=" + adminUserId
				+ ", phoneNumber=" + phoneNumber + ", validityDate=" + validityDate + "]";
	}
	
	
}
