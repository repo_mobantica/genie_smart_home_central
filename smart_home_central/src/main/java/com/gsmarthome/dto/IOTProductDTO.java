package com.gsmarthome.dto;

public class IOTProductDTO {

	private Long homeId;

	private Long roomId;

	private String productName;

	public IOTProductDTO() {
	}

	public IOTProductDTO(Long homeId, Long roomId, String productName) {
		this.homeId = homeId;
		this.roomId = roomId;
		this.productName = productName;
	}

	public Long getHomeId() {
		return homeId;
	}

	public void setHomeId(Long homeId) {
		this.homeId = homeId;
	}

	public Long getRoomId() {
		return roomId;
	}

	public void setRoomId(Long roomId) {
		this.roomId = roomId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	@Override
	public String toString() {
		return "IOTProductDTO [homeId=" + homeId + ", roomId=" + roomId + ", productName=" + productName + "]";
	}

}
