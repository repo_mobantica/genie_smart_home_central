package com.gsmarthome.dto;

public class EntityIdDTO {

	private Long entityId;

	public EntityIdDTO() {
	}

	public EntityIdDTO(Long entityId) {
		this.entityId = entityId;
	}

	public Long getEntityId() {
		return entityId;
	}

	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}

	@Override
	public String toString() {
		return "EntityIdDTO [entityId=" + entityId + "]";
	}

}
