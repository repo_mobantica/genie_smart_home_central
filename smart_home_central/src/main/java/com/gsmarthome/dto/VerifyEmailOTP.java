package com.gsmarthome.dto;

public class VerifyEmailOTP {

	private String email;
	
	private String otp;

	public VerifyEmailOTP() {
	}

	public VerifyEmailOTP(String email, String otp) {
		this.email = email;
		this.otp = otp;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	@Override
	public String toString() {
		return "VerifyEmailOTP [email=" + email + ", otp=" + otp + "]";
	}
	
}
