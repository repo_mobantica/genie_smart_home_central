package com.gsmarthome.dto;

public class ResponseDTO<T> {

	String status;

	T result;

	String msg;

	Long userId;

	public ResponseDTO() {
		super();
	}

	public ResponseDTO(String status, T result) {
		super();
		this.status = status;
		this.result = result;
	}

	public ResponseDTO(String status, T result, String msg) {
		super();
		this.status = status;
		this.result = result;
		this.msg = msg;
	}

	public ResponseDTO(String status, T result, String msg, Long userId) {
		super();
		this.status = status;
		this.result = result;
		this.msg = msg;
		this.userId = userId;
	}
	
	public T getResult() {
		return result;
	}

	public void setResult(T result) {
		this.result = result;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	public Long getUserid() {
		return userId;
	}

	public void setUserid(Long userid) {
		this.userId = userid;
	}

}
