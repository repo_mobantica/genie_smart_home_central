package com.gsmarthome.dto;

public class ChangeStatusByHomeDTO {

	private String switchId;
	private String profileId;	
	private String onoffstatus;
	private String modeName;
	private String dimmerValues;
	private Long userId;
	private String messageFrom;

	public String getSwitchId() {
		return switchId;
	}

	public void setSwitchId(String switchId) {
		this.switchId = switchId;
	}
	
	public String getProfileId() {
		return profileId;
	}

	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}

	public String getOnoffstatus() {
		return onoffstatus;
	}

	public void setOnoffstatus(String onoffstatus) {
		this.onoffstatus = onoffstatus;
	}
	
	public String getModeName() {
		return modeName;
	}

	public void setModeName(String modeName) {
		this.modeName = modeName;
	}

	public String getDimmerValues() {
		return dimmerValues;
	}

	public void setDimmerValues(String dimmerValues) {
		this.dimmerValues = dimmerValues;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getMessageFrom() {
		return messageFrom;
	}

	public void setMessageFrom(String messageFrom) {
		this.messageFrom = messageFrom;
	}

}
