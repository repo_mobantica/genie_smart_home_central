package com.gsmarthome.dto;

public class PanelDetailsDTO {


	private String panelCommand;
	private String panelName;
	
	public String getPanelCommand() {
		return panelCommand;
	}
	public void setPanelCommand(String panelCommand) {
		this.panelCommand = panelCommand;
	}
	public String getPanelName() {
		return panelName;
	}
	public void setPanelName(String panelName) {
		this.panelName = panelName;
	}
	
	
}
