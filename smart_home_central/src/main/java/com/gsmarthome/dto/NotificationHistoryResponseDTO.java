package com.gsmarthome.dto;

public class NotificationHistoryResponseDTO {
	
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	public NotificationHistoryResponseDTO()
	{
		
	}
	public NotificationHistoryResponseDTO(String message) {
		super();
		this.message = message;
	}
}
