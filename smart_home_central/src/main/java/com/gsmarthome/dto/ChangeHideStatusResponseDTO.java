package com.gsmarthome.dto;

public class ChangeHideStatusResponseDTO {
	private String homeId;

	private String panelNumber;

	private String switchNumber;

	private String hideStatus;

	public ChangeHideStatusResponseDTO() {
	}

	public ChangeHideStatusResponseDTO(String homeId, String panelNumber, String switchNumber, String hideStatus) {
		this.homeId = homeId;
		this.panelNumber = panelNumber;
		this.switchNumber = switchNumber;
		this.hideStatus = hideStatus;
	}

	public String getHomeId() {
		return homeId;
	}

	public void setHomeId(String homeId) {
		this.homeId = homeId;
	}

	public String getPanelNumber() {
		return panelNumber;
	}

	public void setPanelNumber(String panelNumber) {
		this.panelNumber = panelNumber;
	}

	public String getSwitchNumber() {
		return switchNumber;
	}

	public void setSwitchNumber(String switchNumber) {
		this.switchNumber = switchNumber;
	}

	public String getHideStatus() {
		return hideStatus;
	}

	public void setHideStatus(String hideStatus) {
		this.hideStatus = hideStatus;
	}

	@Override
	public String toString() {
		return "ChangeHideStatusResponseDTO [homeId=" + homeId + ", panelNumber=" + panelNumber + ", switchNumber="
				+ switchNumber + ", hideStatus=" + hideStatus + "]";
	}
}
