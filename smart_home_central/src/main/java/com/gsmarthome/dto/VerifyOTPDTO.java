package com.gsmarthome.dto;

public class VerifyOTPDTO {

	private Long userId;

	private String otp;

	public VerifyOTPDTO() {
	}

	public VerifyOTPDTO(Long userId, String otp) {
		this.userId = userId;
		this.otp = otp;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

}
