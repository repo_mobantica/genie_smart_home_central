package com.gsmarthome.dto;

public class UpdateUserDeviceDTO {	
	private Long userId;
	private String deviceId;
    private String deviceType;
    
    public UpdateUserDeviceDTO() {
		super();
	}

	public UpdateUserDeviceDTO(Long userId, String deviceId, String deviceType) {
		super();
		this.userId = userId;
		this.deviceId = deviceId;
		this.deviceType = deviceType;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public String getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	} 
	@Override
	public String toString() {
		return "UpdateUserDeviceDTO [userId=" + userId + ", newUserType="
				+ deviceId + "]";
	}
    
}
