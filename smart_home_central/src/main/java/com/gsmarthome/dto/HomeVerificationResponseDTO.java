package com.gsmarthome.dto;

import com.gsmarthome.entity.DeviceDetails;
import com.gsmarthome.entity.UserDetails;

public class HomeVerificationResponseDTO {

	private DeviceDetails deviceDetails = new DeviceDetails();
	private UserDetails userDetails = new UserDetails();
	private String homeId;
	private String Otpcode;
	private String Status;
	private String Username;

	public HomeVerificationResponseDTO() {
		super();
	}

	public HomeVerificationResponseDTO(DeviceDetails deviceDetails, UserDetails userDetails, String homeId,String Otpcode, String Status, String Username) {
		this.deviceDetails = deviceDetails;
		this.userDetails = userDetails;
		this.homeId = homeId;
		this.Otpcode = Otpcode;
		this.Status = Status;
		this.Username = Username;
	}

	public DeviceDetails getDeviceDetails() {
		return deviceDetails;
	}

	public void setDeviceDetails(DeviceDetails deviceDetails) {
		this.deviceDetails = deviceDetails;
	}

	public UserDetails getUserDetails() {
		return userDetails;
	}

	public void setUserDetails(UserDetails userDetails) {
		this.userDetails = userDetails;
	}
	
	public String getHomeId() {
		return homeId;
	}

	public void setHomeId(String homeId) {
		this.homeId = homeId;
	}

	public String getOtpcode() {
		return Otpcode;
	}

	public void setOtpcode(String otpcode) {
		Otpcode = otpcode;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public String getUsername() {
		return Username;
	}

	public void setUsername(String username) {
		Username = username;
	}

}
