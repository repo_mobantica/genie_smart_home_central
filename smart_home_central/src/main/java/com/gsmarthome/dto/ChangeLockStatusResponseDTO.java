package com.gsmarthome.dto;

public class ChangeLockStatusResponseDTO {

	private String homeId;

	private String panelNumber;

	private String switchNumber;

	private String lockStatus;

	public ChangeLockStatusResponseDTO() {
	}

	public ChangeLockStatusResponseDTO(String homeId, String panelNumber, String switchNumber, String lockStatus) {
		this.homeId = homeId;
		this.panelNumber = panelNumber;
		this.switchNumber = switchNumber;
		this.lockStatus = lockStatus;
	}

	public String getHomeId() {
		return homeId;
	}

	public void setHomeId(String homeId) {
		this.homeId = homeId;
	}

	public String getPanelNumber() {
		return panelNumber;
	}

	public void setPanelNumber(String panelNumber) {
		this.panelNumber = panelNumber;
	}

	public String getSwitchNumber() {
		return switchNumber;
	}

	public void setSwitchNumber(String switchNumber) {
		this.switchNumber = switchNumber;
	}

	public String getLockStatus() {
		return lockStatus;
	}

	public void setLockStatus(String lockStatus) {
		this.lockStatus = lockStatus;
	}

	@Override
	public String toString() {
		return "ChangeSwitchStatusResponseDTO [homeId=" + homeId + ", panelNumber=" + panelNumber + ", switchNumber="
				+ switchNumber + ", lockStatus=" + lockStatus + "]";
	}

}
