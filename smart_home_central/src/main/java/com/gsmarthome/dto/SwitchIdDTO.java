package com.gsmarthome.dto;

public class SwitchIdDTO {

	private Long switchId;

	public SwitchIdDTO() {
	}

	public SwitchIdDTO(Long switchId) {
		this.switchId = switchId;
	}

	public Long getSwitchId() {
		return switchId;
	}

	public void setSwitchId(Long switchId) {
		this.switchId = switchId;
	}

}
