package com.gsmarthome.dto;

public class ChangeStatusByRoomResponseDTO {
	private long switchid;
	private int status;

	public long getSiwtchid() {
		return switchid;
	}

	public void setSiwtchid(long switchid) {
		this.switchid = switchid;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public ChangeStatusByRoomResponseDTO(long switchid, int status) {
		super();
		this.switchid = switchid;
		this.status = status;

	}

}
