package com.gsmarthome.dto;

import java.util.ArrayList;
import java.util.List;

import com.gsmarthome.entity.RoomType;
import com.gsmarthome.entity.SwitchType;

public class HomeDetailsResponseDTO {
	private List<RoomType> roomtypeList = new ArrayList<RoomType>();
	private List<SwitchType> switchtypeList = new ArrayList<SwitchType>();
	private String noofswitchs;

	public HomeDetailsResponseDTO() {

		super();
	}

	public HomeDetailsResponseDTO(String noofswitchs, List<RoomType> roomtypeList, List<SwitchType> switchtypeList) {
		this.noofswitchs = noofswitchs;
		this.switchtypeList = switchtypeList;
		this.roomtypeList = roomtypeList;
	}

	public List<RoomType> getRoomtypeList() {
		return roomtypeList;
	}

	public void setRoomtypeList(List<RoomType> roomtypeList) {
		this.roomtypeList = roomtypeList;
	}

	public List<SwitchType> getSwitchtypeList() {
		return switchtypeList;
	}

	public void setSwitchtypeList(List<SwitchType> switchtypeList) {
		this.switchtypeList = switchtypeList;
	}

	public String getNoofswitchs() {
		return noofswitchs;
	}

	public void setNoofswitchs(String noofswitchs) {
		this.noofswitchs = noofswitchs;
	}

}
