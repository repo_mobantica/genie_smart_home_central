package com.gsmarthome.dto;

public class verifygeniehubDTO {
	private String response;
	private String Status;

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

}
