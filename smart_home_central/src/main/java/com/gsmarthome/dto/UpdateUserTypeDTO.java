package com.gsmarthome.dto;

public class UpdateUserTypeDTO {
	
	private Long userId;
	private Integer newUserType;
	private Long adminUserId;
	private String validityDate;

	public UpdateUserTypeDTO() {
		
	}

	public UpdateUserTypeDTO(Long userId, Integer newUserType) {
		this.userId = userId;
		this.newUserType = newUserType;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Integer getNewUserType() {
		return newUserType;
	}

	public void setNewUserType(Integer newUserType) {
		this.newUserType = newUserType;
	}

	public Long getAdminUserId() {
		return adminUserId;
	}

	public void setAdminUserId(Long adminUserId) {
		this.adminUserId = adminUserId;
	}
	
	public String getValidityDate() {
		return validityDate;
	}

	public void setValidityDate(String validityDate) {
		this.validityDate = validityDate;
	}
	
	@Override
	public String toString() {
		return "UpdateUserTypeDTO [userId=" + userId + ", newUserType="
				+ newUserType + "]";
	}

}
