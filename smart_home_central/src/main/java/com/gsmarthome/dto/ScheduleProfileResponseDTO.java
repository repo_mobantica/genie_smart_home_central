package com.gsmarthome.dto;

public class ScheduleProfileResponseDTO {


	private String userId;
	private String profileId;
	private String profileName;
	private String scheduleId;
	private String switchStatus;
	private String lockStatus;
	private String repeatStatus;
	private String repeatWeek;
	private String scheduleDateTime;
	

	public ScheduleProfileResponseDTO() {
		super();
	}
	public ScheduleProfileResponseDTO(String scheduleId, String userId,	String scheduleDateTime) {
		this.scheduleId = scheduleId;
		this.userId = userId;
		this.scheduleDateTime = scheduleDateTime;
	}
	
	public ScheduleProfileResponseDTO(String profileId, String profileName, String scheduleId, String switchStatus,String repeatStatus, String repeatWeek, String userId,
			String scheduleDateTime) {
		this.profileId = profileId;
		this.profileName=profileName;
		this.scheduleId = scheduleId;
		this.userId = userId;
		this.repeatStatus= repeatStatus;
		this.repeatWeek= repeatWeek;
		this.switchStatus = switchStatus;
		this.scheduleDateTime = scheduleDateTime;
		
	}
	
	public String getScheduleId() {
		return scheduleId;
	}

	public void setScheduleId(String scheduleId) {
		this.scheduleId = scheduleId;
	}
	
	public String getProfileId() {
		return profileId;
	}

	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}

	public String getProfileName() {
		return profileName;
	}

	public void setProfileName(String profileName) {
		this.profileName = profileName;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getSwitchStatus() {
		return switchStatus;
	}

	public void setSwitchStatus(String switchStatus) {
		this.switchStatus = switchStatus;
	}

	public String getLockStatus() {
		return lockStatus;
	}

	public void setLockStatus(String lockStatus) {
		this.lockStatus = lockStatus;
	}
	public String getRepeatStatus() {
		return repeatStatus;
	}

	public void setRepeatStatus(String repeatStatus) {
		this.repeatStatus = repeatStatus;
	}

	public String getRepeatWeek() {
		return repeatWeek;
	}

	public void setRepeatWeek(String repeatWeek) {
		this.repeatWeek = repeatWeek;
	}
	
	public String getScheduleDateTime() {
		return scheduleDateTime;
	}

	public void setScheduleDateTime(String scheduleDateTime) {
		this.scheduleDateTime = scheduleDateTime;
	}
	
}
