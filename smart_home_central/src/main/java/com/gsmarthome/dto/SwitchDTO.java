package com.gsmarthome.dto;

import java.util.ArrayList;
import java.util.List;

import com.gsmarthome.entity.Switch;

public class SwitchDTO {

	private Long homeid;
	private String response;
	private int status;

	List<SwitchResponseDTO> switchList=new ArrayList<SwitchResponseDTO>();

	public Long getHomeid() {
		return homeid;
	}

	public void setHomeid(Long homeid) {
		this.homeid = homeid;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public List<SwitchResponseDTO> getSwitchList() {
		return switchList;
	}

	public void setSwitchList(List<SwitchResponseDTO> switchList) {
		this.switchList = switchList;
	}
	
}
