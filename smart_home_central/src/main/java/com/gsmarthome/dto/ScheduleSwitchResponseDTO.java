package com.gsmarthome.dto;

public class ScheduleSwitchResponseDTO {
	private Long switchId;
	private String switchStatus;
	private String lockStatus;
	private Long userId;
	private String scheduleDateTime;

	public ScheduleSwitchResponseDTO() {

		super();
	}

	public ScheduleSwitchResponseDTO(Long switchId, String switchStatus, String lockStatus, Long userId,
			String scheduleDateTime) {

		this.switchStatus = switchStatus;
		this.lockStatus = lockStatus;
		this.userId = userId;
		this.scheduleDateTime = scheduleDateTime;
		this.switchId = switchId;
	}

	public Long getSwitchId() {
		return switchId;
	}

	public void setSwitchId(Long switchId) {
		this.switchId = switchId;
	}

	public String getSwitchStatus() {
		return switchStatus;
	}

	public void setSwitchStatus(String switchStatus) {
		this.switchStatus = switchStatus;
	}

	public String getLockStatus() {
		return lockStatus;
	}

	public void setLockStatus(String lockStatus) {
		this.lockStatus = lockStatus;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getScheduleDateTime() {
		return scheduleDateTime;
	}

	public void setScheduleDateTime(String scheduleDateTime) {
		this.scheduleDateTime = scheduleDateTime;
	}

	@Override
	public String toString() {
		return "ScheduleSwitchResponseDTO [switchId= " + switchId + " userId" + userId + " scheduleDateTime "
				+ scheduleDateTime + " switchStatus " + switchStatus + " lockStatus= " + lockStatus + "]";
	}

}
