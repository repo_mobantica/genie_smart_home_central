package com.gsmarthome.dto;

public class ValidateUserResponse {
	private String userType;
    
	private String homeId;

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getHomeId() {
		return homeId;
	}

	public void setHomeId(String homeId) {
		this.homeId = homeId;
	}
	
	
}
