package com.gsmarthome.dto;

public class ValidateVoterUserDTO {

	
	private String voterId;
	
	private String email;

	private String voterAddress;

	public ValidateVoterUserDTO() {
	}

	public ValidateVoterUserDTO(String email, String voterId, String voterAddress) {
		this.email = email;
		this.voterId = voterId;
		this.voterAddress = voterAddress;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDeviceId() {
		return voterId;
	}

	public void setDeviceId(String voterId) {
		this.voterId = voterId;
	}

	public String getShareControlOTP() {
		return voterAddress;
	}

	public void setShareControlOTP(String voterAddress) {
		this.voterAddress = voterAddress;
	}

	@Override
	public String toString() {
		return "ValidateVoterUserDTO [email=" + email + ", voterId=" + voterId + ", voterAddress=" + voterAddress
				+ "]";
	}

}
