package com.gsmarthome.dto;

public class GenieCamResponseDTO {

	private String genieCamId;
	private String userId;
	private String camName;
	private String localIP;
	private String internetIP;
	private String localPort;
	private String InternetPort;
	private String messageFrom;

	public GenieCamResponseDTO() {
	}

	public String getGenieCamId() {
		return genieCamId;
	}

	public void setGenieCamId(String genieCamId) {
		this.genieCamId = genieCamId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getCamName() {
		return camName;
	}

	public void setCamName(String camName) {
		this.camName = camName;
	}

	public String getLocalIP() {
		return localIP;
	}

	public void setLocalIP(String localIP) {
		this.localIP = localIP;
	}

	public String getInternetIP() {
		return internetIP;
	}

	public void setInternetIP(String internetIP) {
		this.internetIP = internetIP;
	}

	public String getLocalPort() {
		return localPort;
	}

	public void setLocalPort(String localPort) {
		this.localPort = localPort;
	}

	public String getInternetPort() {
		return InternetPort;
	}

	public void setInternetPort(String internetPort) {
		InternetPort = internetPort;
	}
	
	public String getMessageFrom() {
		return messageFrom;
	}

	public void setMessageFrom(String messageFrom) {
		this.messageFrom = messageFrom;
	}

}
