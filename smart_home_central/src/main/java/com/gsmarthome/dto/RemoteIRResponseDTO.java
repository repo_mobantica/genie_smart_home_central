package com.gsmarthome.dto;

public class RemoteIRResponseDTO {

	private String irRemoteinfo;
	private String userId;
	private String homeId;
	private String brandName;
	private String remoteType;
	private String modelNumber;
	
	public RemoteIRResponseDTO() {
	}

	public String getIrRemoteinfo() {
		return irRemoteinfo;
	}

	public void setIrRemoteinfo(String irRemoteinfo) {
		this.irRemoteinfo = irRemoteinfo;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getHomeId() {
		return homeId;
	}

	public void setHomeId(String homeId) {
		this.homeId = homeId;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getRemoteType() {
		return remoteType;
	}

	public void setRemoteType(String remoteType) {
		this.remoteType = remoteType;
	}

	public String getModelNumber() {
		return modelNumber;
	}

	public void setModelNumber(String modelNumber) {
		this.modelNumber = modelNumber;
	}

	@Override
	public String toString() {
		return "RemoteIRDTO [irRemoteinfo=" + irRemoteinfo + ", userId=" + userId + ", homeId=" + homeId
				+ ", brandName=" + brandName + ", remoteType=" + remoteType + ", modelNumber=" + modelNumber + "]";
	}

}
