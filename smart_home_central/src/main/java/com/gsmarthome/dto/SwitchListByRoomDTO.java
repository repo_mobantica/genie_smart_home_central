package com.gsmarthome.dto;

import com.gsmarthome.entity.IOTProduct;

public class SwitchListByRoomDTO {

	private String id;

	private String switchName;

	private Integer switchNumber;
	
	private String switchType;

	private String dimmerStatus;

	private String dimmerValue;

	private String switchStatus;

	private byte[] onImage;

	private byte[] offImage;

	private String lockStatus;
	private String hideStatus;
	private String switchIdentifer;
	
	private String panelname;
	private String iotProduct;
	public SwitchListByRoomDTO(String id, String switchName, Integer switchNumber, String switchType, String dimmerStatus, String dimmerValue,
			String switchStatus, byte[] onImage, byte[] offImage, String lockStatus, String hideStatus,
			String switchIdentifer, String panelname, String iotProduct) {
		super();
		this.id = id;
		this.switchName = switchName;
		this.switchNumber=switchNumber;
		this.switchType = switchType;
		this.dimmerStatus = dimmerStatus;
		this.dimmerValue = dimmerValue;
		this.switchStatus = switchStatus;
		this.onImage = onImage;
		this.offImage = offImage;
		this.lockStatus = lockStatus;
		this.hideStatus = hideStatus;
		this.switchIdentifer = switchIdentifer;
		this.panelname=panelname;
		this.iotProduct=iotProduct;
	}
	

	public String getIotProduct() {
		return iotProduct;
	}


	public void setIotProduct(String iotProduct) {
		this.iotProduct = iotProduct;
	}


	public String getPanelname() {
		return panelname;
	}


	public void setPanelname(String panelname) {
		this.panelname = panelname;
	}


	public Integer getSwitchNumber() {
		return switchNumber;
	}


	public void setSwitchNumber(Integer switchNumber) {
		this.switchNumber = switchNumber;
	}


	public String getLockStatus() {
		return lockStatus;
	}

	public void setLockStatus(String lockStatus) {
		this.lockStatus = lockStatus;
	}

	public String getHideStatus() {
		return hideStatus;
	}

	public void setHideStatus(String hideStatus) {
		this.hideStatus = hideStatus;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSwitchName() {
		return switchName;
	}

	public void setSwitchName(String switchName) {
		this.switchName = switchName;
	}

	public String getSwitchType() {
		return switchType;
	}

	public void setSwitchType(String switchType) {
		this.switchType = switchType;
	}

	public byte[] getOnImage() {
		return onImage;
	}

	public void setOnImage(byte[] onImage) {
		this.onImage = onImage;
	}

	public byte[] getOffImage() {
		return offImage;
	}

	public void setOffImage(byte[] offImage) {
		this.offImage = offImage;
	}

	public String getDimmerStatus() {
		return dimmerStatus;
	}

	public void setDimmerStatus(String dimmerStatus) {
		this.dimmerStatus = dimmerStatus;
	}

	public String getDimmerValue() {
		return dimmerValue;
	}

	public void setDimmerValue(String dimmerValue) {
		this.dimmerValue = dimmerValue;
	}

	public String getSwitchStatus() {
		return switchStatus;
	}

	public void setSwitchStatus(String switchStatus) {
		this.switchStatus = switchStatus;
	}

	public String getSwitchIdentifer() {
		return switchIdentifer;
	}

	public void setSwitchIdentifer(String switchIdentifer) {
		this.switchIdentifer = switchIdentifer;
	}

}
