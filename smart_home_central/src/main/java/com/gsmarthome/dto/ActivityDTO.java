package com.gsmarthome.dto;

public class ActivityDTO {

	private Long homeId;

	public ActivityDTO() {
	}

	public ActivityDTO(Long homeId) {
		this.homeId = homeId;
	}

	public Long getHomeId() {
		return homeId;
	}

	public void setHomeId(Long homeId) {
		this.homeId = homeId;
	}

	@Override
	public String toString() {
		return "HomeDTO [userId=" + homeId + "]";
	}

}
