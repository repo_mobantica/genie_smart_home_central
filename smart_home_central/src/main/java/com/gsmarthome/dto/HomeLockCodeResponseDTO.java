package com.gsmarthome.dto;

import com.gsmarthome.entity.Switch;

public class HomeLockCodeResponseDTO {
	
	private Switch objSwitch;
	private String lockCode;

	public HomeLockCodeResponseDTO() {
	
	}
	
	public String getLockCode() {
		return lockCode;
	}

	public void setLockCode(String passCode) {
		this.lockCode = passCode;
	}

	public Switch getObjSwitch() {
		return objSwitch;
	}

	public void setObjSwitch(Switch objSwitch) {
		this.objSwitch = objSwitch;
	}
	
}
