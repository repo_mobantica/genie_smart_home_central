package com.gsmarthome.dto;

public class SwitchResponseDTO {

	private Integer switchNumber;

	private String dimmerValue;

	private String switchStatus;

	private String lockStatus;

	private Long iotProduct;
	
	private String panelName;
	
	private String panelCommand;


	public Integer getSwitchNumber() {
		return switchNumber;
	}

	public void setSwitchNumber(Integer switchNumber) {
		this.switchNumber = switchNumber;
	}

	public String getDimmerValue() {
		return dimmerValue;
	}

	public void setDimmerValue(String dimmerValue) {
		this.dimmerValue = dimmerValue;
	}

	public String getSwitchStatus() {
		return switchStatus;
	}

	public void setSwitchStatus(String switchStatus) {
		this.switchStatus = switchStatus;
	}

	public String getLockStatus() {
		return lockStatus;
	}

	public void setLockStatus(String lockStatus) {
		this.lockStatus = lockStatus;
	}

	public Long getIotProduct() {
		return iotProduct;
	}

	public void setIotProduct(Long iotProduct) {
		this.iotProduct = iotProduct;
	}

	public String getPanelName() {
		return panelName;
	}

	public void setPanelName(String panelName) {
		this.panelName = panelName;
	}

	public String getPanelCommand() {
		return panelCommand;
	}

	public void setPanelCommand(String panelCommand) {
		this.panelCommand = panelCommand;
	}
	
}
