package com.gsmarthome.dto;

public class VerifyOTPResponse {

	private Long userId;

	private String X_AUTH_TOKEN;

	public VerifyOTPResponse() {
	}

	public VerifyOTPResponse(Long userId, String x_AUTH_TOKEN) {
		this.userId = userId;
		X_AUTH_TOKEN = x_AUTH_TOKEN;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getX_AUTH_TOKEN() {
		return X_AUTH_TOKEN;
	}

	public void setX_AUTH_TOKEN(String x_AUTH_TOKEN) {
		X_AUTH_TOKEN = x_AUTH_TOKEN;
	}
}
