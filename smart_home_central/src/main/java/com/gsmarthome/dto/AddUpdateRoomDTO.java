package com.gsmarthome.dto;

public class AddUpdateRoomDTO {

	private Long userId;

	private Long roomId;

	private String roomName;

	private String roomType;
	
	private String messageFrom;
	private Integer roomImageId;

	public Integer getRoomImageId() {
		return roomImageId;
	}

	public void setRoomImageId(Integer roomImageId) {
		this.roomImageId = roomImageId;
	}

	public AddUpdateRoomDTO() {
	}

	public AddUpdateRoomDTO(Long userId, Long roomId, String roomName, String roomType) {
		this.userId = userId;
		this.roomId = roomId;
		this.roomName = roomName;
		this.roomType = roomType;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getRoomId() {
		return roomId;
	}

	public void setRoomId(Long roomId) {
		this.roomId = roomId;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public String getRoomType() {
		return roomType;
	}

	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}

	@Override
	public String toString() {
		return "AddUpdateRoomDTO [userId=" + userId + ", roomId=" + roomId + ", roomName=" + roomName + ", roomType="
				+ roomType + "]";
	}

	public String getMessageFrom() {
		return messageFrom;
	}

	public void setMessageFrom(String messageFrom) {
		this.messageFrom = messageFrom;
	}

	
	
	

}
