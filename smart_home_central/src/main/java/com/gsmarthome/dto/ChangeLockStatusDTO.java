package com.gsmarthome.dto;

public class ChangeLockStatusDTO {
	private Long switchId;
	private Long userId;

	private String lockStatus;
	private String messageFrom;

	public ChangeLockStatusDTO() {
		super();
	}

	public ChangeLockStatusDTO(Long switchId, String lockStatus) {
		super();
		this.switchId = switchId;
		this.lockStatus = lockStatus;

	}

	public Long getSwitchId() {
		return switchId;
	}

	public void setSwitchId(Long switchId) {
		this.switchId = switchId;
	}

	public String getLockStatus() {
		return lockStatus;
	}

	public void setLockStatus(String lockStatus) {
		this.lockStatus = lockStatus;
	}

	@Override
	public String toString() {
		return "ChangeLockStatusDTO [switchId=" + switchId + ", lockStatus=" + lockStatus + "]";
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getMessageFrom() {
		return messageFrom;
	}

	public void setMessageFrom(String messageFrom) {
		this.messageFrom = messageFrom;
	}
	
	

}
