package com.gsmarthome.dto;

public class ChangeSwitchStatusDTO {

	private Long switchId;
	private String switchStatus;
	private String dimmerValue;
	private Long userid;	
	private String messageFrom;
	private String genieColor;
	
	public Long getUserid() {
		return userid;
	}

	public void setUserid(Long userid) {
		this.userid = userid;
	}

	public ChangeSwitchStatusDTO() {
		super();
	}

	public ChangeSwitchStatusDTO(Long switchId, String switchStatus, String dimmerValue, Long userid) {
		super();
		this.switchId = switchId;
		this.switchStatus = switchStatus;
		this.dimmerValue = dimmerValue;
		this.userid = userid;

	}

	public Long getSwitchId() {
		return switchId;
	}

	public void setSwitchId(Long switchId) {
		this.switchId = switchId;
	}

	public String getSwitchStatus() {
		return switchStatus;
	}

	public void setSwitchStatus(String switchStatus) {
		this.switchStatus = switchStatus;
	}

	public String getDimmerValue() {
		return dimmerValue;
	}

	public void setDimmerValue(String dimmerValue) {
		this.dimmerValue = dimmerValue;
	}

	public String getMessageFrom() {
		return messageFrom;
	}

	public void setMessageFrom(String messageFrom) {
		this.messageFrom = messageFrom;
	}
	
	@Override
	public String toString() {
		return "ChangeSwitchStatusDTO [switchId=" + switchId + ", switchStatus=" + switchStatus + ", dimmerValue="
				+ dimmerValue + "]";
	}

	public String getGenieColor() {
		return genieColor;
	}

	public void setGenieColor(String genieColor) {
		this.genieColor = genieColor;
	}
	

}
