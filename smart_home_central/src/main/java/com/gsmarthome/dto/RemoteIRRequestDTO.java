package com.gsmarthome.dto;

public class RemoteIRRequestDTO {

	private String irRemoteinfo;
	private Long userId;
	private Long homeId;
	private String brandName;
	private String remoteType;
	private String modelNumber;
	
	public RemoteIRRequestDTO() {
	}

	public String getIrRemoteinfo() {
		return irRemoteinfo;
	}

	public void setIrRemoteinfo(String irRemoteinfo) {
		this.irRemoteinfo = irRemoteinfo;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getHomeId() {
		return homeId;
	}

	public void setHomeId(Long homeId) {
		this.homeId = homeId;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getRemoteType() {
		return remoteType;
	}

	public void setRemoteType(String remoteType) {
		this.remoteType = remoteType;
	}

	public String getModelNumber() {
		return modelNumber;
	}

	public void setModelNumber(String modelNumber) {
		this.modelNumber = modelNumber;
	}

	@Override
	public String toString() {
		return "RemoteIRDTO [irRemoteinfo=" + irRemoteinfo + ", userId=" + userId + ", homeId=" + homeId
				+ ", brandName=" + brandName + ", remoteType=" + remoteType + ", modelNumber=" + modelNumber + "]";
	}

}
