package com.gsmarthome.dto;

public class ChangeSwitchStatusResponseDTO {

	private String homeId;

	private String panelNumber;

	private String switchNumber;

	private String state;

	private String dimmerValue;

	private String lockBit;

	private String lockStatus;

	private String hideStatus;

	private String status;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getLockStatus() {
		return lockStatus;
	}

	public void setLockStatus(String lockStatus) {
		this.lockStatus = lockStatus;
	}

	public String getHideStatus() {
		return hideStatus;
	}

	public void setHideStatus(String hideStatus) {
		this.hideStatus = hideStatus;
	}

	public ChangeSwitchStatusResponseDTO() {
	}

	public ChangeSwitchStatusResponseDTO(String homeId, String panelNumber, String switchNumber, String state,
			String dimmerValue, String lockBit, String lockStatus, String hideStatus, String status) {
		this.homeId = homeId;
		this.panelNumber = panelNumber;
		this.switchNumber = switchNumber;
		this.state = state;
		this.dimmerValue = dimmerValue;
		this.lockBit = lockBit;
		this.lockStatus = lockStatus;
		this.hideStatus = hideStatus;
		this.status = status;
	}

	public String getHomeId() {
		return homeId;
	}

	public void setHomeId(String homeId) {
		this.homeId = homeId;
	}

	public String getPanelNumber() {
		return panelNumber;
	}

	public void setPanelNumber(String panelNumber) {
		this.panelNumber = panelNumber;
	}

	public String getSwitchNumber() {
		return switchNumber;
	}

	public void setSwitchNumber(String switchNumber) {
		this.switchNumber = switchNumber;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getDimmerValue() {
		return dimmerValue;
	}

	public void setDimmerValue(String dimmerValue) {
		this.dimmerValue = dimmerValue;
	}

	public String getLockBit() {
		return lockBit;
	}

	public void setLockBit(String lockBit) {
		this.lockBit = lockBit;
	}

	@Override
	public String toString() {
		return "ChangeSwitchStatusResponseDTO [homeId=" + homeId + ", panelNumber=" + panelNumber + ", switchNumber="
				+ switchNumber + ", state=" + state + ", dimmerValue=" + dimmerValue + ", lockBit=" + lockBit + "]";
	}

}
