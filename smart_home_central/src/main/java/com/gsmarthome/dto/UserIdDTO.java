package com.gsmarthome.dto;

public class UserIdDTO {

	private Long userId;
	private String messageFrom;

	public UserIdDTO() {
		super();
	}

	public UserIdDTO(Long userId) {
		super();
		this.userId = userId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getMessageFrom() {
		return messageFrom;
	}

	public void setMessageFrom(String messageFrom) {
		this.messageFrom = messageFrom;
	}

}
