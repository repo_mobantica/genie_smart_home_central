package com.gsmarthome.dto;

public class RoomListByUserDTO {

	private Long id;

	private String roomName;

	private String roomType;

	private byte[] roomImage;
	

	public RoomListByUserDTO() {
		super();
	}

	public RoomListByUserDTO(Long id, String roomName, String roomType, byte[] roomImage) {
		super();
		this.id = id;
		this.roomName = roomName;
		this.roomType = roomType;
		this.roomImage = roomImage;
	}
	
	public RoomListByUserDTO(Long id, String roomName, String roomType, byte[] roomImage, String isInternet) {
		super();
		this.id = id;
		this.roomName = roomName;
		this.roomType = roomType;
		this.roomImage = roomImage;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public String getRoomType() {
		return roomType;
	}

	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}

	public byte[] getRoomImage() {
		return roomImage;
	}

	public void setRoomImage(byte[] roomImage) {
		this.roomImage = roomImage;
	}

}
