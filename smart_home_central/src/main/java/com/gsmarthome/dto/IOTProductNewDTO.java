package com.gsmarthome.dto;

public class IOTProductNewDTO {

    private Long id;
	
	private Integer iotProductNumber;
	
	private String productName;

	private Long roomId;

	private String product_type;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getIotProductNumber() {
		return iotProductNumber;
	}

	public void setIotProductNumber(Integer iotProductNumber) {
		this.iotProductNumber = iotProductNumber;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Long getRoomId() {
		return roomId;
	}

	public void setRoomId(Long roomId) {
		this.roomId = roomId;
	}

	public String getProduct_type() {
		return product_type;
	}

	public void setProduct_type(String product_type) {
		this.product_type = product_type;
	}

	
}
