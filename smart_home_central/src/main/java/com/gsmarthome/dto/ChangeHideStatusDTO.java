package com.gsmarthome.dto;

public class ChangeHideStatusDTO {

	private Long switchId;

	private String hideStatus;
	private Long userId;
	private String messageFrom;

	public ChangeHideStatusDTO() {
		super();
	}

	public ChangeHideStatusDTO(Long switchId, String hideStatus) {
		super();
		this.switchId = switchId;
		this.hideStatus = hideStatus;

	}

	public Long getSwitchId() {
		return switchId;
	}

	public void setSwitchId(Long switchId) {
		this.switchId = switchId;
	}

	public String getHideStatus() {
		return hideStatus;
	}

	public void setHideStatus(String hideStatus) {
		this.hideStatus = hideStatus;
	}

	@Override
	public String toString() {
		return "ChangeHideStatusDTO [switchId=" + switchId + ", hideStatus=" + hideStatus + "]";
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getMessageFrom() {
		return messageFrom;
	}

	public void setMessageFrom(String messageFrom) {
		this.messageFrom = messageFrom;
	}
	

}
