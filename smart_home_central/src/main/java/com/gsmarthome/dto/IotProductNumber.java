package com.gsmarthome.dto;

public class IotProductNumber {

	private String last_iot_product_number;
	private long total_room_size;
	
	
	public long getTotal_room_size() {
		return total_room_size;
	}

	public void setTotal_room_size(long total_room_size) {
		this.total_room_size = total_room_size;
	}

	public String getLast_iot_product_number() {
		return last_iot_product_number;
	}

	public void setLast_iot_product_number(String last_iot_product_number) {
		this.last_iot_product_number = last_iot_product_number;
	}
	
	
}
