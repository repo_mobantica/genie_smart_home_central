package com.gsmarthome.dto;

public class Test {	
	private String abc;

	public Test() {
		super();
	}
	public Test(String abc) {
		super();
		this.abc = abc;
	}
	public String getAbc() {
		return abc;
	}
	public void setAbc(String abc) {
		this.abc = abc;
	}
	@Override
	public String toString() {
		return "Test [abc=" + abc + "]";
	}
}
