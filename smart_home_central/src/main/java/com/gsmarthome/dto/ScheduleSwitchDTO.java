package com.gsmarthome.dto;

public class ScheduleSwitchDTO {


	private String scheduleSwitchId;
	private Long switchId;
	private Long userId;
	private String switchStatus;
	private String lockStatus;
	private String dimmerValue;
	private String dimmerStatus;
	private String repeatStatus;
	private String repeatWeek;
	private String scheduleDateTime;
	private String messageFrom;
	
	public ScheduleSwitchDTO() {
		super();
	}

	public ScheduleSwitchDTO(Long switchId, String switchStatus, String lockStatus, Long userId,
			String scheduleDateTime) {
		this.switchId = switchId;
		this.switchStatus = switchStatus;
		this.lockStatus = lockStatus;
		this.userId = userId;
		this.scheduleDateTime = scheduleDateTime;
	}

	public Long getSwitchId() {
		return switchId;
	}

	public void setSwitchId(Long switchId) {
		this.switchId = switchId;
	}

	public String getSwitchStatus() {
		return switchStatus;
	}

	public void setSwitchStatus(String switchStatus) {
		this.switchStatus = switchStatus;
	}

	public String getLockStatus() {
		return lockStatus;
	}

	public void setLockStatus(String lockStatus) {
		this.lockStatus = lockStatus;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getDimmerValue() {
		return dimmerValue;
	}

	public void setDimmerValue(String dimmerValue) {
		this.dimmerValue = dimmerValue;
	}

	public String getDimmerStatus() {
		return dimmerStatus;
	}

	public void setDimmerStatus(String dimmerStatus) {
		this.dimmerStatus = dimmerStatus;
	}
	
	public String getRepeatStatus() {
		return repeatStatus;
	}

	public void setRepeatStatus(String repeatStatus) {
		this.repeatStatus = repeatStatus;
	}

	public String getRepeatWeek() {
		return repeatWeek;
	}

	public void setRepeatWeek(String repeatWeek) {
		this.repeatWeek = repeatWeek;
	}

	public String getScheduleDateTime() {
		return scheduleDateTime;
	}

	public void setScheduleDateTime(String scheduleDateTime) {
		this.scheduleDateTime = scheduleDateTime;
	}
	
	public String getMessageFrom() {
		return messageFrom;
	}

	public void setMessageFrom(String messageFrom) {
		this.messageFrom = messageFrom;
	}

	public String getScheduleSwitchId() {
		return scheduleSwitchId;
	}

	public void setScheduleSwitchId(String scheduleSwitchId) {
		this.scheduleSwitchId = scheduleSwitchId;
	}


}
