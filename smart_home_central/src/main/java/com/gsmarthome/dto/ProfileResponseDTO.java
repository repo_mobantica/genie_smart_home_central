package com.gsmarthome.dto;

public class ProfileResponseDTO {

	private String profileId;
	private String userId;
	private String profileName;
	private String switchList;
	private String switchStatus;
	private String dimmerValues;
	private String messageFrom;
	
	ProfileResponseDTO()
	{}
	
	public ProfileResponseDTO(String profileId,String userId,String profileName,String switchList,String switchStatus,String dimmerValues, String messageFrom){
		this.profileId = profileId;
		this.userId = userId;
		if(profileName == null)
			this.profileName = "";
		else{
			this.profileName = profileName;
		}
		if(switchList == null)
			this.switchList = "";
		else{
			this.switchList = switchList;
		}
		if(switchStatus == null)
			this.switchStatus = "";
		else{
			this.switchStatus = switchStatus;
		}
		if(dimmerValues == null)
			dimmerValues = "";
		else{
			this.dimmerValues = dimmerValues;
		}
		
		this.messageFrom = messageFrom;
	}

	public String getProfileId() {
		return profileId;
	}

	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}

	public String getProfileName() {
		return profileName;
	}

	public void setProfileName(String profileName) {
		this.profileName = profileName;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getSwitchList() {
		return switchList;
	}

	public void setSwitchList(String switchList) {
		this.switchList = switchList;
	}

	public String getSwitchStatus() {
		return switchStatus;
	}

	public void setSwitchStatus(String switchStatus) {
		this.switchStatus = switchStatus;
	}
	
	public String getDimmerValues() {
		return dimmerValues;
	}

	public void setDimmerValues(String dimmerValues) {
		this.dimmerValues = dimmerValues;
	}

	public String getMessageFrom() {
		return messageFrom;
	}

	public void setMessageFrom(String messageFrom) {
		this.messageFrom = messageFrom;
	}
	
}
