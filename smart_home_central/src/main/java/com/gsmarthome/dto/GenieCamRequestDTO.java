package com.gsmarthome.dto;

public class GenieCamRequestDTO {

	private Long genieCamId;
	private Long userId;
	private String camName;
	private String localIP;
	private String internetIP;
	private String localPort;
	private String internetPort;
	private String messageFrom;

	public GenieCamRequestDTO() {
	}

	public Long getGenieCamId() {
		return genieCamId;
	}

	public void setGenieCamId(Long genieCamId) {
		this.genieCamId = genieCamId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getCamName() {
		return camName;
	}

	public void setCamName(String camName) {
		this.camName = camName;
	}

	public String getLocalIP() {
		return localIP;
	}

	public void setLocalIP(String localIP) {
		this.localIP = localIP;
	}

	public String getInternetIP() {
		return internetIP;
	}

	public void setInternetIP(String internetIP) {
		this.internetIP = internetIP;
	}

	public String getLocalPort() {
		return localPort;
	}

	public void setLocalPort(String localPort) {
		this.localPort = localPort;
	}

	public String getInternetPort() {
		return internetPort;
	}

	public void setInternetPort(String internetPort) {
		this.internetPort = internetPort;
	}

	public String getMessageFrom() {
		return messageFrom;
	}

	public void setMessageFrom(String messageFrom) {
		this.messageFrom = messageFrom;
	}
	
	

}
