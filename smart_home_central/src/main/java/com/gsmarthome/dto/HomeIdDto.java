package com.gsmarthome.dto;

public class HomeIdDto {

	Long homeid;

	public Long getHomeid() {
		return homeid;
	}

	public void setHomeid(Long homeid) {
		this.homeid = homeid;
	}
	
}
