package com.gsmarthome.security.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.gsmarthome.dto.ResponseDTO;
import com.gsmarthome.dto.UpdateUserDeviceDTO;
import com.gsmarthome.dto.UserLoginResponseDTO;
import com.gsmarthome.entity.UserDetails;
import com.gsmarthome.security.domain.User;
import com.gsmarthome.security.domain.UserAuthentication;
import com.gsmarthome.security.repository.AuthenticationUserDetailsService;
import com.gsmarthome.security.token.TokenAuthenticationService;
import com.gsmarthome.service.UserDetailsService;

public class StatelessLoginFilter extends AbstractAuthenticationProcessingFilter {

	private final TokenAuthenticationService tokenAuthenticationService;
	private final AuthenticationUserDetailsService userDetailsService;
	private static final String DEVICE_ID_HEADER_NAME = "DEVICE-ID";
	private static final String DEVICE_TYPE_HEADER_NAME = "DEVICE-TYPE";
	private final UserDetailsService usersDetailsService;
	
	//private final AuthenticationFailureHandler failurehandler;
	
	public  StatelessLoginFilter(String urlMapping, TokenAuthenticationService tokenAuthenticationService,
			AuthenticationUserDetailsService userDetailsService, AuthenticationManager authManager,UserDetailsService usersDetailsService) {
		super(new AntPathRequestMatcher(urlMapping));
		this.userDetailsService = userDetailsService;
		this.tokenAuthenticationService = tokenAuthenticationService;
		this.usersDetailsService=usersDetailsService;
		//this.failurehandler = failureHandler;
		setAuthenticationManager(authManager);
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException, IOException, ServletException {

		final User user = new ObjectMapper().readValue(request.getInputStream(), User.class);
		final UsernamePasswordAuthenticationToken loginToken = new UsernamePasswordAuthenticationToken(
				user.getUsername(), user.getPassword());
		return getAuthenticationManager().authenticate(loginToken);
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response,
			FilterChain chain, Authentication authentication) throws IOException, ServletException {
	
			// Lookup the complete User object from the database and create an Authentication for it
			final User authenticatedUser = userDetailsService.loadUserByUsername(authentication.getName());
			final UserAuthentication userAuthentication = new UserAuthentication(authenticatedUser);
	
			// Add the custom token as HTTP header to the response
			tokenAuthenticationService.addAuthentication(response, userAuthentication);
	
			// Add the authentication to the Security context
			SecurityContextHolder.getContext().setAuthentication(userAuthentication);
			
			response.setContentType("application/json");
			
			//TODO handle authentication
			
			UserDetails userDetails = userDetailsService.getUserDetailsBy(authenticatedUser.userId);
			
			if(userDetails.getImage()==null)
			{
				
				userDetails.setImage("");
			}
			UserLoginResponseDTO responseDTO = null;
			if(userDetails.getHome()!= null)
			{
			 responseDTO = new  UserLoginResponseDTO(userDetails.getId(), userDetails.getFirstName(),
					userDetails.getLastName(), userDetails.getEmail(), userDetails.getBirthDate(), userDetails.getUserType(),userDetails.getPhoneNumber(),userDetails.getImage() 
						,userDetails.getIsFirstLogin(),response.getHeader("X-AUTH-TOKEN"),userDetails.getHome().getId().toString());
			}
			else
			{
				 responseDTO = new  UserLoginResponseDTO(userDetails.getId(), userDetails.getFirstName(),
						userDetails.getLastName(), userDetails.getEmail(), userDetails.getBirthDate(), userDetails.getUserType(),userDetails.getPhoneNumber(),userDetails.getImage() 
							,userDetails.getIsFirstLogin(),response.getHeader("X-AUTH-TOKEN"),"");
	
			}
			final String device_id = request.getHeader(DEVICE_ID_HEADER_NAME);
			final String device_type = request.getHeader(DEVICE_TYPE_HEADER_NAME);
			UpdateUserDeviceDTO updateUserDeviceDTO= new UpdateUserDeviceDTO();
			updateUserDeviceDTO.setUserId(authenticatedUser.userId);
			updateUserDeviceDTO.setDeviceId(device_id);
			updateUserDeviceDTO.setDeviceType(device_type);
			boolean flag = usersDetailsService.updateUserDevicetype(updateUserDeviceDTO);
			
			ResponseDTO<UserLoginResponseDTO> responseDTO2 = new ResponseDTO<UserLoginResponseDTO>("SUCCESS", responseDTO);
			
			Gson gson=new Gson();
		
			String json=gson.toJson(responseDTO2);
		
			response.getWriter().write(json);
		
	}
	
/*	@Override
	protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException failed) throws IOException, ServletException {
	
		 	SecurityContextHolder.clearContext();
	        failurehandler.onAuthenticationFailure(request, response, failed);
	}*/
}