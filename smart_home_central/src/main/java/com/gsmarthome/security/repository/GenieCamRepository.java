package com.gsmarthome.security.repository;


import org.springframework.data.repository.CrudRepository;

import com.gsmarthome.entity.GenieCam;



public interface GenieCamRepository extends CrudRepository<GenieCam, Long> {

}
