package com.gsmarthome.security.repository;

import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AccountStatusUserDetailsChecker;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.gsmarthome.entity.UserDetails;
import com.gsmarthome.repository.UserDetailsRepository;
import com.gsmarthome.security.domain.User;

@Service
public class AuthenticationUserDetailsService implements UserDetailsService {

	
	@Autowired
	UserDetailsRepository userDetailsRepository;
	
	private final AccountStatusUserDetailsChecker detailsChecker = new AccountStatusUserDetailsChecker();
	
	private static Integer ENTITY_TYPE_SUPER_ADMIN=0;
	
	private static Integer ENTITY_TYPE_ADMIN=1;
	
	private static Integer ENTITY_TYPE_BASIC=2;
	
	@Override
	public User loadUserByUsername(String username) throws UsernameNotFoundException 
			{
		
		
		UserDetails authenticationDetails=null;
		
		try{
			authenticationDetails=userDetailsRepository.findByEmail(username);
			
		}catch(RuntimeException e)
		{
			
		}
				
		if(authenticationDetails==null )
		{
		
			throw new UsernameNotFoundException("Unable to find user"+username);
						
		}
		else
		{
			Boolean isLocked=false;
			
			/*
			if(authenticationDetails.getIsEmailVerified())
			{
				isLocked=false;
			}
			*/
			User newUser=new User(username,authenticationDetails.getPassword(), authenticationDetails.getUserType(),
					authenticationDetails.getId(),isLocked);
			
			detailsChecker.check(newUser);
			return newUser;
		}
				
	}
	
	public UserDetails getUserDetailsBy(Long id)
	{
		UserDetails userDetails=userDetailsRepository.findOne(id);
		
		return userDetails;
		
	}
	
	public static HashSet<GrantedAuthority> getAuthority(Integer customerType)
	{
		HashSet<GrantedAuthority> list=new HashSet<GrantedAuthority>();
		
		if(customerType!=null)
		{
		if(customerType.equals(ENTITY_TYPE_SUPER_ADMIN))
			list.add((()->"SUPER_ADMIN"));
			else if(customerType.equals(ENTITY_TYPE_ADMIN))
				list.add((()->"ADMIN"));
			else if(customerType.equals(ENTITY_TYPE_BASIC))
				list.add((()->"BASIC"));
		}			
		return list;
		
	}

}
