package com.gsmarthome.security.domain;

import java.util.HashSet;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.gsmarthome.security.repository.AuthenticationUserDetailsService;

public class User implements UserDetails {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String username;

	private String password;

	private long expires;

	public Integer customerType;

	public Long userId;

	private Boolean isLocked;

	public User() {

	}

	public User(String username) {
		super();
		this.username = username;
	}

	public User(Integer customerType, Long customerId, long expires) {
		super();
		this.customerType = customerType;
		this.userId = customerId;
		this.expires = expires;
	}

	public User(String userName, String password, Integer customerType, Long customerId) {
		super();
		this.username = userName;
		this.password = password;
		this.customerType = customerType;
		this.userId = customerId;
	}

	public User(String userName, String password, Integer customerType, Long customerId, Boolean isLocked) {
		super();
		this.username = userName;
		this.password = password;
		this.customerType = customerType;
		this.userId = customerId;
		this.isLocked = isLocked;
	}

	public User(String userName, String password, long expires, Integer customerType) {
		super();
		this.username = userName;
		this.password = password;
		this.expires = expires;
		this.customerType = customerType;
	}

	@JsonDeserialize(as = HashSet.class)
	@JsonIgnore
	public Set<GrantedAuthority> getAuthorities() {

		HashSet<GrantedAuthority> list = AuthenticationUserDetailsService.getAuthority(customerType);

		return list;
	}

	public void setUsername(String userName) {
		this.username = userName;
	}

	@JsonIgnore
	public String getPassword() {
		return password;
	}

	@JsonProperty
	public void setPassword(String password) {
		this.password = password;
	}

	@JsonIgnore
	public boolean isAccountNonExpired() {
		return true;
	}

	@JsonIgnore
	public boolean isAccountNonLocked() {
		return !this.isLocked;
	}

	@JsonIgnore
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@JsonIgnore
	public boolean isEnabled() {
		return true;
	}

	@Override
	public String getUsername() {

		return username;
	}

	public long getExpires() {
		return expires;
	}

	public Integer getCustomerType() {
		return customerType;
	}

	public void setCustomerType(Integer customerType) {
		this.customerType = customerType;
	}

	public void setExpires(long expires) {
		this.expires = expires;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long customerId) {
		this.userId = customerId;
	}

	@Override
	public String toString() {
		return "User [username=" + username + ", password=" + password + ", expires=" + expires + ", customerType="
				+ customerType + ", customerId=" + userId + "]";
	}

}
