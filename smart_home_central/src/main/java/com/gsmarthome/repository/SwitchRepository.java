package com.gsmarthome.repository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.gsmarthome.entity.Switch;
import com.gsmarthome.entity.IOTProduct;

public interface SwitchRepository extends CrudRepository<Switch, Long> {
	
	@Query("select s from Switch s where s.switchNumber = ?#{[0]} and s.iotProduct.id = ?#{[1]}")
	public Switch checkSwitchalreadyExist(int switchNumber,long IOTPRODUCTID );


	@Query("select s from Switch s where s.switchStatus = ?#{[1]} and s.iotProduct.room.id = ?#{[0]}")
	public Switch[] getSwichbyRoomId(long ROOMID,String onoffStatus );	

	
	public Switch findBySwitchNumberAndIotProduct(Integer switchNumber, IOTProduct iotproduct);
}
