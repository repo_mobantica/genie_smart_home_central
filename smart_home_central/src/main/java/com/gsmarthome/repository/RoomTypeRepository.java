package com.gsmarthome.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.gsmarthome.entity.RoomType;

public interface RoomTypeRepository extends CrudRepository<RoomType, Long> {
	RoomType findByRoomTypeName(String roomType);

	@Query("select r from RoomType r where r.roomTypeName like ?#{[0]}")
	public RoomType findByRoomTypeNamelike(String roomType);
}
