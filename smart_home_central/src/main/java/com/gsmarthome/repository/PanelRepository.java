package com.gsmarthome.repository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.gsmarthome.entity.Panel;
import com.gsmarthome.entity.Room;
public interface PanelRepository extends CrudRepository<Panel, Long>{

	 Panel findByPanelId(Integer panelId);	 
	 Panel findBypanelName(String panelName);
	 Panel findBypanelId(Integer panelId);	
	 Panel findByPanelNameAndPanelCommand(String panelName ,String panelCommand);
	 Panel findBypanelIp(int panelid);
		@Query("select p from Panel p")
		public Panel[] getPanel();


		@Query("select s from Panel s where s.panelId = ?#{[0]} and s.panelCommand = ?#{[1]}")
		public Panel checkPanelalreadyExit(Integer panelId,String panelCommand);
}
