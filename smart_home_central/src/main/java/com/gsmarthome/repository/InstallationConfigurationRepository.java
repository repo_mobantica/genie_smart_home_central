package com.gsmarthome.repository;

import org.springframework.data.repository.CrudRepository;

import com.gsmarthome.entity.InstallationConfiguration;

public interface InstallationConfigurationRepository extends CrudRepository<InstallationConfiguration, Long> {
	
}
