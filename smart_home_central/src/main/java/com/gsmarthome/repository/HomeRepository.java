package com.gsmarthome.repository;

import org.springframework.data.repository.CrudRepository;

import com.gsmarthome.entity.Home;


public interface HomeRepository extends CrudRepository<Home, Long> {
	
	public	Home findByHomeName(String homename);	
	
/*	@Query("SELECT h FROM HOME h where h.isBlocked = ?#{[1]} AND h.id = ?#{[0]}")
    public Home checkHomeBlocked(Long homeId, Integer isBlocked);*/

}
