package com.gsmarthome.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.gsmarthome.entity.UserDetails;

public interface UserDetailsRepository extends CrudRepository<UserDetails, Long>{
	
	UserDetails findByEmail(String email);
	
	UserDetails findByPhoneNumber(String phoneNumber);
	
	UserDetails findByHome(String Home);
	
	UserDetails[] findByHomeId(Long HomeId);

	@Modifying
	@Transactional
	@Query("UPDATE UserDetails u SET u.isFirstLogin = ?#{[1]} WHERE u.email = ?#{[0]}")
	public Integer updateUserIsFirstTimeLogin(String emailId, Boolean isFirstLogin);

	UserDetails findBydeviceId(String deviceId);
	
}
