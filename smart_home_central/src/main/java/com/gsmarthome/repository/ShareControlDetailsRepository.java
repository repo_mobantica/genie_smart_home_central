package com.gsmarthome.repository;

import org.springframework.data.repository.CrudRepository;

import com.gsmarthome.entity.ShareControlDetails;

public interface ShareControlDetailsRepository extends CrudRepository<ShareControlDetails, String> {

	ShareControlDetails findByEmail(String email);
	
	ShareControlDetails findByPhoneNumber(String phoneNumber);
	
	/*
	@Query("select s from ShareControlDetails s where s.createdDate = ?#{[0]}")
	public ShareControlDetails[] findGuestUserByDate(String scheduleDateTime);*/
	
}
