package com.gsmarthome.repository;

import org.springframework.data.repository.CrudRepository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import com.gsmarthome.entity.Enquiry;

public interface EnquiryRepository extends CrudRepository<Enquiry, Long>{

	
}
