package com.gsmarthome.repository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.gsmarthome.entity.ScheduleSwitch;;

public interface ScheduleSwitchRepository extends CrudRepository<ScheduleSwitch, Long> {
	
	@Query("select s from ScheduleSwitch s where s.scheduleDateTime = ?#{[0]}")
	public ScheduleSwitch[] findSwitchByDate(String scheduleDateTime);

}
