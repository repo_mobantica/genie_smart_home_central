package com.gsmarthome.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.gsmarthome.entity.Notification;
public interface NotificationRepository extends CrudRepository<Notification, Long> {
	
	@Query("select count(n) from Notification n where n.status='true' and n.isRead=0 and n.userId=?#{[0]}")
	public int findUnreadNotification(long userId);
	
	
	@Query("select  n from Notification n where n.status='true' and n.isRead=0 and n.userId=?#{[0]} order by id desc")
	public Notification[] findfityUnreadNotification(long userId);
}



