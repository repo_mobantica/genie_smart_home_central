package com.gsmarthome.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.gsmarthome.entity.Activity;

public interface ActivityRepository extends CrudRepository<Activity, Long> {
	
	  	@Query("select a.id from Activity a where homeid= ?#{[0]} order by a.id desc ")
	    public Page<Integer> findLatestId(Long homeid, Pageable pageable);

	    @Modifying
	    @Transactional
	    @Query("select a from Activity a where a.id in (:idList) order by a.id desc")
	    public  Activity[] selectByExcludedId(@Param("idList") List<Integer> idList);
	    
	    @Modifying
	    @Transactional
	    @Query("delete from Activity t where t.id not in (:idList)")
	    public void deleteByExcludedId(@Param("idList") List<Integer> idList);


}

