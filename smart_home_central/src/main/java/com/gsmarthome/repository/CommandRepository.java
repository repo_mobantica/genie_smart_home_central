package com.gsmarthome.repository;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.gsmarthome.entity.Command;
public interface CommandRepository extends CrudRepository<Command, Long> {
	
	
	   @Query("select t.id from Command t order by t.id desc ")
	    public Page<Integer> findLatestId(Pageable pageable);

	    @Modifying
	    @Transactional
	    @Query("delete from Command t where t.id not in (:idList)")
	    public void deleteByExcludedId(@Param("idList") List<Integer> idList);
	
}
