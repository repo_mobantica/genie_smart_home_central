package com.gsmarthome.repository;

import org.springframework.data.repository.CrudRepository;

import com.gsmarthome.entity.RemoteDetails;
import com.gsmarthome.entity.Home;
import java.util.List;


public interface RemoteRepository extends CrudRepository<RemoteDetails, Long> {

	List<RemoteDetails> findByHome(Home home);

}
