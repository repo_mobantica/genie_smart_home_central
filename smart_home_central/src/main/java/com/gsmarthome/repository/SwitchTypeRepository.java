package com.gsmarthome.repository;

import org.springframework.data.repository.CrudRepository;

import com.gsmarthome.entity.Switch;
import com.gsmarthome.entity.SwitchType;

public interface SwitchTypeRepository extends CrudRepository<SwitchType, Long> {
	
	 SwitchType findBySwitchTypeName(String switchTypeName);
	
}
