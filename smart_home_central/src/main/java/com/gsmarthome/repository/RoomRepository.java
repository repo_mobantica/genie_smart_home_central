package com.gsmarthome.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.gsmarthome.entity.Home;
import com.gsmarthome.entity.Room;
import com.gsmarthome.entity.Switch;

public interface RoomRepository extends CrudRepository<Room, Long> {	
	Room findRoomByroomIdentifier(String roomIdentifier);
	

	@Query("select s from Room s where s.roomIdentifier = ?#{[0]} and s.home.id = ?#{[1]}")
	public Room checkRoomalreadyExit(String roomIdentifier,long HOMEID );

	@Query("SELECT COUNT(id) FROM Room s where s.home.id = ?#{[0]} ")
	public long totalCountByHomeWise(long homeId );

	List<Room> findByHome(Home home);
	
	Room findById(long id);
}
