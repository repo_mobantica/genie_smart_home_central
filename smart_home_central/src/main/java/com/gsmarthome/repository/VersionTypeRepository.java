package com.gsmarthome.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.gsmarthome.entity.VersionType;

public interface VersionTypeRepository extends CrudRepository<VersionType, Long> {

	List findAll();
}
