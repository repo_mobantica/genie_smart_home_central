package com.gsmarthome.repository;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.gsmarthome.entity.IOTProduct;
import com.gsmarthome.entity.Room;

public interface IOTProductRepository extends CrudRepository<IOTProduct, Long> {
	IOTProduct findByiotProductNumber(Integer iotProductNumber);


	@Query("SELECT COUNT(id) FROM IOTProduct s where s.room.id = ?#{[0]} ")
	public long getTotalCountByRoom(Long id);


	List<IOTProduct> findByRoom(Room id);
}
