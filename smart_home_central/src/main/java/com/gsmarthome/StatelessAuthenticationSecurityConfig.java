package com.gsmarthome;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.gsmarthome.security.filter.StatelessAuthenticationFilter;
import com.gsmarthome.security.filter.StatelessLoginFilter;
import com.gsmarthome.security.repository.AuthenticationUserDetailsService;
import com.gsmarthome.security.token.TokenAuthenticationService;

@EnableWebSecurity
@Configuration
@Order(1)
public class StatelessAuthenticationSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private AuthenticationUserDetailsService userDetailsService;

	@Autowired
	private TokenAuthenticationService tokenAuthenticationService;
	
	@Autowired
	com.gsmarthome.service.UserDetailsService userDetailsService2;

	public StatelessAuthenticationSecurityConfig() {
		super(true);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http

	   //.csrf().disable().addFilterBefore(new StatelessCSRFFilter(), CsrfFilter.class)
		   .exceptionHandling().and()
			.anonymous().and()
				.servletApi().and()
				
		   .authorizeRequests()
		   
		   .antMatchers(HttpMethod.OPTIONS,"/**","/*","/***").permitAll()
		   .antMatchers(HttpMethod.OPTIONS,"/*").permitAll()

				//allow anonymous resource requests
				.antMatchers("/").permitAll()
				.antMatchers("/favicon.ico").permitAll()
				.antMatchers("/resources/**","/","/user/sendOTP","/user/verifyOTP","/user/verifyEmailOTP","/home/homeverification","/home/homedetails","/user/validateUser","/home/verifygeniehub","/home/findBydeviceId","/user/sendPasswordOTP","/send/shareControlData","/home/fillPanel","/switch/getswitchbyroom","/home/getLastIotProductNumber","/home/getPanelListByHome","/user/updateprofile","/home/getactivitylist","/schedule/scheduleswitch","/datatransfer/getSwitchDetails","/datatransfer/iotProductDetails","/versionType/getCurrentVersionType").permitAll()
				
				//allow anonymous POSTs to login
				.antMatchers(HttpMethod.POST, "/user").permitAll()
				
				//allow anonymous GETs to API
				.antMatchers(HttpMethod.GET, "/api/**").permitAll()
				
				//defined Admin only API area
				.antMatchers("/admin/**").hasRole("ADMIN")
				
				//all other request need to be authenticated
			    .anyRequest().authenticated().and()		
		
				// custom JSON based authentication by POST of {"username":"<name>","password":"<password>"} which sets the token header upon authentication
				.addFilterBefore(new StatelessLoginFilter("/login", tokenAuthenticationService, userDetailsService, authenticationManager(),userDetailsService2), UsernamePasswordAuthenticationFilter.class)
			
				// custom Token based authentication based on the header previously given to the client
				.addFilterBefore(new StatelessAuthenticationFilter(tokenAuthenticationService), UsernamePasswordAuthenticationFilter.class);
	}
	
	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService);//.passwordEncoder(new BCryptPasswordEncoder());
	}

	@Override
	protected UserDetailsService userDetailsService() {
		return userDetailsService;
	}
}
